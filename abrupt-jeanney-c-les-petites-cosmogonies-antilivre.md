---
author: "C. Jeanney"
title: "Les petites cosmogonies"
subtitle: "(récit pauvre)"
publisher: "Abrüpt"
date: 2020
lang: "fr"
identifier:
- scheme: 'ISBN-13'
  text: '978-3-0361-0115-6'
rights: "© 2020 Abrüpt, CC BY-NC-SA"
adresse: "https://abrupt.ch/c-jeanney/les-petites-cosmogonies"
---

# Composition

*le texte qui suit contient*^[liste non exhaustive] : 204 183 espaces ; 3 192 virgules ; 645~et ; 529~à ; 229~comme ; 138~tout ; 106~là ; 102~y ; 86~bien ; 63~faire ; 59~aussi ; 58~points d’interrogation ; 54~ici ; 48~tous ; 39~regards ; 37~peut-être ; 31~mot ; 31~mots ; 30~œil ; 28~main ; 28~monde ; 27~encore ; 27~forme ; 26~yeux ; 24~cosmogonie ; 22~autre ; 22~exemple ; 19~gens ; 17~doute ; 16~pourquoi ; 15~mains ; 14~cosmogonies ; 14~livre ; 14~objets ; 13~quelques ; 13~vraiment ; 12~langue ; 12~possible ; 11~écoute ; 10~lignes ; 10~mondes ; 10~sans doute ; 9~autres ; 9~canard ; 9~réellement ; 8~ensemble ; 7~cercle ; 7~cercles ; 7~murs ; 7~rond ; 7~texte ; 6~bruits ; 6~couleur ; 6~fabriquer ; 6~kilomètres ; 6~ligne ; 6~livres ; 6~mur ; 6~pages ; 6~phrases ; 5~phrase ; 4~impossible ; 4~orange ; 4~ronds ; 3~bruit ; 3~fiction ; 3~formes ; 3~merci ; 3~pages ; 2~disque ; 2~textes ; 1~disques ; 1~pardon ; 0~méthanol ; 0~pesticide.

# Liste des chapitres suivants, convertibles en chapitres précédents à peu de frais

1. Où l'on en saura plus sur une mauvaise habitude, bali, detroit, la
liberté et les baleines
2. Où sera posée sur la table la question du sujet (du latin
*subjectus*, "soumis, assujetti", ce qui d'entrée me contrarie, je
propose une sédition)
3. Où je me demande ce que j'oublie
4. Où le voyage écarte soudainement les bras sur une distance qui va de
calhoun street au jardin du luxembourg
5. Où se lave et se rince quelque chose qui dit oui
6. Ne perdons pas de temps : penchons.nous sur l'attente
7. Du youpala à frank capra
8. Où sera mentionné un livre au futur antérieur, ce qui n'est pas une
mince affaire, et pourquoi pas une épopée au plus.que.parfait pendant
qu'on y est, tout cela finira en toute logique à la poste avec un
gobelet tendu
9. Parlons un peu des filatures et des pianos qui tombent
10. Dessiner des lunettes
11. Examinons ensemble la question des dieux, de la bavière et des
cheveux gris
12. Que dire des pierres, des rhinocéros et des ongles manucurés
13. Quand tout à coup surgit une énorme boule orange ... heureusement
miró intervient
14. Entre parenthèses
15. Aimez.vous le vélo, les montagnes violettes et les effilochures ?
moi oui
16. En aparté
17. Et qu'est.ce qu'on sait du poids des couvertures de survie ?
18. Où il sera question d'œufs cuisinés, de draps, de taxis et de
floraisons blanches
19. Dans la partie qui va suivre, on ne dira pas ce que wechsler pense
de l'intelligence (qui à ses yeux n'est pas une capacité unique, mais
bien un agrégat de plusieurs traits humains), c'est bien sûr une marque
d'étourderie de ma part
20. Très court chapitre qui pointe ex abrupto la possible inadéquation
d'un titre avec ce qu'il est censé annoncer
21. Et pourquoi pas un roman sur la flûte et rien d'autre
22. S'arrêter pour un bilan provisoire me semble ici approprié
23. Où l'on se demandera quel est le bon côté des portes, tout en
faisant une petite cosmogonie (deux)
24. Nous voilà près d'une urne, c'est étonnant
25. Pour faire de petites cosmogonies (trois, etc.)
26. Quelques données scientifiques
27. Livraison gratuite satisfait ou remboursé deux ans de garantie
paiement sécurisé elle n'est pas belle la vie ? n'attendez plus pour
vous faire plaisir
28. Où la narration évoque l'éventualité du yukon et frôle des gueules
de truites avant de se perdre dans les visages et l'écholocation chez les
chauves-souris
29. Digérer difficilement est un emploi à plein temps
30. Focus sur la vie des nasses réticulées
31. "Tu n'arriveras à rien", dit.elle
32. Quittons.nous sans une once de rancœur, gardons.nous d'évoquer les
fosses que nous creusons pour nous mettre à l'abri, ainsi que le manque
de visibilité qui y règne, ayons la pugnacité joyeuse, allons allons, à
toute berzingue

# Où l'on en saura plus sur une mauvaise habitude, bali, detroit, la liberté et les baleines

Je mâche ma langue. C'est un tic. Parfois, quand je réfléchis à quelque
chose, ou quand j'avance en allant d'un endroit à un autre, j'utilise
ma langue comme du chewing-gum et je la mâche, doucement, sans me faire
mal. J'ai un peu honte de ce tic. Un jour je me suis vue dans un reflet
en train de mâcher ma langue, la bouche un peu ouverte, et ça me donnait
un air un peu fou, un peu abasourdi, un peu vieux, un peu dérangé.
Pourtant je ne me sens pas folle quand je mâche ma langue. Mais vieille
oui. Comme une vieille. Comme un vieux pêcheur qui mâche ses vieilles
phrases sur un quai, comme un vieux cliché qui ressasse ses vieilles
combines, comme une vieille histoire dont la fin est bancale, avec une
morale trop appuyée ou trop enrubannée. Parfois je mâche ma langue aussi
à l'intérieur, plus loin que la mâchoire, au profond de la tête,
personne ne s'en rend compte. Comme quand elle (la trentaine,
faussement décontractée) a raconté une blague qu'elle tentait de faire
passer pour une histoire vraie, une anecdote, alors que ça sortait en
ligne droite des pages de Pif Gadget ou du Vermot et tout le monde (ils
étaient tous assis en cercle, une soirée, certains se connaissaient mais
pas tous, il y avait du champagne, une lampe avec un pied transparent
remplie de coquillages et un canard en porcelaine dont on m'a expliqué
que c'était un canard thérapeutique), tout le monde autour du cercle a
éclaté de rire, d'un rire énorme. Plus fort que les bruits extérieurs
qui montaient des fenêtres ouvertes devant le kaléidoscope de lumière
dehors, avec les projecteurs bleus et violets sur les gargouilles. Je
suis restée la seule à ne pas rire. Ou plus exactement, la seule à faire
silence, c'est-à-dire à fabriquer du silence. Bizarrement, mon silence
s'entendait. Il m'enveloppait comme un airbag. Je suis sûre qu'ils
ont ri encore plus fort d'entendre mon silence, qu'ils ont forcé leur
rire à monter en colonne d'air plus puissamment, depuis le diaphragme,
je ne sais pas trop, la technique des chanteurs d'opéra, pour que leur
rire monte à l'unisson et vienne s'arc-bouter franchement à mon silence.
Comme s'ils avaient voulu, tous ensemble et sans se concerter,
l'anéantir, le mouvement réflexe des gnous qui détalent tous au même
moment même s'il n'y a pas de prédateurs en vue. Et depuis mon
silence, comme si j'étais montée debout sur un tabouret invisible,
dedans, je mâchais ma langue.

Je mâche ma langue sur le côté, la bouche sur le côté, en travers,
c'est pour ça que ça me donne l'air dérangé (l'autre jour,
l'adolescent qu'on a croisé en ville, entouré par deux autres, il
criait, il geignait, tordait aussi sa bouche sur le côté, mais son
visage impassible pendant qu'il criait, quelle rupture, on n'a pas su
quoi en faire, ça partait comme en ricochets, le contraste entre sa
figure placide et ses cris, et le contraste entre la rue civilisée et
ses cris, c'était comme une photo de Vivian Maier mais avec le son).

Je mâche ma langue, je mâche mes mots, là aussi ça part en ricochets. Je
mâche mes mots, au contraire de ceux qui ne mâchent pas leurs mots, qui
parlent cash, moi je ne sais pas faire, j'anticipe, je tortille, je
reformule. Je prends tellement de gants qu'on dirait qu'ils se
superposent, plusieurs paires. Et quand j'ai l'impression de dire
quelque chose de sec, de limite agressif, c'est raté, parce que j'ai
tellement mâché mes mots avant de les prononcer que c'est tout aplati,
inoffensif. Et je mâche mes mots quand j'écris. Je les ressasse comme
une vieille, je les lisse, je les améliore, je les remplace au fur et à
mesure par plus précis qu'eux, mais ça m'éloigne. Ou ça les éloigne.
C'est très difficile de rester tout près. On ne s'en rend pas
forcément compte tout de suite, mais plus on lisse les mots et plus le
glissement augmente, les bords s'écartent comme la faille de San Andrea
et on se retrouve les pieds de chaque côté de la crevasse, mais ce
n'est pas une question de plaques tectoniques, c'est une question de
mots, je veux dire une question d'être. Pour que les bords se
réajustent, qu'ils se réparent, il faudrait se coller aux mots sans les
lisser, sans repasser les fripes, les ourlets défraîchis, rester tout
près sans rien recoudre. C'est le plus difficile, parce qu'en restant
tout près on se retrouve contre, tout contre sa sauvagerie.

Je mâche ma langue comme quand au téléphone on écoute la voix
préenregistrée qui donne le temps estimé avant d'avoir le conseiller en
ligne et on a préparé un papier, un crayon, de quoi noter au cas où, et
pendant la reprise en boucles de la voix mécanique, jingle sur Vivaldi,
on gribouille des cercles concentriques. Il paraît que griffonner des
quadrillages, des constructions, c'est avoir le problème bien en main,
se sentir prêt à le résoudre. Si c'est un problème récurent, insoluble,
on fait des cercles qui peu à peu se recouvrent. On mâche des cercles.

J'ai demandé à mes proches de me dire que je mâchais ma langue quand je
mâche ma langue, de me prévenir, car c'est un tic, le plus souvent je
ne m'en rends pas compte, et je ne veux pas avoir l'air dérangé. Quand
ils me le disent, je réponds que je suis désolée, et c'est vrai que je
le suis, désolée, mais en même temps je suis dérangée qu'on me le dise.
Je suis dérangée doublement sans le vouloir, empêchée d'avoir l'air
fou, et contrariée d'avoir l'air fou.

Ça me dérange aussi de ne pas avoir vu tout de suite qu'elle avait une
main artificielle. Elle disait qu'elle parlait cinq langues, mais elle
le disait en anglais. Elle disait qu'elle allait parler plus lentement
pour ceux qui ne comprennent pas bien l'anglais mais elle parlait à la
même vitesse. Elle disait qu'elle avait fait du bénévolat à Bali, pour
les enfants sourds, qu'elle leur donnait des cours d'anglais, et je
n'ai pas pu l'imaginer concrètement à Bali, au milieu des plateaux de
fruits, de fleurs et de safran, à cause de sa main artificielle que je
n'avais pas remarquée.

Je ne cadre pas. Je donne l'information trois fois et trois fois ils me
la redemandent (ceux du cercle, qui se connaissent ou ne se connaissent
pas tous, ils ont une assiette sur les genoux et un verre de champagne à
proximité). Pour ne pas les vexer, je fais semblant de réfléchir, comme
si je devais faire travailler mes méninges pour retrouver l'information
cachée au fond de ma mémoire incertaine (tous les mardi, jeudi et samedi
jusqu'au 27 août), en cherchant bien, avec efforts, en hésitant à
formuler, alors que je sais parfaitement répondre à ce qu'ils
demandent. Ils redemandent trois fois, trois fois je fais l'effort de
répondre avec les mêmes faux semblants, je fais l'effort de ne pas
penser qu'ils sont obtus, et j'oublie de penser qu'ils veulent
peut-être que l'information soit redite et durable pour que s'installe
une coulée de paroles fluides qui sache bercer.

Par contre, il était beau le jeune homme lorsqu'il a mis sa tête dans
son coude replié pour montrer à quel point Donald Trump est désespérant.
Et puis il y a eu ce moment où un autre a montré à quelle hauteur était
tombée la neige à Detroit, cet hiver-là, et c'était plus haut que sa
tête. Les images de salles de concert délabrées, de pianos édentés, de
salles de classe abandonnées, les tables renversées et les portillons
des vestiaires tous ouverts ou déglingués, se sont superposées à celle
de la neige bleue la nuit, lorsqu'il faut déblayer l'allée ou le toit
de la voiture tandis que les lumières rougeoient et se reflètent en
flipper muet et qu'on salue le voisin pour l'encourager devant la
masse de ce qu'il reste à faire. C'étaient des empêchements lointains,
ils ne pouvaient pas nous atteindre là où nous étions, à la hauteur où
nous nous tenions, la même que celle de la rue d'à côté où, sous deux
volets peints, se voient encore des lettres très anciennes, elles sont
presque effacées, "Café au 1^er^". J'ai entendu un bébé pleurer en
contrebas. Je me suis penchée pour regarder, mais rien, que des pavés
vus de haut derrière une jardinière de géraniums. Un des invités avait
laissé son verre sur le rebord de la fenêtre, je ne sais pas qui.
Peut-être la fille qui ressemblait à une épingle acérée. Ses oreilles
comme des lames, ses jambes de jean en métal. Elle disait C'est très
beau en prenant des photos. Ils disaient tous C'est très beau en
prenant des photos, en faisant bien attention de cadrer le mot Liberté
projeté sur les murs, en plusieurs langues, certaines langues que je ne
connais pas. Le mot Liberté flottait à plusieurs mètres de hauteur,
hors-sol. Il ne résonnait pas réellement en eux, pas plus qu'une
couverture posée élégamment sur un sofa ou un marque-page en dentelle
(ils vendent de la dentelle ici, deux maisons plus loin). Mais ça ne
m'a pas attristée cette histoire de mot Liberté sans résonance, je me
suis juste dit que chacun mettait ce qu'il pouvait dans les mots et que
ça n'était pas forcément une faute, ou une erreur, ou un manque, parce
que c'était contrebalancé par ce qu'ils avaient mis ailleurs, dans
d'autres mots qu'ils ne disaient pas, ou dans d'autres objets, comme
ce canard en porcelaine turquoise qui soignait. C'est une question de
croyance au fond, et chacun a ses petites limites. Il faut se forcer
pour le comprendre, mais on l'oublie. Il faut se forcer à penser qu'à
six mille kilomètres de distance, au milieu du sable et de pics rocheux,
un homme époussette des os de baleine disposés en arc de cercle. Une
baleine préhistorique. Il faut s'imaginer que le présent ne s'arrête
pas à l'envergure des bras, à la portée de l'œil, et que la femme
acérée comme une épingle a besoin de son jean de métal pour marcher,
comme la locataire du 1^er^ a besoin de penser qu'un canard posé à côté
d'une lampe lui est très utile, curatif. On ne sait pas grand-chose des
objets. Ou plus exactement, je ne sais pas grand-chose des objets (il
faut que je perde cette habitude d'édicter des règles qui seraient
communes à une masse d'humains, je suis plutôt la seule dans le coin à
mâcher ma langue).

Le dimanche, en été, les objets sortent. Il y a des braderies, des
vide-greniers. Les gens sont assis derrière leur table pliante. Parfois
de la musique déborde de la voiture garée en parallèle et ils laissent
les portières ouvertes pour écouter des chansons. Il y a des enfants
aussi, et ceux qui ont grandi vendent leurs Playmobil. Il y a des
porte-clés, des bougies, des chaussures à talon, des cendriers Pernod,
des clubs de golf, des canevas, des poneys en plastique, des saucières,
des numéros anciens de *L'Illustration*. Ils vendent des calendriers,
certains récents, d'autres parfois plus vieux que moi. Des tas
d'objets et ils ont décidé que c'étaient des objets sans importance.
Il y a peut-être un canard de porcelaine quelque part, mais lui ne
soigne rien. Il y a peut-être un marque-page en dentelle unique pour
quelqu'un quelque part, et ce quelqu'un le caressait tout en lisant ou
en pensant à des choses graves, à des choses secrètes, et puis ce
quelqu'un est mort, ou l'a perdu, et maintenant les gens assis à côté
des voitures aux portières ouvertes le vendent 2 euros. Ce sont des
trajectoires, des mutations sans unité centrale décisionnaire. Un monde
d'objets en formation. Les brocantes se forment comme se développent
les univers, en six jours, en un instant. Le lundi matin, les étals sont
pliés et tout a disparu, mais le dimanche il y a de vieilles cartes
postales avec au dos les Bons baisers de Lion-sur-mer d'une famille en
vacances qui écrit à sa tante. Juste à côté, à Luc-sur-mer, ils ont
trouvé une baleine échouée ("*Dans la nuit du 14 au 15 janvier 1885,
une baleine de 40 tonnes et longue de 19 mètres*"). Ce qui reste
d'elle est suspendu dans le jardin public de la commune et présenté en
enfilade de bouches successives, ouvertes dans un certain ordre sur du
vide. Je ne sais pas trop si c'est de l'os, si c'est du cartilage, si
c'est du boyau séché. Ça peut même ressembler à de la pierre. J'aime
l'idée d'une baleine de pierre. Et l'idée de cet homme à six mille
kilomètres qui en ce moment même époussette le sable avec une brosse. Il
est au milieu du désert, ce qui n'est déjà pas commun. C'est déjà un
destin de personnage. Et il fait apparaître une baleine morte, il
brosse, elle dépasse du sol, du sable où elle avait coulé. On peut
identifier nettement sa forme vue d'avion, qui n'est pas effacée comme
les lettres Café au 1^er^. C'est quand même incroyable ce qu'on
a sous les yeux. Et incroyable ce qui est hors de portée. Ce qui est mis
dans les objets, les ossements, les distances, les changements, les
répétitions.

# Où sera posée sur la table la question du sujet (du latin *subjectus*, "soumis, assujetti", ce qui d'entrée me contrarie, je propose une sédition)

J'ai pensé que je pourrais toujours écrire depuis ces personnages que
j'avais croisés. J'ai pensé que je pourrais écrire depuis la femme
épingle, depuis le manager en chemise satinée, depuis la blonde comme il
faut, un peu ramassée sur elle-même, le sourire un peu facile, un peu
raide, comme si elle se brûlait. Que je pourrais écrire depuis cette
construction là-haut, à la même hauteur que Café au 1^er^, au-dessus de
la pancarte PRODUITS BIOS et de *Sorry I am not listening* imprimé sur
un t-shirt à tête d'ours.

J'ai pensé que je pourrais écrire une sorte d'autofiction, et qu'à
l'intérieur un déclencheur étrange surviendrait, comme une dislocation
: la même scène sans paroles, juste le rire surpuissant ; la même scène
trop bavarde et le rire me désagrège ; la même scène et remplacer la
puissance du rire par des torrents de larmes, ils s'écroulent tous sur
le parquet en chêne massif, s'écroulent tous, le téléphone à la main
ouvert sur la fonction photo, on voit nettement le mot Liberté bien
cadré, tous en larmes ils s'écrient C'est très beau, puis ils fondent et
leurs caractéristiques se descellent, se mélangent et dévalent
l'escalier jusqu'en bas, jusqu'à la grille électrique munie d'un
digicode, et là, au pied des sacs poubelle, on retrouve des fragments
fracassés (la chemise satinée, les oreilles en lame de rasoir, les
épaules de la blonde comme il faut toutes serrées), les passants les
réparent ou s'en emparent, ça sert à autre chose (comme la boîte à
cigares transformée en banjo que j'ai vue en vitrine l'autre jour). Ça se
disperse. J'ai pensé que je pourrais tout écrire et son contraire, qu'il
ne me manquait que la confiance en moi pour ça, que je n'avais qu'à y
croire pour que cela prenne forme.

Mais c'était fatigant aussi cette succession d'événements troublants,
fatigant d'extrapoler pour deviner à quel point ça se défaisait, ça se
distordait, et surtout je n'avais pas vraiment besoin que ça arrive
réellement dans le texte. La réalité est déjà assez défaite et assez
distordue comme ça. J'ai ouvert un nouveau document et je n'ai pas su
écrire autre chose que "J'ai ouvert un nouveau document", et c'était
fatigant cet entre-soi, d'écrire entre-soi, en retour sur soi, de se
regarder. C'est ce qui arrive, naturellement, spontanément, de se
regarder écrire. Ça commence quand on apprend à former les lettres, un
adulte posté au-dessus de notre épaule dit mais regarde un peu ton g, à
quoi ça ressemble. Il faut donner un grand coup de pied en bas pour se
propulser et propulser ce qu'on écrit à un endroit où on ne se regarde
pas.

C'est fatigant aussi de toujours désirer une construction. Quelque chose
qui fasse corps. Même pour dire que ce qui fait corps ne réussira pas
vraiment à faire corps, que c'est quand même un peu une illusion. Même
pour dire qu'on ne peut pas donner corps, réellement, car on est
impuissant. On ne peut pas dire le corps des mains artificielles
perdues, les très discrètes qui ne se voient pas au premier coup d'œil,
qui prennent l'avion pour Vancouver en serrant bien leur sac et leurs
épaules, ou rient comme on se brûle, à petits hoquets secs,
douloureusement.

J'ai pensé que c'était les gens mon sujet, oui les gens. Et je ne les
connaissais pas. J'ai pensé qu'avoir un bon sujet en main c'était
surtout ne pas le connaître. Bien connaître son sujet c'est souvent
l'encercler, le réduire, rétrécir les angles, maintenir à l'écart ce
qui nourrit et modifie, la vie n'étant pas une expérience scientifique
en milieu stérile, la vie étant imbibée de kilomètres de détails
impropres, et chaque sujet également. J'ai pensé que je n'étais pas
spécialiste et que c'était peut-être un avantage. J'ai pensé que les
spécialistes ajustent leur monocle à tel point qu'il arrive qu'il ne
leur reste que l'espace d'une tête de clou pour regarder. Sans doute
une façon pour eux de se sentir en prise avec le réel, de se réconforter
en le disséquant. Peut-être aussi leur peur d'être débordé. C'est bien
compréhensible. Je comprends les spécialistes qui veulent se rassurer,
enfin se spécialiser, je comprends qu'ils doivent, tout comme nous,
composer avec leurs affects, leurs manques, leurs déshérences, leurs
petites fêlures singulières, c'est épuisant. Aussi, il ne faut pas leur
en vouloir s'ils deviennent prétentieux, guindés d'érudition, car sans
elle, ils pourraient tomber dévastés à genoux sur le sol, anéantis.
Certains spécialistes approfondissent tellement leur spécialisation
qu'ils en arrivent à ne plus parler qu'à leur main, chacun la sienne.
C'est d'ailleurs très comique, question effet visuel, lorsque des gens
d'allure respectable fixent leur paume pour entamer une discussion avec
leurs doigts. Ça rappelle le cercle de la piste du cirque avec au centre
le ventriloque qui agite sa marionnette. Les enfants rient. Peut-être
qu'on devrait rire aussi des spécialistes, c'est ce que je me dis (mais
après tout, qui suis-je pour juger, n'étant pas spécialiste des
spécialistes --- mise en abyme).

Les gens, voilà un bon sujet. Un sujet solide. Quand on traite le sujet
des gens dans un roman, ou un récit, ou un poème, une vidéo, une
chanson, un opéra, une peinture, une sculpture ou une installation, on
obtient tant de réponses différentes. Tant de regards différents avec
tant de distances possibles. En surplomb, à côté, dos tourné, toutes les
façons de regarder les gens se retrouvent démultipliées par les regards
qu'on porte. Mais ça ne m'avance pas beaucoup. Est-ce qu'on regarde les
gens quand on regarde la façon de regarder les gens ? Sans doute que
oui, mais à force d'ajouter le filtre du regard sur le regard, et avec
cette succession d'additions de regards, est-ce qu'on ne risque pas au
final, à force de filtres et de filtres, de poser un rideau opaque sur
ce qu'on voudrait mettre en lumière ? Je ne sais pas. Je connais mon
sujet, c'est déjà ça. Je sais que je ne le connais pas, un point de
plus. Je sais aussi que rien n'est plus versatile que l'image d'une
image.

Ici il y a des touristes, nombreux. Et parfois, dans les rues, des
expositions de photos géantes. Il arrive que les touristes prennent en
photo ces photos géantes. Lorsqu'on les voit faire, on assiste à un
vertige grandeur nature. On peut visualiser à quel point les reflets du
monde se répercutent. Peut-être qu'à chaque nouveau reflet, un peu de
substance se perd. C'est possible. Un peu de lumière pourrait
s'échapper, une couche colorée pâlir, baver ou s'intensifier, un
contour se déformer sous le zoom, le dézoom, le flou gagner ici ou là.
Les images, parce que leurs reflets s'additionnent, pourraient se
trouver repoussées de ce qu'elles sont censées raconter. Peut-être que
c'est aussi ce qui se passe avec les mots, ce qui se passe quand on
regarde les mots. C'est comme ravir. C'est quand même bien étrange que
le même assemblage de sons, ra et vir, dise autant le rapt que
l'émerveillement.

Le reflet d'une lampe flotte sur la vitrine et lèche une fausse blouse
d'artiste devant un chevalet factice où une reproduction d'esquisse est
exposée. Sur la table vernie, les gouttes d'eau forment des amas bombés
qu'on a peur de détruire. Ils sont parfaits, parfaits au point de ne pas
savoir le raconter, de ne pas savoir les reproduire, de ne pas savoir
donner simplement une idée de leur beauté géométrique, de leur
transparence charnue, absolue. La fausse blouse, plus haut, a sûrement
été mise en scène par un spécialiste des choses hiérarchisées selon leur
importance. La vérité des détails est cependant bien plus complexe. Ce
serait une belle vie d'attraper de plein fouet le détail des gouttes
d'eau et de s'y tenir. De ne pas considérer ce qui advient comme
décousu, mais comme une chance. Une belle vie de prendre cette certitude
comme sienne. Ça pourrait générer une nouvelle croyance, une église
triviale, une religion du tout-venant. On la reconnaîtrait à des autels
discrets, hétéroclites, disséminés un peu partout. Par exemple on lève
la tête et on remarque un fil qui part d'une tige vers une feuille
craquelée, ensuite une araignée qu'on dirait en lévitation. La pluie
lui tombe dessus, des gouttelettes de pluie plus grosses que son corps,
solides, aussi solides que des gouttes de plomb. L'araignée valdingue
et valdingue, pourtant elle ne tombe pas.

# Où je me demande ce que j'oublie

Je lis la correspondance d'un sculpteur célèbre. C'est un travail
d'archivage, et toutes les lettres y sont, même celles qui n'y sont pas
(avec la date, un destinataire, suivi de la mention "lettre perdue ou
détruite"). Il y a aussi les lettres basiques ("je vous écris pour
vous donner de mes nouvelles, je suis un peu malade et je vous souhaite
une bonne année"). La scène avec tous les invités assis en rond ce
soir-là, au 1^er^ étage, ressemble à cette correspondance. Il y a ce qui
est perdu ou détruit (les enfants sourds de Bali qui apprennent
l'anglais, la nièce qui entre à l'université) et ce qui est basique (qui
veut encore du fromage). Il n'y a pas non plus de construction
spécifique. Le sculpteur ne dit pas dans ses lettres de quelle façon il
sculpte, comment ça lui arrive et pourquoi. C'est toujours par la bande,
toujours des reflets de ce qu'on voit qu'on voit.

Je ne sais pas m'en aller. Je ne sais pas dire au revoir sans avoir
l'air d'une petite fille au fond de la classe qui demande à aller aux
toilettes et hésite depuis deux heures déjà, dépêchez-vous de lui
répondre, ça va être trop tard. Je n'ai pas su partir de ce salon sans
être cette petite fille. Je ne sais pas dire bonjour non plus, je ne
sais pas rencontrer les gens sans ressentir une sorte de sidération
fébrile, c'est d'enfiler un masque qui prend du temps, et la fébrilité
c'est parce que je sens que si j'allais trop vite le masque pourrait me
glisser des mains et on verrait clairement sans lui la petite fille
chercher la porte des toilettes dans le dédale des portes identiques
sans la trouver. Je n'ai pas su saluer les invités lorsqu'ils arrivaient
au compte-gouttes. Chacun d'eux semblait être un piège à contourner, un
plat brûlant qu'il faut sortir du four. Mais entre les salutations
d'arrivée et ma fuite, est-ce que j'ai su écouter et étreindre ce qu'il
était possible d'écouter et d'étreindre ? J'ai entendu l'histoire de ce
canard thérapeutique et pas une seconde je n'ai douté de lui, ni de son
existence, ni de ses capacités à soigner. Et je n'ai pas pensé une seule
seconde que je pouvais en avoir moi-même un, ou plusieurs, qu'ils
prenaient d'autres formes, indécelables pour l'œil non averti, et la
couleur, pas sûre qu'elle soit turquoise mais c'est possible.

Ensuite j'ai écrit une lettre en anglais à un destinataire rare, une
entité entière ancrée dans un corps vif. J'ai essayé de lui dire que je
crois que le monde est fait d'émotions. Même si on pense avoir affaire à
des plantes séchées, des coups de feu dans les rues, des couloirs
d'hôpitaux, des femmes en bleu qui ressemblent à une Françoise Sagan
très vieille à la poste. Des émotions. Des émotions de jardinières
contenant autant de cailloux que de mégots et de géraniums. Des émotions
placardées en affiches pour la fête des ânes l'été dernier,
sérigraphiées sur des plaques de porcelaine Coca-Cola, collées sur des
porte-clés à l'effigie de Gagarine. Des émotions dans les brames des
bisons enregistrés il y a longtemps près du mont Sheridan. Des émotions
dans les photographies, toutes. Parfois c'est daté et signé. Parfois,
c'est sorti de l'album et il est mis en vente entièrement vide dans un
marché aux puces, avec juste les coins blancs qui permettaient de fixer
les images, mais elles non, on ne les voit plus, ça n'empêche pas.

Je voudrais être claire dans ma lettre en anglais, mais c'est très
difficile parce qu'en français non plus, ma langue natale, je ne sais
pas si c'est compréhensible ce que je raconte (*almost nonsense*). Par
exemple, pour ce qui est des émotions : je ne crois pas que le monde
provoque des émotions (bien sûr, oui, mais pas seulement, ça ne se
limite pas à ça). Je ne crois pas que les émotions fabriquent le monde
(par leur façon d'influencer là où tu vas quand tu y vas et comment, en
tout cas pas seulement, et seulement dans une certaine mesure). Je crois
que le monde est constitué d'émotions. Qu'elles prennent la forme de
châteaux d'eau tagués Sarah L Je t'aime ou de bancs incurvés joliment
pour empêcher les SDF de s'allonger --- les émotions ne sont pas toutes
bienveillantes. Et bien sûr c'est confus, même si j'en suis certaine. Je
crois aussi que dans une flaque, ou dans le bois d'un volet à des
milliers de kilomètres de là où j'habite, dans ce que ce bois garde
comme empreintes digitales, ou même dans une lettre, une simple lettre
et d'autres, de simples lettres, très claires ou sibyllines, des lettres
qui n'ont pas été retrouvées ("lettre perdue ou détruite"), ou des
lettres qui existent réellement, archivées ou non archivées, chiffonnées
avant même d'être pensées, et dans des listes de courses, dans de
vieilles pages d'agenda, il y a un monde en résumé, en tout petit.

Un monde complet, avec sa juste part d'oubli. L'oubli, on ne le voit
pas tout de suite. Il n'entre pas dans le cadre. Il faut se reculer un
peu. Comme dans ce dessin humoristique qui montre Dieu en train
de bricoler la Terre sur son établi. Il la répare et va la poser
derrière lui sur une étagère. Au dessin suivant, on a pris de la
distance, on peut voir des dizaines d'étagères et des tonnes de Terres
posées dessus, avec d'autres établis en enfilade et devant eux des
dizaines et des dizaines de dieux au travail. L'usine est si grande que
ses limites débordent de la page. Pour l'oubli, c'est un peu pareil.
Notre dessin contient toutes les particularités qui nous semblent
tangibles, parce qu'on a le visage très près et qu'elles prennent toute
la place. En reculant seulement d'un pas, on apercevrait peut-être
une enveloppe large et nuageuse d'oubli qui attrape tout. Et puis on
l'oublierait, ce qui serait logique. Perturbant mais logique.

# Où le voyage écarte soudainement les bras sur une distance qui va de calhoun street au jardin du luxembourg

Il a dit : "J'ai fait un drôle de rêve, j'ai rêvé que je n'arrivais pas
à m'endormir."

Tirer sur un fil est une possibilité, et tirer sur ce même fil par son
autre extrémité en est une autre. Par exemple, la ville de Detroit,
évoquée ce soir-là, est reliée à l'appartement du 1^er^ étage sur le
principe de la cordelette qui unit ensemble deux boîtes de conserve que
chacun colle à son oreille.

Sur Calhoun Street, les maisons sont toutes semblables, en briques
rouges, et les allées très bien tenues, pas un arbuste qui dépasse. Il y
a une route entre Dearborn et le centre de Detroit qu'on peut suivre
pendant au moins vingt minutes sans hésiter tant elle est rectiligne.
Les bâtiments sont bas. Le rouge n'est pas très rouge et le gris
envahissant semble sûr de lui. Il y a peu de piétons, certains savent où
ils vont mais d'autres non, ils se dandinent et changent d'avis sous des
enseignes Ford de douze mètres de long. On ne sait pas s'ils sont les
rescapés de l'escalier de secours d'une impasse, ou bien les rois du
quartier où la pharmacie est à vendre depuis des mois. On se fait
doubler par des Chevrolet aux portières froissées et aux pare-brises
arrière réparés par du scotch. La route montre les réparations
successives du bitume, des nuances de gris, selon qu'elles sont
récentes ou pas. Elles suivent des lignes nettes avant de se diluer dans
l'ombre des panneaux Speed Limit 35. Sur la façade aveugle d'un cube
énorme, juste avant de tourner à droite vers Charles Street, il reste la
trace d'un coquillage Shell et le mot LIFE. Entre deux hangars vides,
une silhouette en bonnet se faufile, titube avant de changer de
trottoir. Plus loin, des maisons de briques toutes pareilles alignent
leurs entrées ouvertes sur deux chaises derrière des palissades.
Ailleurs des tags, des escaliers jumeaux, des bardages de bois blanc, la
végétation rousse. La rivière Detroit, tu la suis vers le sud et elle
t'emmène vers Toledo, vers Cleveland. Si tu passes par le nord, après le
lac Saint Clair, tu vas remonter vers Sarnia, et là tu continues en
passant sous le Mackinac Bridge pour rallier Chicago, c'est la même eau
qui borde. Par la route tu mets quatre heures et demie, et en passant
par l'eau je dirais presque un jour entier. Ici un couple de faucons
pèlerins tue les pigeons. Qu'est-ce qu'il y aurait à Chicago ? Tu
pourrais vivre dans Calhoun Street, t'asseoir sur un des deux fauteuils
derrière la barrière peinte en blanc sans plus jamais bouger. Suivre du
doigt la route qui mène sur la carte to Downtown Detroit avec les feux
qui se balancent, suspendus dans les airs. Qu'est-ce qu'il y aurait à
Chicago ? Quelqu'un qui chante *No woman no cry* sur Wabash Avenue. Des
tours plus hautes, plus claires. Des passants affairés, une église
adossée à une tour si imposante que le clocher semble renfrogné. L'homme
qui a montré à quelle hauteur la neige s'entassait cet hiver-là (et
c'était plus haut que sa tête) travaille là-bas. Il aime se promener. Il
ne croit pas en une puissance céleste. Il croit plus volontiers au
chaos. Il pense que la naissance résulte de forces contradictoires, et
pour ce qui est de l'existence de l'harmonie, il ne sait pas, mais il
aimerait.

Comme il n'avait que douze ans en arrivant, il parle sans accent. Et on
peut dire l'inverse, comme il avait déjà douze ans quand il est parti,
il parle sans accent, c'est-à-dire qu'il n'a ni l'accent français quand
il parle américain ni l'accent américain quand il parle français, et il
passe d'une langue à l'autre en glissant comme un surfeur. La gestuelle,
je veux dire celle qui ne sait pas parler sans faire de gestes (et
vraiment on la contrarierait beaucoup si on lui attachait les mains,
elle sous-titre tous les mots qu'elle prononce, loin, et sa main
s'éloigne, grand, sa main monte, compliqué, sa main suit la courbe
hasardeuse d'une fumerolle) parle écossais et français, elle aussi sans
souffrir du passage d'une langue à l'autre, mais avec un accent marqué,
autoritaire, comme elle impose des gestes à ce qu'elle dit. Elle a
besoin de maîtriser. Assis en rond, une assiette sur les genoux et un
verre à proximité, ils ont tous besoin de maîtriser, mais ils ne veulent
pas tous maîtriser les mêmes choses.

La chemise satinée veut maîtriser les enfants qui montent sur les
fauteuils et écrasent des cookies sous leurs fesses. La blonde comme il
faut veut maîtriser son rire en gardant les épaules serrées. La
locataire du 1^er^ veut maîtriser les touristes qu'elle promène, surtout
ceux qui lui posent des colles (races de vaches normandes, dates de
construction de transepts et tympans, chronologie des guerres,
descendance de la Reine Mathilde), elle lit énormément et elle apprend
les réponses par cœur. La police de Milwaukee veut maîtriser les
manifestants, et les pompiers de Milwaukee les flammes qui s'échappent
de voitures ventres à l'air. Les hélicoptères récupèrent les images que
les chaînes d'information maîtrisent puis diffusent par flashs,
montrant des foules fantomatiques aux vêtements surlignés de violet à
cause des gyrophares et du feu.

La télé est allumée ce soir-là au 1^er^ étage, mais tous n'y jettent
qu'un œil distrait, vaguement hypnotisés tandis qu'ils parlent
d'autre chose. Sur l'écran une femme (une chanteuse) tourne à 360
degrés derrière des rideaux de douche, puis c'est la même découpée en
lamelles par des stores, la même assise sur un cheval d'arçon, la même
appuyée à un balcon, la caméra tourne autour d'elle avant de finir en
plan large sur une mer propre, une mer éduquée, une plage civilisée,
correctement quadrillée de chaises longues assorties aux palmiers. Ici
la côte est plus sauvage.

Des dunes ébouriffées. Des algues par paquets. Des coquilles éclatées.
Des pontes de bulots, petites concrétions de globes minuscules que l'on
appelle parfois "savonnettes de mer". Les mâts de bateaux en
arrière-plan. Des rues pleines de passants, et où vont-ils ? Certains
dans une brocante, on le voit aux trajectoires croisées, vides à
l'aller, chargées de tables basses ou de fougères en pot au retour. Des
cartons sont calés au sol. "Nous ne sommes pas brocanteurs", insiste
la dame. Devant elle, posées sur un journal imprimé en 1905, des
lunettes de théâtre en corne, un peu fêlées. Cette dame raconte qu'elles
ont appartenu à un compositeur, ami de Debussy, qui s'était fâché avec
lui un jour. Le nom de ce compositeur était Bade, ou Barne, ou Barle, je
n'ai pas bien compris. Elle dit qu'il a écrit un opéra, *Les
Intrigantes*. Plus tard, je chercherai, et je ne douterai pas un instant
de ce qui a été dit, de ce qui a existé, pourtant je n'en retrouverais
pas trace, aucune. Au-dessus des cartons qui débordent, dans une
pochette transparente, une feuille bleue datée de 1921, rédigée à New
York, *request*, *protest*, The Chase National Bank. Et collé dans la
partie haute, comme agrafé sans agrafe, un chèque du 24 août 1921 de
deux cents dollars, avec la signature de l'huissier : F. Angeloch, 60
Broadway. En septembre de cette même année, le paquebot *Paris* arrive
là-bas en provenance du Havre avec mille neuf cent cinquante-trois
passagers --- mais est-ce que cet homme, F. Angeloch (F pour Francis,
Finley, Floyd ou Franklin ?) le verra accoster ? Vingt ans plus tard, le
bâtiment brûlera et chavirera, avec sa salle de cinéma, son dancing, son
café en plein air, sa longue promenade, ses cabines de 1^ère^ classe
toutes équipées de téléphones, son fumoir décoré d'un panneau appelé *Le
Jardin du Luxembourg*. Le jardin du Luxembourg, je connais ce lieu.
Tirer sur un fil, qu'on le prenne par un bout ou par l'autre, mène à
un point déjà connu, déjà parcouru, rempli d'enfants en culottes
courtes --- mon père lorsqu'il avait six ans --- et bordé de statues
faussement grandiloquentes. À quelques rues de là, mon grand-père habite
dans un deux-pièces. Les choses savent fabriquer ce genre d'escaliers
de Penrose. On part à tâtons droit devant, ça tentacule, ça s'enroule,
ça nous enroule, ça n'est jamais fini. C'est foisonnant, ça ne désemplit
pas, les objets suivent en alluvions. C'est une expérience renouvelable.
Les mille neuf cent cinquante-trois passagers redescendent, reviennent
sur leurs pas, recommencent, c'est si puissant. Ça peut traverser les
océans. Ça peut construire des ponts, même momentanés, entre Detroit et
un appartement au 1^er^ étage, entre un port éloigné et un papier agrafé
sans agrafe dans un carton.

Ce n'est pas neuf, sans doute qu'il y a ces déplacements depuis la
première pirogue. Au nord ils ont construit un mur de quatre mètres de
haut. Ils pensent --- ou font semblant de croire --- que ça peut être
éteint, être stoppé, ce vouloir-vivre. C'est fou comme ils ne
comprennent pas, fou comme ils ne voient pas, et fou comme ils s'en
moquent. Ils sont comme des statues aveugles qui dégringolent en
écrasant des corps.

# Où se lave et se rince quelque chose qui dit oui

La nuit, la cuisine du restaurant garde les fenêtres qui donnent sur
l'arrière-cour ouvertes, on peut passer la main entre les barreaux. Le
témoin orange d'une multiprise clignote. L'écran suspendu qu'on a
oublié d'éteindre montre la terrasse vide et un peu du trottoir, ça ne
bouge pas (la nuit).

Les images sont patientes, et les objets. C'est un monde patient qui
attend qu'on vienne découper, tailler, éplucher, qu'on sorte fumer dans
l'arrière-cour, qu'on parle de moteurs de voiture et de permis moto
entre les glings qui préviennent qu'une assiette est prête à être servie
à un client. À l'étage, une femme fabrique une sorte de tableau vivant.
Elle plie les nappes qu'elle vient de repasser et les empile à un
endroit dont on ne voit rien. Ces nappes attendent d'être étendues
dehors sur les tables de la terrasse, d'être filmées par la caméra
oubliée et que s'allument les lampes, aussi puissantes que discrètes,
qui projetteront le mot Liberté sur un mur du XIV^e^ siècle. Le mot
Liberté attend d'être pris en photo par les invités du 1^er^ étage qui,
assis en rond, attendent le fromage, sans vraiment tenir les comptes. Ou
s'ils les tiennent, ils se concentrent sur le nombre de visiteurs, de
nuitées, de promotions et de réservations, de spectateurs, de repas à
thèmes, et moins sur d'autres pourtant parlants : votes exprimés,
ventes de charité, tentes sous les ponts, expulsions, refus de demandes
d'asile, kilométrages de barbelés, nombres d'incapacités à savoir
étreindre, consoler, à ajouter aux quantités de gouttelettes acides qui
tombent, formant de petites trouées permanentes dans les corps des gens.
On a posé une coupe de champagne sur le rebord de la fenêtre.

Derrière une autre fenêtre de ce même bâtiment, mais au rez-de-chaussée,
c'est moi. Je lave des rectangles de coton dans un lavabo, des parures
indiennes très colorées, avec de l'orange lumineux, du rouge vif et du
vert pénétrant. Je les presse, je tords le tissu, le retourne, le pétris
et la mousse remonte à la surface en imbibant les plis là où des pattes
de biches croisent des trompes d'éléphants empêchées. Des plantes
grimpantes et des panthères sont séparées ou combinées sous des nuances
de verts et du grenat puissant. Le blanc de la lessive entre en
émulsion, des bulles se surélèvent pendant un court instant, vite
remplacées par des brillances. Cet assemblage (tissu, eau, couleurs)
peut paraître chaotique : il n'y a pourtant rien de plus logique, rien
qui obéisse mieux à des règles de physiques, des vecteurs de forces en
puissance ("Un tas de gravats déversés au hasard : le plus bel ordre du
monde", dit Héraclite). L'eau gonfle le coton, le déserte, respire. Si
on s'enfonçait plus loin dans la trame en s'équipant de loupes, on
apercevrait des bêtes à corps compacts, grosses de quelques micromètres,
munies de plusieurs antennes très fines et pourvues d'une carapace
marquetée de points blancs. On verrait des batailles terrifiantes.

L'eau de rinçage, on dirait du lait caramel à reflets roux je pense. Il
submerge le tissu lorsqu'on le presse puis disparaît, tournoyant dans la
bonde. L'amas coloré, essoré, est cerné par l'ovale de la porcelaine. La
même ligne ovale borde les feuilles de cytise tombées au sol (c'est
l'automne), cette même bordure étanche, quand le corps de la feuille se
tache de plaques jaunes et que toute raide elle dégringole sous les
bourrasques en heurtant les trottoirs.

Assise en rond avec les autres ce soir-là au 1^er^ étage, j'ai ouvert
la bouche pour acquiescer, approuver d'un gentil sourire, un outil
possédant la même fonction que les épaules serrées de la blonde comme il
faut. Je ne me suis pas levée pour dire Je refuse d'employer les mots
simples d'une poésie simple pour dénoncer ce qui devrait être acquis
pour tous depuis la première parole prononcée qui avait fonction de
poésie et pas d'évitement ou de charme. Je refuse la fonction
d'insignifiance pour tous les morceaux de papier crépon en forme de
cœur lancés aux mariages et pliés, chiffonnés par la pluie, soudés aux
graviers des terrasses comme une nouvelle couche de graviers, une peau
de graviers blancs et fuchsia. Je refuse les prix et les saisons. Je
refuse les comparatifs. Je veux une parole d'ardoise tombée au sol. Je
veux une parole posée par terre qui ne serait à personne. Je veux la
musique du bâtiment avant qu'il ouvre ses portes, ses grincements, les
pieds dans les escaliers qui font résonner la structure, les cris des
mouettes à contretemps. Je refuse la dureté des paroles dures qui font
fonction de coutelas et d'effaceur. Je refuse d'apparaître sur la
photo. Je refuse que mon refus d'apparaître sur la photo soit catalogué
comme un non. Je veux que mon refus soit compris comme un oui qui se
lève pour parler du chaos d'Héraclite. Je veux que mon refus soit
compris comme un oui à l'adresse d'une parole posée au sol. Je refuse
d'assimiler les têtes démises. Je refuse d'assimiler les pelleteuses et
les hommes en uniformes qui disjoignent les planches d'un abri à
Calais. Je refuse d'assimiler le croche-pied fait à un homme qui court
poursuivi par des matraques, et sa course est gênée par ses sacs, des
sacs de n'importe quoi, des sacs écorchés de plastique publicitaire usé,
et à la main son petit garçon tombe aussi avec lui. Je refuse les
enfants assis sur des sièges tout neufs avec de la cendre sur les
jambes. Je refuse les fillettes qui sortent de l'eau en hurlant et les
fillettes que l'eau empêche de hurler. Je veux que mon refus soit
compris comme le refus de tout ce qui n'a aucun sens. Je ne veux pas me
justifier. Nous sommes injustifiables. Je refuse que notre parole ne
dise pas que nous sommes injustifiables. Je refuse qu'elle tranche ce
message à coups de coutelas et d'effaceurs, et qu'elle l'éteigne dans
nos épaules serrées et nos sourires assis en rond. Je refuse que mes
paroles de refus soient considérées comme des défaites. Je veux que mes
refus soient des victoires de soin et d'évidences complexes non
effaçables, et enfermer dans mon poing le monde d'une parure indienne
colorée pour l'offrir.

Je ne me suis pas levée, je n'ai rien dit parce que je savais que ma
parole tomberait quelque part, par terre, de toute façon, et qu'ici
personne n'oserait baisser les yeux. Puis j'ai aidé à couper du melon,
à enlever les pépins à la cuillère et à jeter les graines humides et
stériles pour de bon. C'est à ce moment-là que j'ai décidé de créer de
petits mondes, ronds approximativement, grossiers et assez délicats pour
qu'au premier regard rien n'apparaisse.

# Ne perdons pas de temps : penchons-nous sur l'attente

L'énergie primordiale se sacrifie pour former l'univers, c'est ce
qu'il paraît. Certains parlent aussi de l'œuf cosmique, mais peu savent
à quoi il ressemble. Les Upanishads racontent que le Hiranyagarbha
("œuf d'or, ou matrice d'or") flotta dans le vide un moment, puis
fut rompu en deux moitiés qui formèrent le ciel et la terre.

Pour faire de petites cosmogonies, j'ai besoin d'un récipient en
plastique. Il faut le remplir de chiquettes de papier. Chiquettes,
c'est un mot qu'on utilisait dans la région où j'ai habité il y a
longtemps, un mot que j'avais trouvé parfaitement bien adapté la
première fois que je l'avais entendu. Chiquettes disait morceaux
informes et petits, bouts arrachés, papier réduit en pluie de rien,
sûrement pas en confetti --- "confetti", un terme trop élégant
(l'Italie, l'opéra, le carnaval de Venise). Chiquettes était
irrégulier, pragmatique, laborieux, efficace et rieur, de la même façon
qu'était E (c'était E qui avait prononcé ce mot la première fois, E
était efficace, mais avec bonne humeur, prête à rire, prête à être
efficace et à rire avec la même intensité et simultanément, engagée à
quatorze ans comme bonne à tout faire E avait pleuré, pleuré, des
heures, des nuits entières à quatorze ans, trop sensible, pas assez
dégourdie, loin de sa mère, et la vieille vache de patronne ne lui
laissait rien passer, celle-là c'était la méchanceté qui la faisait
tenir debout --- ses jambes n'iraient plus que son venin la ferait
marcher encore ---, ménage, couture, E ne rentrait chez elle qu'une fois
par mois sur son vélo, et comme E en avait voulu à sa mère de l'avoir
forcée à ça, à être ça, boniche, levée à cinq heures du matin tous les
jours, houspillée, malmenée et pas grand-chose de gourmand dans
l'assiette, et c'est plus tard, bien plus tard --- mais est-ce que E
avait fait le rapprochement ? est-ce que E en avait tiré des conclusions
ou est-ce qu'elle avait pris la nouvelle avec son caractère de femme
devenue adulte ? --- plus tard, une fois bien entraînée à rester rieuse
et efficace, bien plus tard, totalement par hasard, E avait appris
qu'elle avait été adoptée, qu'elle portait un autre prénom que E à la
naissance, E râlait sur les pommes de terre trop petites qu'elle
appelait des pétotes, cette plaie pour les éplucher, E riait d'un seul
coup, ça partait net et clair, ça claquait, E avait pris des leçons de
violon mais E ne voulait pas en jouer, jamais, devant personne, car
c'était une honte que ce ne soit pas aussi beau en dehors de son crâne
que dans son crâne, E s'occupait du garçon de la voisine comme de son
propre fils qu'elle n'avait pas et qu'elle n'aurait jamais, ni fille
ni personne, et sur ce sujet-là E n'avait pas su être efficace et rire
avec la même intensité comme pour le reste, sur ce sujet-là E portait du
noir sous ses vêtements et du noir sous sa peau, une longue robe de
deuil rigide et noire qui craquait lorsqu'elle faisait le geste de
nettoyer, d'éplucher des pétotes trop petites, de secouer, de frotter et
ranger, gestes qui lui étaient quotidiens depuis ses quatorze ans, pour
les autres comme pour elle-même, bah, E continuait de faire ce qu'il
fallait, accompagnée de ses petites superstitions, la crêpe de la
Chandeleur, la première qu'on prépare, on la fait sauter et on la
retourne en tenant un louis d'or dans la main, la main droite, tu la
laisses sur le haut du buffet, tu n'y touches plus, un an après elle est
intacte, elle est devenue comme du papier, regarde, celle-là elle a six
ans, c'est du carton, mais ça ne marche qu'avec la première, la deuxième
crêpe pourrit, et il faut un vrai louis d'or aussi, on en avait un qu'on
se prêtait entre cousines ce jour-là, les cousines qui avaient de vrais
noms de naissance, E née sous X, fille d'une autre boniche sûrement, on
n'avait pas son mot à dire à cette époque, les patrons décidaient de
tout, alors une fois ton récipient rempli de chiquettes de papier,
qu'est-ce que tu fais ?).

Ensuite il faut ajouter de l'eau, mais pas
trop, que ce soit submergé, et attendre. Le secret c'est d'attendre.
Attendre, ça n'est pas difficile. Mais c'est parfois très dur. (Comme quand la blonde comme il faut attend la phrase qui apaise, debout
dans la cabine, le torse nu, les bras croisés devant son sein, imaginant
ce qu'elle devrait recommencer. Les analyses. L'amputation, l'infirmière
le matin, le produit jaune qui désinfecte. L'apprenti pharmacien qui ne
saura pas masquer sa rigolade lorsqu'elle dira "prothèse mammaire
externe" en lui montrant son ordonnance. Et dans la glace : soi.)

# Du youpala à frank capra

S'il n'y a pas de répétitions possibles, si l'on ne peut pas se baigner
deux fois dans le même fleuve, l'eau se renouvelant sans cesse dit
Héraclite, alors l'oubli ne peut pas se répéter, il ne peut
qu'amplifier. Des choses restent, d'autres disparaissent, on avance
malgré tout. Le malgré-tout est une technique de pointe très efficace.

Sur le plan, avec le "vous êtes ici" cerné de rouge, les noms des rues
changent, cela arrive, je l'ai vu. Des rues sont baptisées,
débaptisées, rebaptisées. On vénère, on efface, on recommence. Les noms
sont écrits à la craie, même dans le marbre, même sur les bornes
milliaires qui honorent Tibère Claude, fils de Drusus, salué empereur
pour la 11^e^ fois. Sur le mur d'une ferme, on peut encore lire (pour
l'instant) AUTOMOBILISTES A VOTRE SERVICE, PEUGEOT, RUE VILLETTE GÂTÉ à
900 m. tout DROIT. La rue Villette Gâté n'existe pas, mais la rue
Villette Gâte oui, du nom de Henri Villette-Gaté, directeur d'une
tannerie, là où sont écharnées les peaux du monde. Et le tout DROIT
risque la dégringolade à cause des briques disjointes. Il risque
l'oubli.

L'oubli se retrouve à l'air libre dans les vide-greniers. Tout ce qui
a fait son temps comme on dit, se retrouve là, étalé sur une couverture,
arrangé avec soin sur une table pliante ou déposé en vrac, étiqueté ou
pas. Tout ce qui ne sert plus, mais qui pourrait, pourquoi pas, servir à
d'autres. C'est la question générale, la question mère, celle qui est
à l'origine des suivantes. Avez-vous besoin d'un 33t de Charles
Aznavour ? De *L'Avare*, texte intégral suivi d'un appareil critique
détaillé et d'un cahier d'illustrations en couleurs ? D'un album
(vous semblez être placomusophile) rempli de plaques de muselet ? D'une
vache en céramique qui fait beurrier ? D'un buste de Napoléon au nez
pelé ? D'une horloge à marées ? Attention, sans votre intervention,
cela sera rangé, avec ou sans étiquettes, dans des caisses à l'arrière
de voitures, relégué dans des caves, trié, jeté, ça débarrassera le
plancher, en vrac ou soigneusement, pour atterrir peut-être dans une de
ces décharges à ciel ouvert, remuées par des pelleteuses et survolées de
mouettes, qu'on voit dans les documentaires sur Mexico.

Le futur des objets se conjugue dans les vide-greniers. Ils vivent
dangereusement incertains, en route vers un asile sûr, ou la noyade.

Et la question du "avez-vous besoin de" s'accompagne d'une solide
quantité de "non merci", c'est-à-dire d'une dose considérable
d'humilité : on peut avoir signé des milliers d'autographes, rempli
des stades, et finir là, à côté du Babar en peluche ; on peut avoir été
fêté, choyé, primé, récompensé, et se tenir au bord du vide sur une
table de camping, tout près de s'évanouir ; des stars en une de
Cinémonde lancent des regards à la Lauren Bacall, pas pour séduire, mais
pour être adoptées. Des thermomètres de tôle donnent la température
d'une journée obsolète avec obstination, une attitude splendide
et superflue. Ces objets messieurs-dames travaillent sans
filets et sans applaudissements.

Par exemple une chaise de jardin dont la peinture s'en va par plaques
et se déroule en rubans sous l'assise. Par exemple une boîte à
chaussures où se trouvent six photos. On les sort pour les étaler et
c'est fou : on voit bien qu'elles viennent d'un peu partout, que la
boîte à chaussures est pour elles une nouveauté, et pourtant on dirait
qu'elles forment une famille. Des femmes qui ne se connaissaient pas,
mais se ressemblent. Et elles ressemblent toutes à Hélène Bessette.
Hélène Bessette enfant, dans une sorte de youpala antique. Hélène
Bessette, sac à main sous le bras, petit col droit, coiffure en chou en
haut du crâne, vaillante car elle est jeune, elle n'est pas encore
obligée de faire les ménages. La mère d'Hélène Bessette, l'austérité
de son chapeau triangulaire. Sa grand-mère au regard mélancolique, son
portrait signé par un professionnel sur du carton fané.

Les familles retrouvées, recomposées, réelles, c'est quand la chance
est là, ce n'est pas tous les jours car, il faut le dire, la majeure
partie du temps, c'est l'oubli au travail, besogneux. Et pour le
ralentir, les étals nous appellent à l'aide. C'est comme dans une
comédie musicale. Le théâtre va faire faillite, les acteurs se
maquillent dans les loges, ajustent leur smoking, redressent leurs
bretelles et leurs strass, ils sont tendus parce que c'est la dernière
représentation, le moment du dernier numéro de claquettes. Nous sommes
les spectateurs attendus. Les objets dansent tout ce qu'ils peuvent.
Sans nous, les lampes dans la salle se rallumeront et le film deviendra
illisible. L'espoir de notre venue traverse les choses, on a beau dire,
ça n'est pas rien l'espoir. Il y a de l'impatience. La même que dans
*La vie est belle*, quand les personnages de Capra s'entassent dans une
seule pièce --- mais ce n'est pas l'ascenseur comique des Marx Brothers
---, et qu'ils se bousculent, bienveillants. Cette impatience qu'ils
ont, tous ces objets, de nous voir nous presser tout autour d'eux. Parce
qu'ils savent qu'on vient leur sauver la mise. On vient avec la
promesse de réparer le fracassé. L'impatience, l'espoir, les
réparations, ça n'est pas rien.

# Où sera mentionné un livre au futur antérieur, ce qui n'est pas une mince affaire, et pourquoi pas une épopée au plus-que-parfait pendant qu'on y est, tout cela finira en toute logique à la poste avec un gobelet tendu

J'aurais voulu écrire un livre que je ne vendrais qu'aux amies et aux
amis. J'y ai pensé parce qu'il y a une imprimerie près de chez moi, une
imprimerie à l'ancienne, artisanale. Derrière la vitrine, à droite de
l'entrée, on voit une machine au ventre de bois compliqué par des roues
de métal, lourde, soudée sur le carrelage (tout est ancien, même les
dalles sur le sol). En face de cette machine, il y a un meuble avec des
tiroirs empilés, des tiroirs assez minces, une étiquette sur chaque
façade, c10 ronde, c8 gothique. On fabrique ici des faire-part de
naissance, de mariage, de décès (sans se soucier de l'ordre
chronologique), aussi des impressions sur des t-shirts ou des mugs.

J'aurais voulu faire un livre, comme Virginia W. avec la Hogarth Press,
elle coupe, elle colle les couvertures, elle coud les pages. J'aurais
voulu fabriquer ma maquette de livre, la mettre sur une clé USB et
entrer. Entrer là, dans cette imprimerie, et demander combien
pouvez-vous m'en faire ? comme on demande au poissonnier À votre avis
combien de filets pour trois ? J'aurais voulu y entrer mais pas demain,
pas tout de suite, seulement après avoir passé beaucoup de temps à
préparer, à contrôler la place du texte et l'emplacement des blancs, et
comment ils s'accordent, les vis-à-vis et la structure. Certaines pages
seraient presque vides pour laisser respirer, d'autres serrées pour
mieux remplir. Entre le vide et le rempli il y aurait de la toile, de la
trame, comme des effilochures. Comme du tricot mais incomplet et sans
volonté d'habiller (ça ne s'habillerait pas), comme ces sortes de
fausses manches qu'on met sur les gouttières des stations balnéaires
pour faire joli. Ce serait un détournement. Ce serait un livre détourné
qui ne rentrerait pas dans la fonction commerciale du livre, qui
n'aurait pas d'attaché de presse, pas d'éditeur, et pas vraiment
d'autrice, enfin "mon moi" d'autrice serait accessoire. Une sorte
d'objet mi-anonyme mi-collectif, avec la part de moi mi-anonyme
mi-collective de quand j'écris. Écrit sous les pales des ventilateurs.
Sous les informations qui tombent. Les nouveau-nés emmaillotés à bord
des bateaux de sauvetage. Les glaciers immobiles qu'on couvre de
citations. Les habitants d'Asie en colonnes, en graphiques. Les
peintures rouges de Matisse. Les toits de lauze des maisons enterrées
sous des tumulus d'herbes. Les portes jaunes de Berlin, criardes, avec
du plexiglas dedans. Les îles artificielles construites à force de
bateaux coulés en l'an 1400. Les hommes illustres en mocassins près des
vignobles, un carnet à la main qu'ils annotent, ou bien ils font du
canotage. Celui qui tombe tout droit, la tête en bas, peut-être un
employé du restaurant Windows of the World, aux 106^e^ et 107^e\ ^étages
de la tour Nord.

Il n'y aurait pas que des choses voyantes ou évidentes. Il y aurait
aussi des bricoles. Des encouragements. Quand ça s'enchevêtre les
feuilles. Quand ça se plie des planches, que ça se déforme, que de la
rouille éclate la surface peinte pour fabriquer des archipels et des
carcasses de bateaux. Quand ça s'ouvre toujours à la même page, et cette
page-là on sait ce qu'elle contient, on pourrait réciter ce qu'elle
contient par cœur d'un seul tenant, l'autre pourrait vérifier s'il le
souhaite au mot près, sauf qu'on ne saurait pas lui dire en plus ce
qu'il y a dans ce récité d'une seule traite, ce pan entier de soi tenu
dans un seul bloc qui lance des tentacules vers des choses dormantes.
C'est irréel et malgré tout réel. On ne pourrait pas non plus dire le
fil électrique dénudé près du portail aux hortensias, et la question de
ceux qui se font éventrer par des peines plus grandes qu'eux, mais ça
pourrait aussi être dans le livre, avec de temps en temps "qu'est-ce
qu'on peut faire pour que", ce genre de questions qui rassemble, ce
qu'on a en commun, les écorchés.

Après, tout ça, c'est de la conjugaison. Du futur antérieur. "J'aurais
voulu, j'aurais pu, j'aurais dû." C'est un livre au futur antérieur
dont je parle (le futur antérieur étant habituellement défini comme ce
qui a été impossible. Mais si ça se conjugue cet impossible, comment
peut-on affirmer --- cet aplomb insensé qu'il faut --- que ça n'existe pas
?). J'aurais envoyé le livre aux amies, aux amis. Je me serais assise à
la poste pour recopier les adresses proprement. Ensuite j'aurais pu
regarder autour de moi ce que le petit monde d'une poste raconte, qui
sont ces gens et où vont-ils, et est-ce qu'ils donnent une pièce à
l'homme derrière les portes vitrées, et cet homme en blouson vert foncé,
comment fait-il, quelle force a-t-il pour garder le bras tendu aussi
longtemps et au bout son gobelet ? Ces questionnements pourraient courir
dans les adresses en se faufilant dans les mains des facteurs, des
postières, pendant des kilomètres ou plus, atterrir près d'un lac, d'un
mont des Vosges ou d'un garage de Copenhague, avec le souci de la force
qu'on aurait, des forces qu'il y aurait, des forces qu'il y a partout,
qui changent les noms des plans sur les itinéraires et sont heurtées par
plus dur qu'eux, et quoi.

# Parlons un peu des filatures et des pianos qui tombent

Ça a commencé par les rues pavées et l'hôtel du Doyen, ce qui reste de
remparts et les fouilles où on trouva une tête de Minerve. Puis les
chars Sherman M4 A1 et les plages, ce qu'on a conservé, ce qui se voit
dans les drapeaux et les nombreuses photos d'époque reproduites sur
affiches, le Général figé sous les acclamations, les plateaux, les
sous-verres et les porte-monnaie. Ensuite les villes les plus proches et
la Baie du mont Saint-Michel, les boucles de la Seine, puis des musées,
le Marmottan, le Louvre (il est trop cher, si on veut rentabiliser on
doit y rester la journée et c'est trop long, à la fin on ne voit plus
rien), et Disneyland qu'elle visite seule en repérages, pour y piloter
des familles qui seraient équipées d'enfants. Elle ira les chercher au
Havre à 7h30 le matin. Ils posséderont cinq cliniques, des centaines de
yards et deux chiens. Ils restaureront une Studebaker Commander Regal
1952 et d'autres voitures de course antiques. Elle répondra à toutes
leurs questions. Elle notera celles dont la réponse lui échappe,
consultera ensuite des revues, des guides, des encyclopédies pour
dénicher de quoi maîtriser mieux (s'ils lui demandent de passer par
Bordeaux pourquoi pas, elle connaît un endroit où manger la lamproie au
vin rouge, elle recommande). Les détails qu'elle devra mémoriser vont
s'ajouter en piles démesurées, il y en aura toujours plus. Le périmètre
d'activité de la locataire du 1^er^ enfle, élastique, et on pourrait
penser qu'il s'élargit alors qu'elle tourne à l'intérieur du même
manège de bois.

Ce jour-là elle remonte la rue Laitière jusqu'au parvis où commencent
les pavés, ensuite ça n'arrête plus, des centaines, peut-être des
milliers de pavés, et ils ont tous quelque chose qui les différencie les
uns des autres, des marques, des griffures, un relief inégal. Un chiffre
qui ressemble à un sept à qui il manque un trait, trois traînées
d'orange, comme des doigts qui auraient glissé, une tache en forme de
triangle ou d'arbre. Une fente où l'eau s'évapore ou s'infiltre, petit
déluge régénérant qui brille et laisse des coulées d'apparence
fabuleuse, on dirait des animaux sombres, tous à la file, attendant de
monter dans des arches de Noé déformées, ou peut-être d'approcher Tlaloc
--- aussi appelé Tlalocantecuhtli, "celui qui fait ruisseler les
choses". Les formes des pavés sont inégales. Les gens (poussettes,
femmes âgées, hommes à chapeaux) sont inégaux aussi, avec quelque chose
de spécial, comme les personnages de Sempé (mais je crois qu'elle ne
connaît pas Sempé). Long maigre à rayures, bedonnant. Pantalon à fleurs
blanches géantes, torse réduit au minimum. Genoux abîmés, taches sur les
mollets, sous les volants des baskets. Des sacs à dos plus ou moins
compliqués et des boucles d'oreilles qui bougent. Une queue de cheval
coupée net, une veste brodée, des lunettes de soleil tiennent la frange.
À cheveux blancs très courts, on lui rend l'appareil photo, il ne dit
pas merci. Un chien en laisse, attend, écoute l'orgue derrière les
pierres. Les ongles des pieds vernis rose. Un bermuda d'explorateur aux
poches pleines à craquer, drapeau anglais sur le maillot, la capuche à
réajuster en avançant. Mais ça ne suffit pas pour définir qui que ce
soit, ces différences. Il y a des hics, des étonnements. Comme celui aux
longs bras tatoués (piercings sous le nez et sur la tempe, cheveux
coupés ras sauf une tresse maigre sur la nuque), il sort une lingette
d'un couffin fixé à une poussette (son bébé est solaire) pour l'offrir à
son voisin de table (homme mûr, à tête de contremaître) qui s'est
renversé ce qu'il buvait sur les genoux. Des improbabilités. Ça marche
aussi dans l'autre sens (une très vieille femme, secrétaire particulière
d'une des figures les plus monstrueuses du XX^e^ siècle, le décrivant
comme élégant, altier, noble d'allure et de comportement, et sachant
jouer la comédie à la perfection pendant ses discours d'aboiements et de
haine insupportables). On ne peut capter que les détails, on ne peut
voir que la surface. Les erreurs sont communes, répétées, et ça n'est
pas comme dans les jeux télévisés, quand on se trompe aucun buzzer ne
sonne.

On pourrait choisir de suivre une personne dans la rue, la suivre
jusqu'à ce qu'on la connaisse mieux (la locataire du 1^er^ ne le fera
pas, son trajet est préprogrammé). Suivre quelqu'un au hasard, essayer
de surprendre ce que cette personne regarde. Par exemple les lettres du
jeu de Scrabble qu'une main a collées dans les rues (en haut de
l'escalier et sous le lierre elles disent LE FUTUR COMMENCE). On ne
pourrait pas tout voir ni tout appréhender de ce quelqu'un en le
suivant, mais on comprendrait mieux qu'il y a plusieurs couches. Même
quand on penserait qu'il n'y a rien à apprendre, que c'est cliché ou
vide, on n'en serait pas certain. Il y a cette possibilité des couches
profondes, leur existence non avérée, mais concevable. Et lorsqu'on
connaîtrait ce quelqu'un un peu mieux à force de le suivre, parfois on
l'aimerait davantage. On pourrait décider que chacun devrait suivre
quelqu'un, pendant une matinée, de loin, sans l'aborder, et peut-être
qu'il l'aimerait davantage ensuite. Peu à peu on envisagerait les
quelqu'un comme quelqu'un, quelqu'un d'autre, quelqu'un d'unique,
autant que soi. Le mot "foule" deviendrait inutile. On retrouverait
les personnes disparues (parce qu'on aurait sur nous tous les avis de
recherche), on empêcherait les pianos de tomber des fenêtres, on
préviendrait d'un cri, on pousserait les passants qui traversent au
mauvais endroit, ils resteraient secoués mais vivants, et sur les
pare-chocs pas de sang. On remonterait en arrière, vers l'enfance, vers
le début, vers le premier acte de sauvetage, les protections de soi
enfant qu'on n'a pas pu donner on les offrirait là. Les résolutions
éclatantes seraient au futur surpuissant du potentiel. On en aurait fini
avec la mélancolie des grilles d'égout où les plantes poussent en
inversé. Ça éviterait le sombre, celui-là, certains autres, et peut-être
le pire. Ce serait une belle vie d'éviter le pire. On ne sait pas si on
en serait capable (je ne sais pas si je).

Le petit train touristique, qui passe par les endroits clés, transporte
des familles aux regards hésitants (surtout quand la voix enregistrée
dit *on your left* et que c'est *on your right* que ça se passe) et les
déplace, cheval de bois. Ils s'accoudent sur la portière où est fixé un
panneau 30 % de réduction sur lits, canapés, mobilier de jardin. Vissé au
toit de la locomotive, un éléphant bleu nettoie des carrosseries de
voitures. Il y a un lien (même s'il ne saute pas directement aux yeux)
avec les tableaux de Hopper qui sont tous des allégories : nous passons,
nous humains, aux endroits clé, l'œil perplexe, saisis par les offres
alléchantes et l'éclat de chromes astiqués. Une allégorie assez simple,
somme toute prête à l'emploi. C'est bien pratique, je pense. Mais
est-ce que c'est lucide ? Je sors mon allégorie de ce train comme le
mécanicien sa clé de 12, et puis ? Je crois prendre de la distance, je
crois me situer autrement dans un lieu différent, mais : je n'ai rien
vu en regardant, je n'ai rien vu en suivant l'éléphant. Je suis dans
le train moi aussi. Moi aussi je lève le nez *on my right*, *on my
left*, en soupesant les réductions de 30 %. Je ne construis pas de
mausolées, pas de parcs, pas de bibliothèques. Je n'invente pas les
paroles d'une chanson qui fait qu'on se sente moins triste quand on
est triste. Je cultive peu (un pied de fraisier en pot qui vient de
faire deux fleurs). Alors qu'est-ce que je fais ? J'observe. Je mets
des mots. En vrac ou avec soin. Je prends soin de ce vrac --- j'ai de
petites manies, je suis très tatillonne pour que certains mots
n'entrent pas en contact avec d'autres, par exemple les lèvres, ça me
gêne qu'elles soient "finement ourlées", et en ce qui concerne les
gouttes de sueur qui "perlent", je mets beaucoup d'application à ce
qu'elles coulent plutôt de façon fruste, rudimentaire. J'arrange mon
vrac avec méthode, à ma manière, comme en brocante on installe sa table
de camping. Quand j'y arrive, je veux dire quand j'obtiens un
résultat, je me tourne vers une nouvelle table de camping à installer.
Et pareille aux touristes dans le petit train, je passe par mes endroits
clés.

Surtout un en particulier. Je dois d'abord sortir de la cour intérieure
qui fait fonction de sas. On y est à l'abri des cris des passants, des
enfants, et du son des moteurs ou de celui des poubelles de restaurant
qui sonnent infiniment creux lorsqu'elles roulent, comme si un tunnel
s'enfonçait depuis leurs fonds jusqu'au centre de la planète (c'est
possible). Ensuite je prends l'avenue en pente, je passe devant le toit
de la sorcière qu'on peut louer bed & breakfast. Après le rond-point de
la gare, je tourne à droite derrière le magasin de bricolage où on vend
du carrelage, des plaques immenses, debout, ce qui me fait penser à ces
alignements de piscines, debout, qu'on voit parfois en bord de voie
rapide (pour s'entraîner à la nage verticale sans doute). Puis l'espace
fitness rose, le parking, et voilà mon train qui s'élance tout droit,
lancé, je suis lancée tout droit sur la route de là-bas.

Des fermes avec des géraniums. Des haies. Des murs avec des meurtrières
qui ne serviront pas au meurtre, décoratives. De temps en temps des
trouées sur les champs, des trouées sur les pâturages, des aperçus sur
des hangars pas plus grands que la taille d'une pomme. Des maisons
isolées sur fond jaune, des grillages, des poteaux et des arbres
fruitiers, des cyclistes. Aussi un cheval immobile, sa tête en direction
de là-bas. Autour de lui les joncs d'une sorte de marais dépassent.
Des volets blancs. Des hachures peintes au sol pour que les voitures
ralentissent. Au moment où, sans le savoir, on est un peu en hauteur, on
découvre cette hauteur, parce qu'on en surplombe un détail, un léger
détail, un long trait bleu, oh ce si long trait bleu, ou gris, ou gris
bleuté, qui s'inscrit parfaitement entre deux arbres, deux maisons
isolées, deux champs jaunes. C'est ce moment-là que je cherche à
atteindre, pas la destination. Je veux ce moment d'une hauteur pas
encore sue mais découverte, comme l'arrivée fantastique d'une licorne,
d'une chimère, de Zeus crevant les nuages. Ce moment-là ne ressemble à
personne. Il n'est pas comparable. Ensuite, comme je rentre chez moi,
ce moment-là je lui tourne le dos. Durant tout ce temps le cheval a
gardé la même position. Il ressemble trait pour trait à celui du film
*Un homme qui dort* --- on ne le voit qu'une seconde, quasiment une
image subliminale (un box d'abattoir, le cheval est sanglé, entouré
d'hommes, frappé, il tombe mort, d'un coup, de toute sa masse). La
tête dans la même direction.

# Dessiner des lunettes

Rue des Billettes, le ciel est en deux parties. L'une est vraiment
immense, mais striée de six lignes obliques vers le bas. L'autre est
vraiment petite, des rectangles et des croisillons d'une couleur qui
exagère à force de reflets, jaunes, argent, et vert d'eau accentué.
Lorsqu'on regarde dans le rétroviseur, c'est ce qu'on voit.

Les voitures sont à la queue leu leu. J'observe qu'une dame en
imperméable, mains dans le dos et penchée en avant, avance sur le
trottoir. Je vois se pencher l'une vers l'autre les têtes d'un couple
sous un pare-brise. Une autre dame passe, encore plus penchée que la
première. Portant un grand cabas rayé qu'elle secoue, agressive. Parlant
au caniveau. Avec de drôles de mouvements de bouche, comme si elle
mâchait sa langue elle aussi. Comme si elle se parlait depuis dedans.
Comme si ses lèvres articulaient des mots avalés aussitôt. Pour
s'encourager, s'admonester, je ne sais pas. Elle s'arrête régulièrement,
comme pour chercher à traverser, mais elle ne le fait pas. Elle tient
son sac comme une voile de navire.

Comme. Tout est "comme". Comme une voile. Comme se parlant à soi-même.
Les branches des arbres comme des moignons, la peinture du pied du
panneau stop comme heurtée par une multitude de chocs, comme si une
guerre l'avait frôlé, comme. Il y a beaucoup de "comme", ce qui veut
dire beaucoup de fictions, ce qui veut dire beaucoup de traductions, ce
qui veut dire que la peau est épaisse entre soi et ce qui est vu, que la
peau est épaisse entre ce que l'on voit et les mots qui le disent.
C'est un problème.

(Et je ne parle pas du problème des mots qui font un pas de côté. Par
exemple "rose trémière" sonne plutôt aristocratique alors que c'est
une plante rugueuse, chiffonnée, une plante de pauvre. Et pour
"martin-pêcheur", c'est l'inverse, on visualise un âne, nommé Martin
comme tous les ânes, et un retraité dans la Somme le dimanche, assis
devant un seau en plastique en train de préparer ses hameçons, alors que
l'oiseau ravissant pourrait être sculpté sur l'épaule d'un angelot, place des Miracles à Pise.)

(Et je ne parle pas non plus des mots fêlés, des expressions tordues,
comme "plan de sauvetage de l'emploi" qui veut dire licencier,
renvoyer, expulser, liquider.)

Avenue Conseil, à la radio, ils parlent de cérémonies, de plaques de
commémoration, de bougies sur le canal Saint-Martin, de terrasses de
café pleines, de minutes de silence, de ne pas plier. On s'encourage.
Peut-être qu'on est tous comme la dame qui avance à voix haute, on porte
tous une voix qui resterait interne sinon, à destination de nos
entrailles, une voix qui s'admoneste, qui se secoue, qui envisage et qui
démontre, à destination de tous et pour soi. Est-ce qu'on peut traverser
? On ne sait pas. On avance, on agite un cabas qui nous porte ou nous
pousse comme une voile. Les mots se défont peu à peu, ils se rhabillent
à la va-vite, histoire de tenir le coup eux aussi. La radio passe
maintenant une musique de film en noir et blanc, un film hollywoodien
aux contrastes et à la luminosité soignés, parfaitement contrôlés.
Parfaitement élu, le sympathisant du Ku Klux Klan, le quadrillage de
verre ne reflète pas tout, je pense, il manque les pointes parfaitement
alignées des cagoules blanches. Il manque des mots inversés. L'autre
soir, ce jeune homme qui avait mis sa tête dans son coude, comment
est-il en ce moment ? La couleur de sa peau a-t-elle changé ? Accentuée
de reflets argentés, pâles, vert d'eau, dévastée ? Est-ce qu'il sanglote
? Ou bien la tête dans son cabas il avance, il tente de traverser. Il
entend les slogans, les *great again* scandés. Il baisse la tête. Il
longe les parkings sécurisés, destinés à ceux qui vivent dans leurs
voitures car les loyers sont hors de prix. Il s'inquiète. Il passe sous
les affiches de westerns qui sont des modes d'emploi du vivre (se
défendre soi-même, être jury et juge soi-même et le colt à la main,
voilà l'idée, les yeux si doux d'Henry Fonda et de Gary Cooper avec en
arrière-plan les cordes qui pendent des gibets. Tout ça, de la fiction,
des fictions, indécelables, incompressibles, tenaces, des injonctions
sucrées qui se propagent, qui contaminent les yeux, les connecteurs
logiques de nos cervelles, et mangent nos émotions, les digèrent, les
régurgitent où c'est possible, on ne voit pas forcément où, elles sont
là, elles réapparaissent dans les nappes phréatiques des avis, elles
avancent progressivement, infectent sans qu'on y prenne garde les
réserves de flotte du cerveau, et puis un jour c'est la sécheresse et
on ouvre les vannes, ça se répand, ça entre, ça lève des drapeaux, ça
touche les gestes simples, ça imbibe les housses en plastique qui
recouvrent les sièges des bus réservés aux migrants --- et ça réduits
leurs corps à une attaque virale, corps cliniques, corps discutables,
corps potentiellement non humains, corps malsains, à isoler sous
cellophane).

J'observe maintenant de plus près le panneau sens interdit, juste à ma
hauteur. La bande blanche horizontale est rayée par une sorte de paire
de lunettes dessinée au feutre noir. Au feutre noir un nez coquin, une
bouche moitié rigolante. Voilà aussi ce que font les mains. Pas
seulement lever les poings, pas seulement les rejets, pas seulement
l'aide en soulevant, en empoignant au-dessus du vide, pas seulement les
douceurs, les accompagnements de corps proches et d'âmes proches
qu'elles tressent de signes et de symboles, elles savent faire aussi
d'un rien, une ligne, une courbe, un point, quelque chose. Quelque chose
du geste sauf. Il faudrait savoir faire la liste de ces gestes, ce
serait une belle liste.

Je sais que quelque part quelqu'un-quelqu'une imprime dans de
l'argile des traces de pierres, de feuillages et d'écorces, ce qui
capture des moments d'émotions, ce qui donne des tableaux.

Je sais, j'ai vu, quelqu'une-quelqu'un tailler dans des parpaings
avec des marteaux-piqueurs, des ponceuses, pour les creuser en
accidents, ça montre que ce qui est solide est discutable, que le sol
peut se dérober sous nos pieds.

Ailleurs, des cascades de tissu tombent de haut, car
quelqu'un-quelqu'une laisse pendre des bandes de cotonnades fleuries
et ça ruisselle, sur le même principe que cet arbre appelé "chêne à
clous", que les gens utilisent pour y accrocher leurs souffrances et
les bannir.

Quelqu'une-quelqu'un tricote un maillot de bain rose fluo, puis grimpe
sur une statue monumentale et l'en habille (la statue d'un notable,
connu également pour ses discours misogynes, à ce moment-là, la police
arrive avec des menottes et des expressions comme "perturbations de
l'ordre public").

Quelqu'un-quelqu'une explique que les pieuvres aiment jouer et pour
preuve montre un film où un poulpe jongle avec un ballon de plage.

Il y a aussi quelqu'une-quelqu'un qui dit vouloir attraper la lumière,
rien que ça. Il faut utiliser les rayons du soleil pour brûler des
couches de papier blanc à l'aide d'une loupe. Des pétales sortent des
profondeurs de papiers superposés, une nature vorace de fleurs marbrées,
ancestrales, tenues par des tiges longilignes.

Quelqu'un-quelqu'une sculpte un éléphant aux jambes-tiroirs pour
introduire dans chaque tiroir de nouvelles sculptures d'éléphants dont
la trompe se soulève et s'ouvre sur d'autres éléphants, le dernier pas
plus gros qu'un ongle.

De l'autre côté de la terre, quelqu'une-quelqu'un façonne des poupées
dans des morceaux de bois, puis les peint et écrit sur leurs socles ODE
À LA PLUIE, avant de les aligner dehors pour qu'elles sèchent.

Quelqu'un-quelqu'une construit une cathédrale de ferrailles et dit :
"c'est au-delà de mon explication."

Et près d'ici, dans les boîtes de quelqu'une-quelqu'un, sont
emprisonnés les volumes évidés de coquillages, et c'est comme si on
pouvait toucher l'air qui les a vus.

Il y a aussi l'atlas que quelqu'un-quelqu'une dessine, morceau par
morceau, chaque matin. Chaque matin une carte sur une feuille de format
A4. Le quadrillage des champs, des lignes (chemins, autoroutes, fleuves,
ponts), les rectangles des usines, des supermarchés, et leurs noms
décrétés par l'accidentel des affiches, des prospectus, ce qui donne
une carte assez réaliste que ce quelqu'une-quelqu'un continue le
lendemain, sur un autre format A4, et ainsi de suite.

Et puis quelqu'un-quelqu'une soude des grilles, construit des cages,
avant d'y déposer tout un enchevêtrement de branches. On les regarde et
on ne sait plus si on se trouve dehors ou dedans, c'est bien.

# Examinons ensemble la question des dieux, de la bavière et des cheveux gris

Il faut bien tenir compte des forces contradictoires, des dieux
anthropomorphes, paroles, gestes, souffles, membres, sécrétions, et
animaux nombreux (lions, serpents, poissons, oiseaux) lorsque l'on crée
un univers, et ne pas avoir peur de faire des sacrifices, à l'image du
dieu Týr qui perdit sa main dans la gueule du loup, une ruse pour s'en
saisir et ainsi le vaincre.

Dans la soirée où tous étaient assis en rond, où tous se lèveraient,
ensemble ou pas, se croisant ou se contournant dans leurs déplacements
pour s'approcher de la longue table couverte de victuailles, y chercher
une assiette, s'en aller la manger dans un angle, contre l'appui de
fenêtre disponible (celui qui souligne le clocher bleu ou l'autre, celui
où l'horizon est noir), on aurait pu faire un travelling des mains
jusqu'aux visages, des pieds qui s'écartaient aux marques de
complicité, d'étonnements d'apprendre qu'il venait en France tous les
ans, qu'elle détestait celle-là dont ils parlaient mais comprenait que
d'autres l'apprécient, une sorte de mansuétude dans ce travelling-là,
une sorte de goût iridescent dans l'autre s'en allant du côté illuminé
de la cuisine pour s'approcher de celle à la main de métal, position
close, serrée sur des couverts, un plat, un verre à pied, les passant
sous le robinet sans sentir l'eau couler ni sa température. Comme un
contact raté, ou réussi, ou transgressé. Une volonté de malgré-tout. Et
malgré-tout elle les essuyait ensemble ses deux mains, la factice
prenait l'avantage, se chargeant de maintenir le torchon tandis que
l'autre tapotait vaguement. Et c'était rassurant ce malgré-tout. Ça
disait qu'une réponse, même ébauchée, même résignée, pouvait servir à
réunir. Puis elle parlait de rien, d'une bricole, avec intensité, se
persuadant elle-même malgré l'ennui de trouver ça glorieux, ou
pertinent, comme si son cerveau lui aussi donnait du malgré-tout,
serrant malgré-tout les informations, les historiettes, avec une poigne
de métal, sans tâtonner.

Elle parlait des marchés, qu'elle aimait d'une force extraordinaire. Là
aussi (je pensais) il y a des travellings de couleur qui attrapent de
petits mondes, plus loin que la cuisine illuminée, car les petites
cosmogonies sont partout, et là c'était les siennes qui miroitaient.
C'était des courses automobiles organisées près de BC Place Stadium,
puis déplacées à l'est de Concord Pacific Place, et brutalement on
pouvait faire le rapprochement avec sa main manquante --- restée coincée
sous un volant ? tranchée net sous l'impact, la tôle déformée, les
tonneaux, le début d'incendie ? Tout à coup ça sentait l'essence, les
bruits des moteurs s'entendaient et par flashs on voyait des
combinaisons de courses aux logos étonnants pendant qu'elle continuait
à essuyer l'assiette, lui faisant faire un quart de tour à chaque
passage du torchon, le maintenant comme un volant, c'est ce qu'on se
disait, comme un volant. Sans crier gare elle sautait à pieds joints
près d'une église de Bavière pour raconter une vierge noire, toute
recouverte d'or. Cette statuette unique portait un nouveau-né enchâssé
dans une robe finement sculptée, et dorée elle aussi, en forme de
trapèze. Elle était allée là-bas, avec son mari --- en quelle année ? et
avait-elle encore sa main valide ? ---, elle avait prié là-bas avec son
mari, elle avait été bouleversée là-bas avec son mari, elle avait
ressenti quelque chose de si profond qu'à l'écouter les larmes
montaient aux yeux, immédiates, peut-être parce qu'on sentait que ce
n'était pas la question de croire ou de ne pas croire, pas la question
du mysticisme ou d'une réflexion raisonnée, pas la question non plus
d'avoir été simple touriste ici, ailleurs, visitant Altötting ou
n'importe quelle ville avec ou sans pèlerinage, ni la question non plus
de souvenirs ordinaires qu'on aurait pu se partager, mais quelque chose
de bien plus lourd, de plus ardu, la question de la perte, de ce qu'on
ne retrouvera pas, qui n'a pas été pris en photo ou n'a pas fait
l'objet d'un journal de voyage --- et même avec ces moyens-là, photos,
journal, est-ce qu'on aurait pu rendre compte de la pellicule des
choses toutes vibrantes parce qu'on est là, à cet endroit, avec son
mari qui n'est plus, le tenant par une main qu'on n'a plus ? ---, une
sorte de constat de drame qui disait la portion gigantesque de
jamais-plus, un catalogue des gestes chargés plus que de nécessaire du
poids terrible de jamais-plus, des lests et des bardas entiers de
jamais-plus, cette part de soi ne pouvant plus être rejointe, les
lumières de milliers de travellings dans les cuisines n'y pouvant rien.

On se touchait l'épaule pour montrer qu'on avait compris, pourtant
c'était inexplicable. Inexplicables, insaisissables, ainsi sont les
cosmogonies, c'est leur nature. Si elles bougent, se développent,
fusent, se diffusent parfois comme par un robinet ouvert, c'est parce
que c'est ce qu'elles savent faire. Et tout autour de soi, il y en a.
Autour de moi, j'en vois, et pas seulement dans les vide-greniers.

Il y a celle du porche qui protège de la pluie. Celle de la vieille dame
qui demande sans cesse l'heure. Celle de la
femme-soutien-gorge-en-dentelle sous plexiglas, fixée sur son pied qui
balance. Celle des réunions du Rétro Auto Club qui le dimanche exposent
une Simca Chambord mise en circulation en 1959. Celles des vases à tige
de verre, du verre des cygnes de cristal de Murano, du cristal des
pochettes de papier cristal, des dentelures des timbres, des cartes
postales, des dépliants touristiques édités par des marques qui
n'existent plus. Celle d'une photo de communiante, sa petite sœur lui
tient la main et on sait qu'elle est blonde, même si sur la photo elle
a les cheveux gris. Des chemins sont tracés entre ces choses. J'y
crois.

# Que dire des pierres, des rhinocéros et des ongles manucurés

Cette soirée (qui m'intrigue, visiblement) contient sûrement des
paramètres, qu'ils soient fictifs ou non, remarquables. Il faut
peut-être la rapprocher de la misère pourpre dans son pot, recouverte
d'un entrelacs de toile d'araignée, ça tire des fils et lorsqu'il
pleut ça retient des gouttes qu'on dirait de métal, parce qu'elles
luisent (c'est intrigant).

La trentaine (faussement décontractée) n'a pas ri à la blague qu'elle
racontait sur le même mode que les autres, bien sûr parce qu'elle la
connaissait, mais aussi parce qu'elle aime contrôler et aime se
contrôler. Une partie d'elle --- la partie gauche ? sa coiffure est
asymétrique --- aime manipuler. Certains se font avoir et foncent comme
des proies qui sautent les unes derrière les autres sans savoir ce qu'il
y a devant. Elle se tient à son poste d'observation, aux aguets. La
trentaine a des bras nerveux et des jambes nerveuses. La trentaine est
tendue, sous la blague, sous ce qui suit la blague, sous sa façon de
rester assise et faussement décontractée, elle est tendue. Elle n'est
pas là par hasard, ne dit pas ça par hasard, ne laisse rien au hasard,
ne croit pas au hasard. Pour elle, chaque événement accouche de sa suite
logique. Il est possible que de grands rouages s'activent lorsqu'elle
traverse une pièce, comme si elle traînait derrière elle un papier peint
vintage à la Jules Verne, décoré de machines retorses, un papier peint
qui se développerait à mesure qu'elle avance, mécanique cohérente, d'une
pièce à l'autre, pour venir avaler les parois, les sols, les plafonds.
La trentaine s'intéresse à tout. Plus particulièrement au concret. Par
exemple les montres-bracelets. Par exemple les sels de bain. Les caves
où sont stockés son four à micro-ondes et son équipement de ski. Les
chaussures de marque espagnole ("Nous avons un cuir pour chaque instant
et pour chaque personne"). Les vues de l'Acropole. Les comparatifs de
systèmes d'alarme performants. Elle porte un prénom étrange, suivi d'un
nom étrange, et les deux peuvent s'interchanger. C'est un mystère
qu'elle porte ces prénoms et noms, spécifiquement.

La trentaine, ce soir-là, n'a pas réellement ri quand le cercle riait de
son histoire, elle a poussé de petits sons répétitifs, bouche
entrouverte, puis elle a attendu que le bruit s'affaisse de lui-même.
Elle en a l'habitude. Une petite pierre dans son cerveau (là où nos
cent milliards de neurones s'activent vingt à mille fois en une
seconde) fait qu'elle ne s'émeut pas facilement. C'est une grande
force en elle, doublée d'un bon degré d'étanchéité qui la sauvegarde,
mais l'isole tout autant (un don et une malédiction). Lorsque la
trentaine se lève, la petite pierre la suit, à l'aplomb de son centre de
gravité. Cette petite pierre est ronde (une sorte de médaillon de Huy,
deux anges et l'arbre de création du monde, "Veritas Univers"). Une
pierre cachée --- mais est-ce que tout le monde n'en possède pas une ?
Une pierre qui tangue parfois, surtout quand la trentaine se penche vers
chemise satinée, son père, à cause des capteurs rigoureux qu'elle a
forgés pour lui. Pour lui répondre. En réaction à cette masse paternelle
satinée d'emportements, de rages, d'éclats de voix. Elle le connaît,
autant que le vétérinaire du zoo connaît le rhinocéros (il ne
s'approchera pas de lui sans de très bonnes raisons, il sait pour sa
myopie, la puissance de sa carcasse et son caractère pétulant). Ce genre
de savoirs influence les pierres, mais ne modifie pas l'avenir
(connaître l'existence de l'abîme n'en protège pas). La trentaine se
tient sur ces gardes. Subrepticement, sans qu'on s'en doute, elle se
retient de respirer. Ça ne se voit pas à l'œil nu comme ça ne s'entend
pas au souffle, mais la trentaine respire difficilement.

La locataire du 1^er^ ne remarque rien, son champ d'action est saturé.
Penser au vin à ne pas mettre au réfrigérateur, penser à découper en
tranches fines le saucisson de sanglier acheté au marché quelle
merveille, penser à montrer aux invités le cadre qu'elle a su accrocher
toute seule dans la salle à manger ("est-ce que c'est droit ?"),
penser à baisser le son de la musique qu'elle met comme on passe le
chiffon, tout ça lui prend trop d'énergie, elle ne peut pas en plus
vérifier si les invités respirent. Ce n'est pas égoïste, elle n'a pas
le choix.

Se lever avant le soleil. Enfiler sa cuirasse de crèmes et de parfums
sous les spots orientables. Rouler. Réceptionner les clients. Sourire.
Proposer vers midi un restaurant spectaculaire avec vue imprenable sur
la côte de Nacre. Garder la ligne. Refuser les avances salaces. Accepter
les pourboires. Plaisanter de façon adaptée. Caser le rendez-vous avec
la manucure. Très important la manucure, c'est un moment à soi, rien
qu'à soi, et la vision de ses ongles correctement entretenus, à chaque
fois qu'elle ouvre une porte ou vérifie la messagerie de son téléphone,
constitue une preuve, la preuve qu'elle s'en sort bien, qu'elle est
une femme battante comme celles en justaucorps qui mettent dans leur
salon les vidéos de Jane Fonda, des femmes d'un certain âge qui font
beaucoup plus jeune. Qui n'ont pas peur. Oui, ce que la locataire du
1^er^ voudrait qu'on retienne d'elle, c'est ça : elle n'a pas peur.
Elle a traversé l'océan Atlantique. Elle est venue sans meubles : le
temps qu'accoste le container, elle a dormi par terre. Elle s'est
acheté, de façon autonome, pour décorer sa table basse enfin
réceptionnée, un canard, regardez, c'est un canard et ce n'est pas un
canard, en tout cas pas seulement, en tout cas son aspect extérieur ne
dit rien de ce qu'il est à l'intérieur, une victoire, la victoire des
ongles soignés qui l'ont posé près de la lampe, une victoire durable,
celle de l'émancipation, on ne peut pas lui voler, on ne peut pas
l'empêcher de courir le dimanche matin un bandeau sur le front, on ne
peut pas l'empêcher de désirer être cette femme déterminée au profil de
gagnante, on ne peut pas la priver de régler le volume de la musique
comme elle l'entend, c'est-à-dire de laisser en fond sonore la télé
allumée sur des clips dont elle se fiche, tout comme on ne peut pas lui
enlever ses coups de téléphone longue distance où elle répète *Don't
worry I'm fine* avec l'impression que c'est vrai.

# Quand tout à coup surgit une énorme boule orange --- heureusement miró intervient

Sur la terrasse, je suis des yeux un hanneton. Il longe un quadrillage
de bois et descend, presque verticalement, à peine quelques écarts, il
fait ce qu'il sait faire. Bientôt il atteindra au pied du mur la mousse
verte, les éclats gris lancés en arbres miniatures. Les cris des
mouettes, une toux, on entend ce qu'on sait entendre. Lorsqu'on écoute
en s'appliquant, il y a ce ronronnement suave, constant, fait de
moteurs (de voitures lointaines), d'aspirateurs (les magasins qui
ouvrent), et les livreurs font s'entrechoquer des clochettes brèves,
très aiguës, pendant qu'ils transbahutent des caisses de bouteilles. On
replie les volets pour chercher la lumière. On emprisonne en même temps
des dépôts serrés de plumes collées à de vieilles feuilles, à des
poussières, des fragments d'emballages. Sur le perron, du sable qu'on
n'a pas eu envie de nettoyer forme une sorte de terre conceptuelle, une
intention de continent, comme une sensation prise dans l'ambre qui
rappelle quelque chose qu'on n'arrive pas à définir, quelque chose du
passé mais d'un certain passé, un instant T qu'on aurait réussi à
capturer dans un bocal. Ce ne sont plus les volets qu'on ouvre mais des
tentatives de comprendre les bruits, les chiens, les pleurs d'enfants,
des tentatives de comprendre les bruits que l'on écarte, qui se
replient et masquent les restes, les oubliés, les attentes devant un lit
d'hôpital, les amnésies et on ne sait même plus son prénom, les
constructions douces qu'on élabore parce qu'il fait froid dehors, même
sous la canicule, et froid dedans, même dans les plis du ventre où le
cœur résiste pour que se stabilisent 37 degrés. Une planète
inhospitalière ici, avec des hannetons qui descendent tout droit et plus
bas que le sol. Mais il faut être juste, y'en a aussi qui montent.

La femme fatiguée plus que de raison mais droite s'appelle I et son
visage est caractéristique. On la reconnaît facilement. Ensuite, dès
qu'on la voit, on sait qu'elle s'appelle I, c'est évident.

La femme fatiguée plus que de raison mais droite (c'est-à-dire I) a rêvé
qu'elle était publiée. Une jeune femme l'accueillait dans son bureau et
lui disait c'est bon, je prends, et la femme fatiguée plus que de raison
mais droite était heureuse. Elle essayait de deviner de quoi il
s'agissait, on lui montrait un vieux manuscrit dont elle ne se souvenait
pas du tout (avec des sortes de saynètes brèves, comme des aplats de
couleur excessive, un truc léger), et ça la rendait encore plus heureuse
: si elle ne se souvenait pas de ce texte c'est qu'il n'était pas lié à
ses entrailles, qu'il ne serait pas douloureux à relire pour le travail
de publication, pas douloureux de se voir proposer des couvertures qui
correspondraient plus ou moins à l'idée qu'elle en aurait, pas
douloureux ni inconfortable d'attendre le jour de sa sortie, ni
douloureux ni inconfortable d'attendre des réactions, ou plus exactement
des non-réactions, grâce à ce texte oublié la femme fatiguée plus que de
raison mais droite pouvait rester tranquille et active, un artisan qui
fait ce qu'il sait faire, sans tortures métaphysiques, sans
tiraillements, sans états d'âme. I faisant son travail de hanneton. Une
fois réveillée et réalisant que ce n'était qu'un rêve, elle a tenté de
se souvenir du texte, de sa forme, de son contenu (c'était peut-être une
piste, une indication, une direction fléchée, elle n'aurait plus qu'à
s'enfiler dans cette voie simplement, comme on met des chaussons), mais
ça n'a pas été possible. Ce texte n'était pas d'elle. Il n'était pas à
elle. Il était à cette fille pragmatique qui cherche à poser des volumes
les uns au-dessus des autres pour que ça fasse une tour. Une fille
simple qui fait des livres comme on fait la vaisselle, parce qu'il le
faut. Cette fille existe en I, mais tant de détails la mangent qu'elle
est souvent contrariée et s'en va. I est une partie d'une autre partie
d'un personnage. C'est ce qui arrive quand les rêves sont très forts,
très profonds. Quelqu'un vient prendre notre place à travers le sommeil,
puis reste. S'installe à résidence. Pour moi c'est I, la femme fatiguée
plus que de raison mais droite.

J'ai un trou dans l'œil gauche. C'est ce que l'ophtalmo a dit, avec
l'accent roumain, un nom roumain et un prénom roumain, des épaules
toutes rondes, des épaules qui auraient pu chanter je crois, et des
sandales tressées. Elle a pris mon œil gauche en photo (enfin, pas elle,
son assistante), puis elle me l'a montré sur l'écran de son ordinateur,
c'est une énorme boule orange un peu ahurie. Un truc à la Dali, avec des
veines. Je n'aime pas Dali. Je n'aime pas Dali pour des raisons
éthiques, j'ai de l'éthique. Et il y a bien une tache, enfin, une zone
blanchâtre, comme un anticyclone des Açores très concentré au-dessus
d'une portion de Terre sans continent, c'est lui le trou ; enfin elle a
parlé d'un trou, puis elle s'est corrigée, et en roulant les sons elle a
dit "une excavation". Excaver, c'est bien creuser, profondément, avec
efforts ou sous la pression d'une urgence, d'une instance dramatique,
d'un danger ? L'exemple du Larousse est frappant : "les excavations
causées par les bombardements." Par quoi mon œil gauche est-il
bombardé, de quoi le cratère visible à sa surface est-il témoin ? --- ce
n'est pas un glaucome dit l'ophtalmo, vous êtes sans doute née avec ça
--- tout s'explique, mes tiraillements, mes interrogations métaphysiques
sur les livres qu'on ne peut pas faire comme on fait la vaisselle, les
portes inatteignables des toilettes, les rires qui enflent et la façon
qu'on a (je veux dire nous humains) de ne pas communiquer et de
communiquer en même temps, tout est là, tout vient de là, du trou dans
mon œil gauche.

Excaver, d'après ce que j'avais compris, ça n'était pas seulement
creuser, mais il y avait aussi (je me suis visiblement trompée) l'idée
de sortir quelque chose du sol. C'est à cause du suffixe en ex qui me
rappelle extraire. Le sens d'extraire s'est appliqué pour moi sur celui
d'excaver comme une tache d'encre. Je trouve même une certaine logique
dans "Les excavations causées par les bombardements" en pensant que
ceux qui bombardent veulent extraire la vie de ceux qui sont bombardés.

Je crois qu'il y a des tonnes de mots en trop, des tonnes de mots
impraticables. Miró dit que le mot arrive avant la pensée. Il y a ces
tonnes de mots dans les dictionnaires qui décrivent des kilomètres
d'horreurs inimaginables, et les aplatissent, et les rendent
inoffensives, et on utilise le mot "bombardement" dans une grille de
mots croisés, comme si ça n'était qu'une suite de figures comparable à
une autre suite de figures équivalentes. Le mot bombardement pourrait
être remplacé par le mot boulangeries sans problème. C'est le genre de
pensées, quand j'y pense, qui pourrait aussi être responsable du trou
dans mon œil gauche. Il y a des territoires, des espaces, où les mots
flottent comme des lampions moisis. Le problème avec Miró, s'il a
raison, et si le mot arrive bien avant la pensée, c'est qu'existent tous
ces espaces où les mots flottent en ampoules usagées. Le problème, c'est
qu'une fois sortis de ces espaces, les mots traînent avec eux une
poussière, une usure, de la glu sous les pieds. On se scandalise
sporadiquement. Il y a des centaines de bombardements, mais il suffit
d'une photo éprouvante pour qu'on le réalise, une photo surmontée du
titre Bombardements. Ce sont des indignations sélectives. Qui ne durent
pas. Ou pas plus longtemps que le temps d'écrire bombardements. Un petit
garçon sur une chaise orange neuve, ses vêtements et ses cheveux
ensevelis sous une poussière grise, ses mollets sagement alignés l'un
contre l'autre, la partie droite de son visage déformée et du sang avec
le mot bombardement écrit en noir dans une police de caractères précise,
justifiée, lisible mais illisible, à la une des journaux. Les
bombardements sont illisibles. Les petits garçons bombardés sont
illisibles. Parce que le mot est passé du côté répugnant des mots qui
n'atteignent pas de cible, qui n'atteignent pas la pensée de ceux qui
décident de bombarder.

Miró parle aussi d'un art anonyme, collectif et personnel. Il dit qu'il
signe toujours ses toiles au dos, "jamais sur la surface peinte". Il
dit racines, et pas patriotisme, et ce qui est certain c'est qu'il a une
façon de souffler sa parole musicale, comme des jets, des à-coups portés
avec la voix, par salves. Il dit souvent "Mais oui bien sûr ! C'est
évident !", mais ce n'est pas réellement péremptoire. Ce serait plutôt
des expressions d'alarme, comme des sirènes, des cornes de brume, un son
qui dirait Où êtes-vous, vous qui êtes d'accord avec moi, qui êtes sur
la même longueur d'onde, où êtes-vous, que je me sente moins seul ? Un
parler mélodique qui jette des cris amples un peu difformes, tenus sur
une seule note, inquiets, comme ce que j'imagine être, pourquoi pas, les
cris d'un canard turquoise thérapeutique.

# Entre parenthèses

(Quelquefois, relativement souvent, je pense à A resté sur le sable
là-bas, surtout tôt le matin ; je le vois ratisser la plage, dessiner
des lignes parallèles parfaites, avec leurs crêtes de sable clair car
déjà sec, plus pâles que la couche brune qui les supporte ; et même sans
en être sûre car je ne l'ai jamais surpris en train de le faire, je
crois qu'il repasse aux endroits où son pied s'est imprimé pour effacer
son empreinte et que réapparaisse le réseau de lignes tressées ; je le
vois enfant, maigrichon, relever son pouce d'un coup sec pour faire
voler une bille le long d'une route de sable qu'il a tracé du coude ou
du plat de la main ; pour que les constructions de sable soient encore
plus lisses, il faut actionner le levier de la fontaine, l'eau dégorge,
sort en bas par une petite ouverture en tunnel et court, elle recouvre
les pores, les minuscules incidents, les annule, la langue d'eau mêlée
au sable brun, épaisse, égalise tout et elle semble d'une telle force
qu'on est étonné qu'elle doive à un moment s'arrêter ; si on l'actionne
furieusement, la fontaine grince, fait déborder le circuit creux façonné
pour les billes, déferle dans les virages, submerge les croisements de
routes qui mènent toutes (après Rome) au sous-sol d'un hôtel en
construction (il ouvrira l'été prochain) ; l'eau se perd dans l'avenir,
s'engorge dans un territoire sombre qui semble illimité car elle y
disparaît, elle s'y engouffre à flots sans réussir à le remplir, elle
tombe par un soupirail ; des tiges rouillées dépassent comme le feraient
les griffes d'un animal oublié là, malformé, inconnu, figé dans une
léthargie millénaire ; A s'est endormi d'un coup ; il est tombé ; il se
tenait le bras ; et avec lui les lignes droites et fragiles, les crêtes
claires et les fontaines furieuses sont tombées ; des mots sans assise,
issus de lampions morts, sont apparus avec les faire-part de décès, et
ils ne montraient rien, ni la mer ni les digues, ni les fêtes du 15
août, ni les cercles d'ombre des parasols au-dessus de lignes tracées
tôt le matin quand la plage est déserte, qui ressemblent à du Kandinsky
; je pense souvent à A ; c'est quelque part en noir et blanc, quelque
part triste et très joyeux ; sans importance, irremplaçable, sans rien
de remarquable ; quelque chose à faire tourner dans sa tête sans savoir
comment faire pour le dire, le décrire, à ranger dans le même tiroir que
les amas bombés des gouttes d'eau sur la table en terrasse le matin,
indescriptibles ; ce n'est rien, vraiment rien, une douleur vive et puis
ça passe, comme un bleu, on le touche, c'est encore douloureux, ça n'en
finit pas de blêmir, c'est passé du violet à l'orangé, au jaune, un
grand rond au milieu du torse, et c'est aussi étrange qu'une métaphore
illogique, que des milliers d'années vécues, qu'un trou en plein centre
de l'œil. "*L'an six cent de la vie de Noé, le second mois, le
dix-septième jour du mois, en ce jour-là toutes les sources du grand
abîme jaillirent, et les écluses des cieux s'ouvrirent*.")

# Aimez-vous le vélo, les montagnes violettes et les effilochures ? moi oui

Je suis la seule, parmi tous, qui habite ici, plus bas, dans la cour. La
seule qui sache que très tôt le matin la fenêtre du restaurant reste
ouverte --- reste ouverte toute la nuit, et que c'est différent chaque
matin. Que demain, un éclair orange clignotera, celui d'un témoin
lumineux qui renforcera les courbes des marmites sous la télé éteinte.
Qu'une des serveuses arrivera en vélo et le déposera contre le mur, près
des poubelles. Avec tous ceux assis en rond ce soir-là, les culs ronds
des marmites et les roues de vélo, décidément cette histoire de cercles
prédomine.

Cosmogonie est un mot rond, mais ce n'est pas un mot rétréci. Le monde
d'une cosmogonie est bien plus grand que ma cour, même si je ne peux en
saisir que ce qui passe à proximité, des indices faibles. Je sais que
les récits façonnent le monde. Je sais que récit et récif se touchent de
très près, distanciés seulement par la graphie d'une lettre (d'où les
drames qui nous guettent --- c'est pareil pour fiction et friction). Je
sais aussi que les mythologies sont personnelles, que chacun les
réinterprète. Les miennes fabriquent des cercles qui englobent plusieurs
vies --- celle du matin, celle de la nuit tombée, celle des trouées
jaunes sous les arbres, celle du sommeil, celle de la maison près du
bassin, loin de chez moi, où j'ai parfois dormi : pardon, ici je vais
prendre un instant pour en parler, cette cosmogonie près du bassin
m'étant en quelque sorte maintenant inaccessible, en tout cas
différente, en tout cas difficile à rejoindre, c'est pourquoi
lorsqu'elle émerge parmi les autres, parmi les idées traversantes et
les ah-oui qui ne sont pas censés durer parce que ce ne sont que des
étincelles de je-me-souviens, je m'y arrête de la même façon que je
m'arrête de faire quoi que ce soit lorsque j'entends *La Valse triste*
de Sibelius.

Près du bassin, voilà comment c'était (et, au fur et à mesure que j'en
parlerai le temps se raccourcira, je veux dire concrètement, les verbes
passeront de l'imparfait au présent, leur graphie sera ainsi réduite,
ne croyez pas que c'est organisé, maîtrisé, contrôlé, je le constate
après coup, exactement comme sur un chantier on fait le point en fin de
journée, on se promène pour voir l'avancée des travaux, des tuyaux
sortent de partout, il y a des naissances partout, des possibles
partout, avec de minuscules gravats, poussières, la matière malaxée,
puis tout à coup une fenêtre, enfin, je constate que passer du passé au
présent est l'indice de quelque chose, de quelque chose situé du côté
de la gorge, près du larynx, là où se logent les émotions fortes) : les
deux tilleuls montaient côte à côte sous les fenêtres pour étaler une
sorte de parapluie de feuilles là où leurs branches se rejoignaient.
Alors on pouvait rester à regarder pleuvoir sur le bassin, dehors,
sans se mouiller. On pouvait faire la liste des rosiers tout autour,
s'étonner des noms différents qu'ils portent. Ensuite monter au grenier,
dans la pièce avec l'œil de bœuf, pour chercher sur les étagères le bon
pinceau, la bonne encre violette (violette comme la montagne au loup).
Prendre un peu d'eau, diluer l'encre. Une goutte violette tombe, on
incline le papier, c'est un iris qui apparaît. Iris, c'est justement le
prénom donné à un enfant mort avant d'être né, qui ne ressemble pas à un
enfant mort, mais à une impression douce, une possibilité lointaine, un
point d'interrogation qui s'est éloigné et n'est pas triste. Derrière le
bassin les herbes sont très hautes et il y a un terre-plein fait de
pierres accumulées, une sorte de petite stèle. On peut y déposer les
vêtements des ancêtres : alors celles qui habitent la maison près du
bassin ont décidé de sortir les manteaux et les robes des armoires où
ils dormaient sans but, de les déposer là pour les laisser être mangés
par la pluie et le temps. Les vêtements anciens retrouvent leur texture
primitive près du bassin, comme un retour aux sources qui s'emplit d'air
dans les peluches et les coutures défaites. Et comme ils deviennent
autre chose (ourlets, replis, motifs, tout se décolore et s'agglomère
d'une nouvelle manière), ce sont des vêtements au futur. Iris s'y trouve
sans doute avec toutes sortes de choses obscures.

(Ce n'est peut-être pas lié, ou ça l'est au contraire, mais) je prends
ce mot, "cosmogonie", à cause des fractales et des nervures de
feuilles qui reproduisent à une autre échelle la forme de la plante qui
les porte, à cause des amas de galaxies qui forment à elles toutes une
nouvelle forme en forme de galaxie (ce qui est quand même, quand on y
pense, quand on s'y intéresse, une source d'étonnement terrible).
"Petite", parce qu'étant mienne, c'est une cosmogonie partielle à qui
il manque des morceaux, un petit monde limité aux murs de ma cour, à une
portion de rue passante, à la fenêtre du restaurant ouverte la nuit, à
l'entrée d'un immeuble collectif où, au 1^er^ étage, s'assoient en
cercle la blonde comme il faut avec les autres. "Petite", parce que
tout ça n'a aucune valeur. J'aime les choses sans valeur, le
journalier, le passant, la voisine. Je ne porte pas un prénom rare mais
un prénom commun, un prénom collectif. Pour faciliter la lecture, je dis
"je", mais ce serait une bonne chose que cela puisse être changé. Que
ce soit vous qui décidiez. Et que selon votre lecture, votre prénom
remplace mon prénom. Ce serait une bonne chose que mes phrases puissent
être remplacées par d'autres phrases. Qu'elles ne soient pas
particulières, mais uniques, communes et collectives.

Ce serait une bonne chose aussi que ce livre puisse changer en temps
réel, se modifier, que les phrases disparaissent ou bien soient
remplacées, que surviennent des trouées, des accidents. Par exemple on
lit un paragraphe qui parle d'une pomme de pin, on laisse passer
quelques minutes, et lorsqu'on le relit, ce même paragraphe décrit la
tête d'un animal sauvage dessiné sur le mur par la mousse et la pluie
polluée, avec son œil surtout, qu'on dirait aux aguets. Et cet œil était
là pendant qu'on se penchait sur la pomme de pin, simplement on n'en
savait rien. Le livre le saurait, lui, il le saurait et il en tiendrait
compte, pour nous, à notre place. Il travaillerait, à sa façon, pour
tenter d'augmenter la quantité d'air respirable, comme c'est déjà le cas
avec ces livres qui nous suivent dans le temps, quand d'une année à
l'autre on les relit et qu'autre chose nous parvient malgré les phrases
identiques ; on s'en rend compte : ce qui a été lu quand on est encore
tendre n'a pas la même texture s'il est relu à un âge non tendre, donc
dans la dureté (durée et dureté sont des mots proches).

Pour faire de petites cosmogonies en relief, on peut utiliser du papier
mâché. Les techniques sont nombreuses, et les recettes aussi. Au Mexique
on froisse des prospectus en conservant leur couleur d'origine. Les
encarts de publicités et les photos d'agences de voyages deviennent des
bandes volumineuses qui se chevauchent sans tenir compte de
l'orientation. C'est très coloré. Au Japon on peut ajouter au papier
mâché l'eau de cuisson du riz (qui aura cuit plus d'une heure) ce qui
servira à la fois de liant et de durcisseur (la technique des Kanzashi,
les fleurs en tissu une fois imbibées deviennent rigides). On peut aussi
mettre dans la pâte à papier de la farine, du sel, du plâtre, du
détergent, de l'huile. Le mélange obtenu changera en séchant. Il durcira
et il s'éclaircira, on ne peut pas vraiment prévoir. On peut le modeler
en petits disques, c'est ce que je fais. Je ne sais pas encore si je
garderai leur périmètre très lisse, ou si je laisserai le cercle
s'effilocher. Je voudrais inclure sur sa surface une multitude de choses
dont je n'ai pas encore idée. Il faudra que ça vienne de soi (commun,
collectif et unique), que ce soit facile à trouver (*ready made* et art
du moindre), que ça passe à proximité (fil, caillou, éclat de coquille
d'œuf, etc.). Chaque disque serait une petite cosmogonie différente,
porteuse d'un monde spécial, précis et limité, sans importance mais
présent. La cour avec le restaurant, la rue passante, l'étage, sont des
sortes de cosmogonies. Les tables des brocantes aussi. Les phrases sont
des cosmogonies graciles et résistantes, incroyablement renouvelables.
C'est ce qui m'appelle vers elles, je crois.

# En aparté

On ne sait pas comment naît une pensée, on connaît les parties du
cerveau activées lorsqu'une pensée est en mouvement, mais on ne sait pas
comment elle apparaît. Les chercheurs cherchent. Ils allument des écrans
qui éclairent des zones, rouges, bleues, ce sont des constellations qui
racontent des fulgurances comme les chansons de geste racontent le
Couronnement de Louis, des représentations, des images animées ou
statiques, des gris-gris échangés sous le manteau, des incantations à la
pluie qui demandent à ce que la terre se gorge d'eau pour que les semis
soient fructueux et l'avenir bénéfique. On suppose qu'une meilleure
connaissance du cerveau nous dédouanera de notre férocité. Que les
images statiques ou animées colorieront en bleu, en rouge et en
constellations les zones les plus propices à la paix dans le monde. Nous
sommes des Miss Univers qui avons hâte de donner les bonnes réponses aux
questions essentielles, la paix, l'amour, la joie, car nous voulons
qu'on nous choisisse, qu'on nous accepte, qu'on nous aime.

Pour ma part, en ce qui me concerne, de mon côté (quant à moi), je ne
veux rien de plus que faire miroir. Je ne veux rien de plus que
surprendre le reflet de la permanence et de l'impermanence des fluides
dans lesquels je m'exerce, même si ces fluides ne font pas office de
fluides à l'œil nu, même si c'est une coulée éparse de fluides séparés
les uns des autres par des données méconnaissables, des cellules
disparates, parcelles, fragments et éclats de coquilles composites de
couleurs indécises. Rien de plus que saisir dans l'espace d'un reflet
ce disparate, le capter, le répercuter, et pourquoi pas l'énumérer (par
ordre croissant si cela vous semble plus pratique, par exemple 1/ un
écossais en kilt avec sa cornemuse sur une plage --- et tout seul il
jouait face à la mer ---, 2/ deux trottinettes appuyées contre un mur,
comme des dents déchaussées, 3/ Andy Warhol à l'entrée du Jardin
botanique --- enfin son double ---, 4/ quelques ampoules à basse
consommation achetées par un vieil homme en béquilles tandis qu'est
diffusé dans les rayons le tube de l'été d'un groupe maintenant séparé
depuis 10 ans).

Vous me direz qu'au bout d'un moment, à force de faire rentrer dans un
pauvre reflet des gens, des gens, encore des gens et les objets que ces
gens transbahutent, ou aiment, ou abandonnent, on ne voit plus rien.
C'est bien possible. C'est bien possible que l'ensemble obtenu (ma
table de camping vue d'avion) en arrive à ressembler à une "galette
d'asticots" (ce que disait Paul Claudel du *Bain turc* --- à ce propos
*Le Bain turc* est une toile ronde, rond, disque, cercle, on n'en sort
pas voyez-vous ? --- j'ajoute que l'avis de Paul C. m'indiffère, mais
pour ce qui est de sa sœur, Camille C., c'est autre chose --- à ce
propos, quelqu'un-quelqu'une s'est emparé d'une reproduction du
*Baiser* de Rodin et l'a entortillé de spaghettis, ce qui à mes yeux
constitue un geste vivifiant).

Je ne veux rien obtenir d'autre qu'un reflet de miroir qui fasse
sonner la résonance sensible des centres sensibles, je veux dire des
gens, et cela malgré les obstacles que sont les mains artificielles et
les trains aux rails contraignants. Cette idée de choses quelque part
reliées, l'existence possible de conduits obscurs, souterrains, de
galeries d'une mine dont ne dépassent que les silhouettes des
chevalets, seules excroissances visibles, furtives, irraisonnées, comme
les preuves de présences justes, me rassérène.

# Et qu'est-ce qu'on sait du poids des couvertures de survie ?

Les objets s'alourdissent. Leur surface se gonfle de prismes qui
s'ajoutent, un peu comme chez l'ophtalmo, lorsqu'il superpose des verres
ronds devant un œil, qu'il les oriente sur ces sortes de lunettes
archaïques qu'on nous pose sur le nez, et la vue (un œil à la fois) se
modifie.

Arrive toujours un temps où se modifient la prégnance et la porosité, la
densité des objets pris dans les brocantes. Par exemple un antique fer à
repasser : un manche de métal courbe scellé par deux vis sur son socle,
gravé N^o^ 5 et W (N^o^ 5 c'est le parfum de Marilyn, W l'île souvenir de
Perec). Ça pèse lourd. Ça pèse du poids ancien des premières fois, quand
je suivais le chemin de terre qui contournait les bâtiments pour arriver
dans les coulisses de l'usine, une fonderie mécanique, où des piles de
pièces de métal de toutes sortes allaient être fondues le lundi. Le
dimanche on avait le droit de s'y promener (puisqu'on habitait dans
l'usine, sur le territoire de l'usine, collés à elle, on était sa
propriété). On passait entre les monticules presque triés, ou à peu près
triés, de vieux radiateurs et d'anciennes machines à coudre aux lettres
SINGER enluminées sur leurs coques noires munies de vieux volants à
poignée en bois pivotante. Et les fers à repasser y étaient, il y en
avait beaucoup, tout un tas. Les mêmes que celui-là, celui avec N^o^ 5 et
W. À cette époque je pensais (ils étaient si nombreux) que ces objets
n'avaient pas d'importance. Je n'imaginais même pas qu'ils avaient dû
servir, je ne pensais pas aux mains et aux torchons fantômes qu'ils
contenaient, aux chemises à col droit, aux robes, aux brûlures aussi, je
les voyais comme de simples objets, utilisables, accumulés et, par
l'accumulation visible en monticules, déroutants.

C'est le genre d'objet qui se déplace. Il a basculé le long d'une
ligne temporelle. Il a été remis d'aplomb sur une table de brocante,
soupesé, ramené à la maison. "Il pèse lourd", c'est ce que j'ai dit.
Je ne pensais pas que c'était si lourd. L'ophtalmo a ajouté des disques
correcteurs, beaucoup. C'est qu'il en faut. Un pour les mains
oubliées, les torchons, les robes, les prénoms, les visages oubliés. Un
pour l'enfance reconstruite. Un pour la distance, son élasticité, quand
le lieu se reflète dans la colonne de l'œsophage ou quelque chose par
là, près des poumons. Ma tête penche vers l'avant (c'est très lourd), à
cause de ces prismes sur un œil, mais un œil à la fois. L'autre œil est
égaré, il passe sur les objets usuels, les quotidiens, sans repérer
celui qui pèsera ou basculera le long de la ligne temporelle suivante.
Cet œil-là passe aussi sur les objets laissés pour compte et entassés.
Des paquets de lettres d'amour. Des pneus. Des couvertures de survie.

L'autre jour, dans un cercle d'herbe qui n'accueille d'habitude que
les moineaux, quelqu'un-quelqu'une avait planté une dizaine de piquets
avec, fichés à leur sommet à la place des drapeaux, ces couvertures
brillantes qu'on distribue aux naufragés : c'était joli, on pouvait
mieux comprendre à quel point c'est fragile (ça se déchire à peine on
respire à côté) et mieux entendre (ça bruisse en continu, un son, un son
de frottements constants, doux, insistants, légers, qui peu à peu
s'alourdit de sortes de stridences, d'urgences --- ça se remplit,
écoutez, ça se remplit de chants ou plutôt de proférations, des
récitatifs acérés, monotones, éreintés, entêtés, inarrêtables, et le
nombre d'oreilles bouchées qui ne les entendent pas, je préfère ne pas
en parler, c'est la marque d'une cosmogonie haïssable. Maudite).

# Où il sera question d'œufs cuisinés, de draps, de taxis et de floraisons blanches

La blonde comme il faut est spécialiste du temps d'attente, car
attendre c'est ce qu'elle fait le plus souvent. Elle attend qu'on
vienne la chercher, elle est très pointilleuse sur les horaires (les
autres moins). Elle est prête, le sac à la main elle fait des
aller-retour entre le salon, l'entrée et la buanderie où pour
accompagner l'attente elle lance le programme de la machine à laver,
range le flacon d'adoucissant, ici un miroir, elle vérifie que son fond
de teint fluide velours suprême ne s'arrête pas net sous le menton en
lui laissant le cou trop pâle. Elle attend l'heure des rendez-vous de
travail confirmés par son agenda. Elle attend des coups de téléphone,
des contacts par mails, des transmissions de dossiers, des grilles de
tarifs selon les périodes bleues, jaunes, rouges. Elle attend la venue
de ses petits enfants, le bébé et la petite fille de quatre ans qui aime
l'andouillette. Elle attend chez le gynécologue et ensuite elle attend
les résultats de la mammographie le torse nu dans une cabine, les bras
croisés devant son sein pour le cacher, par peur que la porte s'ouvre
brutalement. Elle attend qu'on lui dise que tout va bien. Elle attend
de son père qu'il lui dise qu'il regarde Wimbledon, qu'il n'a pas
trop mal à la hanche aujourd'hui et qu'il n'est pas malheureux, même
seul, même s'il pense être abandonné. Elle attend qu'un SMS la
prévienne que sa carte d'identité est prête (un renouvellement), mais
elle n'attend pas vraiment le renouvellement au sens propre, elle
attend que les modifications gardent un statut inoffensif, et puis
d'aller à Vancouver cet hiver. Elle attend que s'articulent les
données de sa vie sans esclandre, que les morceaux (qui ne sont pas des
briques, c'est trop solide) se placent fluide velours suprême comme
dans les films d'animation réalisés image par image, elle attend
qu'image par image la vie la laisse faire ce qu'elle sait faire le
mieux, attendre, attendre avec patience de bonnes nouvelles. La
locataire du 1^er^ chante *Oh baby baby it's a wild world*. C'est le
sixième jour. Elle se repose. Elle est bien fatiguée d'avoir tout créé
jusque-là. Elle cuisine des œufs. Dans la mythologie finnoise, le monde
naît à partir des fragments d'un œuf déposé par un canard plongeur ---
donc thérapeutique --- sur les genoux de la déesse de l'air, mais la
locataire du 1^er^ n'en sait rien, même si de l'air entre par la
fenêtre. Un client lui a offert une boîte de biscuits normands avec un
couvercle en fer décoré d'un tableau de Félix Vallotton.

Ce sont des lavandières sur la plage. À première vue, ça ne ressemble
pas à des femmes mais à des éclats de roches qu'on dirait rassemblés là
sous l'effet d'une grosse vague, et le vent a cessé. Leurs robes noires
font des brisures de pierre ou de quartz (de l'onyx), et les draps (des
triangles blancs et inégaux) sont des lames de nacre, ces coquilles
d'huîtres planes qu'on ramasse, si usées qu'on dirait des feuilles.
Donc même en s'approchant pour mieux comprendre les détails des gestes
et des corps, on garde en tête l'idée d'un habitat marin avec ses
occupants, un banc de coraux noirs et blancs, un lieu vivant, d'une vie
qui n'est pas réellement humaine, ou plus exactement d'une vie qui ne
se limite pas au genre humain. Ce n'est pas que ces femmes auraient
cessé d'être des femmes pour devenir des morceaux de paysage. Ce serait
plutôt que ces femmes sont d'autant plus femmes d'être là, actives à
l'essentiel, allant à l'essentiel, montrant l'essentiel, et l'essentiel
vit dans les rochers. Certaines sont fracassées en tout petits morceaux,
elles ont tellement souffert que c'est en graviers ou en grains de sable
qu'elles se montrent. Elles sont en première ligne, à subir les marées,
les tempêtes, les rouleaux tumultueux (qui montent plus haut que la
neige tombe à Detroit). Elles ont été rassemblées là par hasard (le
terme échoué semble juste, même si elles ne sont pas réellement
échouées, même si leur volonté d'être là est encore en action,
intrépide, le vent et la marée, le jour suivant, les nécessités les
repousseront toujours plus loin, elles resteront, c'est ce qu'elles
savent faire le mieux, comme l'attente pour la blonde comme il faut, il
faut être très volontaire pour savoir attendre, très concentré pour ne
montrer que le fluide des accommodements aux temps et aux intempéries,
le fluide de l'adaptation et de la résistance, la volonté fluide de
rester, même fracassées, au cœur des graviers et du sable, avec la
ténacité de se fondre dans le paysage en lavant la nacre des draps).

À Québec, une application propose d'afficher le visage du chauffeur de
taxi que l'on appelle. On pourra le reconnaître, le retrouver parmi la
myriade de petites pierres fracassées ou intactes qui conduisent des
taxis. Un cintre se balance accroché à un étendoir. Beaucoup de gens
dehors cherchent à savoir à quoi ils sont bons et ce qu'ils savent
faire. Beaucoup d'entre eux se savent échoués sur des plages et
comptent sur des vents qui ne leur seraient pas hostiles. La blonde
comme il faut télécharge l'application sur son téléphone, car elle
compte visiter Québec. La blonde comme il faut compte sur une phrase
apaisante, tout va bien, les bras maintenant croisés sur ses seins de la
même façon, avec la même pression portée aux deux, sur celui qui est
remplacé et sur l'autre, sur celui qui ne contient que sa chair et sur
l'autre, sur celui qui a été donné, placé, et que l'enveloppe de sa
peau recousue fait tenir à la même hauteur que l'autre, et l'autre dont
elle suppose qu'il possède ce genre de floraisons blanches et roses vues
en coupe dans les manuels de SVT (chapitre reproduction).

# Dans la partie qui va suivre, on ne dira pas ce que wechsler pense de l'intelligence (qui à ses yeux n'est pas une capacité unique, mais bien un agrégat de plusieurs traits humains), c'est bien sûr une marque d'étourderie de ma part

La locataire du 1^er^ a un fils qui habite à une dizaine de kilomètres
de la main artificielle, au Canada. Il est venu visiter sa mère quelques
jours. Il n'était pas à la soirée, n'est arrivé que le lendemain,
après avoir passé beaucoup de temps entre avion, navette, métro, train,
bus, où il en profitait pour regarder les flyers d'ici d'un œil neuf.

"Insouciant", c'est un adjectif qui pourrait lui sembler adapté, alors
qu'il ne lui correspond pas du tout, une sorte d'adjectif affiche de
cinéma et la bande-annonce du film est trompeuse. Ses pensées vont à
cent quarante à l'heure, mais réellement. Il possède une carte avec son
prénom et son nom qui atteste que son QI est l'un des plus élevés
d'Amérique du Nord. Ça pourrait sembler cool, et ça l'est quelquefois,
surtout lorsqu'il tente de ressembler le plus possible à son adjectif
insouciant. Je vais l'appeler le jeune, ce sera plus simple. Ça pourrait
sembler cool ce QI qui dépasse les 140 sur l'échelle de Wechsler et le
jeune n'a qu'à montrer sa carte pour qu'on le regarde autrement, un peu
par en dessous, un peu inquiet, qu'est-ce qu'il va penser de moi lui qui
est si intelligent, ou bien qu'est-ce qu'il va remarquer que je n'ai pas
vu lui qui est si intelligent, est-ce qu'il n'est pas comme le faucon
pèlerin avec sa vue qui pourrait repérer une souris à deux mille mètres
et foncer droit dessus sans s'écarter d'une ligne aussi belle que si on
l'avait tracée sur la table inclinée d'un dessinateur industriel, oui,
très cool, mais ça ne l'est pas. Le jeune est très grand, beaucoup trop
grand et maigre aussi, et barbu, ce qui lui donne cette allure
longiligne et légèrement flexible qu'il n'est pas, cet air d'insouciance
qu'il n'a pas, parce que son disque dur est en connexion permanente avec
l'extérieur et doit se réguler à chaque seconde et se reconnecter à
chaque seconde sur nous, qui ne comprenons pas comme lui. C'est comme
s'il cherchait une borne 5G tout le temps, ou comme s'il faisait bouger
une parabole continuellement en direction d'un signal à attraper et, à
cause des orbites de toutes les planètes qui se croisent et des
météorites qui passent n'importe comment, le signal est rompu, ou
tressaute, ou s'efface, il est masqué, il faut toujours que le jeune le
rattrape et se réadapte. Non, pas insouciant. Ça crée des zones
flottements autour de lui. Lui qui flotte, à cause de sa connexion
flottante, et nous qui hésitons parce qu'on ne sait pas si on saura
s'adresser à lui. Parce qu'on serait trop bêtes, trop limités, ou à
l'inverse, parce qu'on serait trop compliqués d'être simples. C'est que
le jeune doit ré-évaluer sans cesse notre niveau, sans doute que parfois
il ne comprend pas ce qui nous échappe, ou il ne comprend pas ce qui
nous titille, ou il ne comprend tout simplement pas, c'est comme nous,
quand nous ne comprenons pas tout de suite que le chat veut sortir parce
qu'il miaule sur le même ton monocorde sa demande trop peu importante
pour nos cerveaux humains préoccupés par autre chose. C'est une économie
de l'attention quand on est avec le jeune. Il faut faire attention à lui
et attention à soi, les deux en même temps. Et ça n'est pas facile.
Surtout que ce n'est pas une pratique habituelle. Assez souvent, avec un
peu de pratique, on peut renoncer à faire attention à soi, une fois
sanglé dans son costume social on n'a qu'à se glisser dans le solide
toboggan social basé sur nos rapports sociaux humains et civilisés, on
fait semblant par politesse sociale d'être intéressé, on a la politesse
éblouie ou la politesse compatissante, on prétend être concerné, mais
avec le jeune c'est impossible. C'est comme si son QI nous renvoyait une
image diffractée du type *Dame de Shanghai*, et qu'il fallait se repérer
dans les morceaux désassemblés. Et comme c'est symétrique, je veux dire
comme on sait que lui aussi en fait autant de son côté, cela donne des
conversations, je veux dire des flottements et des ondoiements de bulles
compliquées de reflets, des bulles qui éclatent tout le temps et se
reforment tout le temps, mais lentement. Des bulles qu'on pourrait voir
monter mais qui ne montent jamais, simplement elles s'élèvent un peu du
sol, à hauteur de nos têtes (assis sur la terrasse, le long des
treillages de bois peints en vert qui ne supportent aucune plante
grimpante) et elles stagnent, ou redescendent ou éclatent
mystérieusement, puis se reforment à l'identique. Le jeune est une
petite cosmogonie à lui tout seul, et il est seul, bien seul. C'est
peut-être ça qui nous saisit. Même s'il se retrouve mêlé aux passants,
aux touristes dans la rue commerçante, qu'il remonte l'escalier qui
longe le musée ou qu'il passe sur le parvis de la mairie ou sur le pont
au-dessus du petit filet d'eau près du parking, ou même le long de
l'hôpital en travaux, oui, même si pendant qu'il se déplace il est
mélangé à tout le monde et que sur une photo on aurait du mal à le
pointer pour l'encercler comme quand on a trouvé Waldo, lui il est seul.
Si les photos de foule étaient réalistes, il serait seul dessus, au
centre d'un grand flou blanc.

Un grand flou blanc, ça je ne sais pas le faire avec de la pâte à
papier. J'essaye cependant. L'autre jour, j'ai pris une plaque de
carton et je l'ai couverte de filaments, de torsions de papier humide et
blanc, comme des amalgames de blanc, comme des ressorts et des blancs
d'intérieurs de têtes, comme une complexité de signaux blancs qu'on
aurait mis en tas sans les débrouiller sur une table. Mais ils ne sont
pas flous, tous ces signaux, juste emmêlés. Et ils ne sont pas collés
pour former un rond non plus. Ce n'est pas une planète. C'est une
cosmogonie rectangulaire, et ça la rend souffrante. Comme si elle avait
de la peine, tiraillée de ses quatre angles en pointes. Un peu écartelée
d'être aplatie. Comme nous.

# Très court chapitre qui pointe ex abrupto la possible inadéquation d'un titre avec ce qu'il est censé annoncer

Effectivement.

# Et pourquoi pas un roman sur la flûte et rien d'autre

Les lumières multicolores projetées en plusieurs langues sont dites au
sol dans le grand jour alors que c'est l'obscurité qui les fait naître
normalement. Le mot Liberté s'affichera bientôt, c'est promis, ce
soir, cette nuit, en grand. Un guide professionnel, équipé d'un micro
en forme de pastille Valda, parle dans les oreillettes d'un groupe
amateur sagement aligné : "Je vous emmène maintenant vers la maison à
la sirène." Une fiction appelle la suivante.

Ma montre est tombée sur le sol. Je ne sais pas si c'est vraiment utile
d'écrire sous pseudonyme, comme si les personnages n'étaient pas moi,
comme si la lumière automatique sous le porche n'était pas programmée
pour s'allumer quand on passe tout près. Comme si tout n'était pas une
question de reflet de lampes dans une fenêtre sur les culs des
casseroles. Comme si le son de la pluie qui heurte les feuilles de
plastique des faux arbustes de la terrasse ne ressemblait pas au bruit
du feu qui crépite dans une cheminée, ou comme si l'abeille morte sur
l'appui de fenêtre n'allait pas rester là, debout, debout et vide des
centaines d'années, ou n'allait pas disparaître mangée de l'intérieur,
ou n'allait pas se trouver écrasée (si fragile) par le bruit des talons
dans la rue. Les bruits de pas cessent. Les bruits d'eau qui
dégoulinent dans la gouttière non, et un oiseau au bord du toit semble
se réinstaller pour ne pas tomber, sûrement il se retourne dans la
gouttière comme nous le matin, quand on reste sous les couvertures
encore un peu. La vie sous les couvertures de gouttières ou de bruits ou
de carapaces d'insecte est une donnée très étrange, en résumé. Ou bien
c'est autre chose qu'un résumé, c'est une traduction. Donc un travail
d'acclimatation. Ça ne rentre pas dans une langue pour venir se former
dans une autre sans perdre sa ponctuation ou les petits connecteurs
logiques auxquels on est habitué. Il y a des décisions à prendre. On
déplace le complément d'objet pour que ce soit compréhensible, on
s'arrange, c'est fou comme on s'arrange, constamment, pour que ce soit
compréhensible, pour que nous soyons compréhensibles des autres qui
cherchent ou ne cherchent pas à nous comprendre, et fou comme on
s'arrange aussi pour rendre compréhensible ce qui ne peut pas l'être.
Quel travail. Et sans QI de 140, ce n'est pas un ouvrage simple ni
reposant.

Sans compter tout ce qu'on rate.

Par exemple, je rate toutes ces petites cosmogonies potentielles, celles
qui passent, qui existent, que j'oublie d'attraper.

La cosmogonie de la feuille de lotus : ample et plate, et lorsqu'on la
trempe entièrement dans l'eau, elle en ressort entièrement sèche --- pas
presque sèche, pas légèrement humide, entièrement sèche ---, c'est la
technique du tapis de fakir dit-on, qui bloque les gouttes d'eau au
sommet de ses pointes.

La cosmogonie des princesses scythes de l'Altaï, tatouées de l'épaule
jusqu'au coude d'animaux fantastiques, chevreuils à bec de griffon et
cornes de bouquetin. On étudie leur peau dans un musée de la ville, mais
les habitants craignent que maintenant la fin soit proche, les chiens se
mettent à hurler, les vitres à tinter, la terre ondule, ils veulent les
ramener où elles sont nées avant le dernier cataclysme.

La cosmogonie de la femme qui cherche ses clés, du petit garçon qui
tient l'accoudoir d'un fauteuil roulant tout en calant une bouteille
d'eau au creux de son épaule, tournant autour de l'arbre remarquable
(selon le label de l'Office National des Forêts), un arbre si vieux
qu'il connaît la signification des mots calèches et hauts-de-forme, et
dont les racines sont si profondes que personne n'y pense.

La petite cosmogonie des ajivas (en forme de pierres, ou sans forme,
comme l'espace), arrêtée à un stade médian, patient, entre l'inanimé et
l'animé, en équilibre entre démons et dieux.

La cosmogonie de la seiche qui voit tout, car elle n'a pas de point
aveugle, elle voit tout à 360 degrés (de plus son œil fonctionne avant
que l'animal sorte de l'œuf, ainsi elle observe son futur gibier avant
de naître, avant de naître elle apprend à traquer, pas nous, nous
n'apprenons qu'au fur et à mesure, nous imitons quelqu'un qui lui
possède un point aveugle, son point aveugle s'ajoute au nôtre, le
nombre de questions s'accroît, s'étend, c'est à se demander si c'est
bien soi qui pense, ou si l'on est pendu au loin au bout du bec d'un cormoran
et c'est lui qui questionne).

La petite cosmogonie du chiffonnier et celle de la riveteuse, qui sont
un peu brouillées car le temps a passé, mais pas inaccessibles.

La petite cosmogonie des joueurs de clairon, en costumes folkloriques,
qui commémorent un jumelage, la tête couverte d'un plumet et la
poitrine lestée de blasons significatifs.

La petite cosmogonie de Marilyn ; celle de sa chambre, et le lit et le
dos de sa tête avec des cheveux blancs (ils étaient blonds, on le sait)
et les draps du sommeil tout froissés et la table de chevet avec les
pilules, comme dans les films, et ce n'est pas un film, et le corps, ou
la fiction du corps sous la couverture basiquement grise qui emmène sa
forme de momie sur le brancard n'est pas une production de la 20th
Century Fox. C'est une cosmogonie terrible parce qu'elle n'a pas de
fond.

Tout ce que je n'écrirai jamais forme une cosmogonie complexe, un monde
refusé et pourtant disponible dans une sorte de reflet de bulle
disloqué, et ça n'a pas de fin.

Par exemple, je pourrais commencer une liste de romans jamais écrits,
mais, la finir, je ne pourrai pas.

Un roman qui décrit jour après jour les ciels --- quel travail.

Un roman qui décrit rue après rue les portes --- quelle difficulté.

Un rêve, et le roman raconterait comment, depuis le rêve, on entend des
couleurs se lever, former des acrostiches, des formes cabalistiques
convertibles en ondes sonores.

Un roman calqué sur les mouvements de *La Grande Pâque* de
Rimski-Korsakov, ample, lyrique, avec des touches d'ocre, de blanc et
de brun qui décrieraient le paysage et les habitants des campagnes
russes.

La vie romancée d'un grand gaillard à la chevelure rousse plaquée et
tirée sur le front, avançant calmement dans des rues où personne ne
saisit sa ressemblance extraordinaire avec John Wayne, et comment son
chemin s'en trouve influencé au quotidien.

Un recueil de nouvelles avec, pour fil conducteur, des espadrilles usées
à semelles de corde fossilisées, la seule paire de chaussures d'une
vieille dame qu'on croiserait à plusieurs reprises et qui interromprait
l'avancée de la narration en posant des questions, Quelle heure est-il
? Quel jour sommes-nous ?

Un catalogue des outils tranchants (coutelas, scies, lames et sabres de
toutes origines) retrouvés au fond des tombes ou des grottes depuis des
temps immémoriaux et à travers tous les continents. Ils seraient
répertoriés précisément (datation, dimensions, poids, descriptif des
inscriptions décoratives). En haut de la dernière page le mot
"Conclusion", suivi de deux points, précéderait un espace vide à
remplir.

Un roman choral dont les multiples narrateurs seraient des papiers
chiffonnés sur le sol d'artères principales de mégapoles, et ils
s'exprimeraient chacun dans leur langage, racontant chacun ce qui les
entoure, et chacun parlerait en fonction d'où il vient, ticket de
cinéma, note de tailleur, facture d'installateur de volets roulants,
publicité pour des tacos, etc.

Un essai méditatif écrit sur les hauteurs de l'Himalaya, avec une note
de bas de page où serait mentionné le nom imprononçable d'un vieux
trapéziste tchèque incapable d'exercer son art dans les hauteurs et qui,
par conséquent, se produirait en spectacle dans les souterrains, les
égouts, les tunnels.

Un livre d'anecdotes. Des anecdotes simplement. Mais leur assemblage
créerait quelque chose de plus global. Par exemple on y lirait qu'une
plume grise de pigeon s'introduit dans une pièce en voletant par la
fenêtre ouverte et, quelques phrases plus loin, on apprendrait qu'un
pigeon monte une par une les marches de l'escalier devant la porte
d'entrée, et forcément on se demande s'il vient chercher sa plume,
alors on lui pose la question.

Une suite de textes de dix lignes écrits par une femme chilienne,
coincée entre Océan Pacifique et Cordillère des Andes, chacun de ses
textes ayant la particularité de n'avoir que dix lignes, seulement dix,
et d'être les seules traces résiduelles d'une femme enclouée au nom
perdu.

Un livre en forme de leporello dont chaque pli déplié s'ouvrirait sur
un autre livre, livre gigogne. À cause du manque de place, on ne
pourrait imprimer que les incipit des livres contenus dans ce livre, ce
qui provoquerait une sorte de symptôme de faim, de soif, de manque, de
désir insatiable de lire les chapitres manquants. Aussi, faute d'en
avoir la possibilité, on les écrirait soi-même.

Une fiction centrée sur un personnage précis qui se déguiserait sans
cesse en d'autres personnages tout aussi précis, chacun trimbalant sa
propre mémoire, ses propres spéculations, ses regrets, ses blessures,
ses joies, et l'histoire se perdrait en circonvolutions, surtout au
moment où certains personnages (ou plutôt certaines émanations du
personnage principal) entreraient en contact les uns avec les autres
pour échanger, en particulier leur point de vue sur ce que doit être un
personnage, à leur avis.

Un roman qui commencerait par une interjection et un point
d'exclamation, suivis de parenthèses entre lesquelles s'inscriraient
les raisons de ce cri porté au tout début, ainsi que les raisons de ces
raisons, ce qui les provoqua, sans occulter la source à l'origine du
commencement de cette réaction vive (et verbale) ajoutée à la vie
complète et tentaculaire de celui ou de celle qui prononça cette parole,
ou la jeta plutôt, et dont on n'apprendrait qu'à la toute fin qu'il ou
elle se trompait du tout au tout, mais alors complètement. Fin de la
parenthèse, suivie des mots "au temps pour moi".

Un texte bâti grâce aux livres d'une bibliothèque : il sélectionnerait,
dans chacun d'eux, une phrase, et viendrait l'assembler à la phrase du
livre suivant (selon un ordre soit alphabétique, soit chronologique),
pour qu'en les lisant à la suite, une certaine substance se dégage, ou
une forme de logique. Comme il y a des milliers de livres, il y aurait
des milliers de logiques et de phrases, tous genres confondus, qui
donneraient des milliers de pages, formant un tout d'un genre unique
puisqu'il les regrouperait tous (utopie).

Une thèse universitaire axée sur un territoire spécifique et qui,
s'appuyant sur les strates géologiques dudit territoire, exposerait les
correspondances et similitudes qui existent entre une couche de roche
granitique et les strophes en stridules d'un poème grec.

Un roman sur la flûte, et rien d'autre.

Le roman de la camionnette où s'affiche URGENCE OXYGÈNE (c'est vrai
que c'est urgent), ou celui d'un cabinet d'architecte dont la devise
est CRÉATEUR DE LIEUX DE VIE (ce qui interloque et mérite d'être
examiné).

Un roman sur une rame de métro qui suivrait en détail les trajectoires
qui y mèneraient, ainsi que les sorties de métro et tourniquets. Le
nombre de personnages serait croissant. À tous seraient donnés un nom,
prénom, une date de naissance, et une carte indiquant leur QI selon
l'échelle de Wechsler, puis l'échelle de Jacob, l'échelle de Krups,
l'échelle de Winggentrem, et le roman se terminerait par la biographie
de cet homme, Winggentrem, né en 1895 (à Oamaru, Nouvelle-Zélande),
dont le père était porteur d'eau.

Une biographie d'un Mozart du néolithique ou du Mozart précolombien.

Ou une biographie de n'importe qui. De toute façon peu importe, ce
serait toujours une biographie faite par moi, je veux dire faussée par
moi. Parce qu'on ne peut pas s'en défaire, je veux dire que ce n'est pas
par égotisme, c'est que la façon de regarder quelqu'un est toujours, et
c'est ainsi, limitée à la taille de ses propres yeux, les pupilles ne se
dilatent jamais vraiment, c'est un constat --- et si on a un trou dans
l'œil, c'est encore pire, la marge est encore plus réduite.

Une traduction de moi. Déjà, il faudrait que j'écrive un texte à la
première personne, et ensuite que je le traduise en moi, ce qui est
compliqué. Sans doute à cause de mes choix. Qui sont à peine des choix.
Je crois que le mot juste est "inclinations".

# S'arrêter pour un bilan provisoire me semble ici approprié

J'incline beaucoup, j'incline jusqu'à tomber parfois, j'incline vers
ce qui n'est pas cernable ni dicible. J'incline vers le couple de
plumes qui balancent attachées par un fil invisible à la rambarde de
l'escalier ; l'idée que je devrais le nettoyer en passant un coup de
brosse m'incline, car j'aime l'idée que je ne le ferai pas.
J'incline vers l'idée de me laisser-aller, d'être portée. J'incline
vers les formes, trouver des formes, même à ce qui n'est pas censé en
avoir. Par exemple un paragraphe : je le voudrais en forme de colimaçon
(mot qui me touche, j'incline tout particulièrement vers ce mot,
"colimaçon", et le fait qu'il me touche n'est ni cernable ni
dicible). Pour le dire sans ambiguïté, j'incline à ne pas comprendre
dans les moindres détails pourquoi j'incline. Parce que ne pas
comprendre est une sensation primitive, une force motrice du vivant qui
ne connaît pas son égal, synonyme de curiosité --- un mot qui
curieusement n'a pas de synonyme.

À l'angle du velux, il y a, coincée dehors, une cosmogonie de mousse
disponible, ronde, bombée et verte. Quand on s'approche pour mieux la
regarder (ce qui n'est pas très simple, le velux étant assez haut
au-dessus de l'escalier pour qu'on doive se mettre sur la pointe des
pieds avec un soupçon de vertige), on voit de petits arbres fins,
terminés par une seule feuille, comme dans un tableau de Magritte. En la
regardant on est proche (je suis proche) de croire que tout est lié.
Cette vision procure (me procure) une dose immédiate de joie et
d'énergie. Il n'y aurait qu'à surprendre tous les fils qui tiennent
toutes les cosmogonies entre elles pour tout comprendre. On se sent (je
me sens) vivante et énergique et joyeuse, et c'est comme si le cadavre
de l'abeille sur l'appui de fenêtre n'existait pas. Comme si les
considérations inquiètes n'existaient pas. Comme si on ne mitraillait
pas dehors, là sur ces marches, devant mon escalier, dans cette cour
derrière la grille avec un digicode. Comme si on ne pleurait pas
intérieurement au 1^er^ étage, tout en buvant une coupe de champagne ou
en cachant sa tête dans son coude. Comme si on ne souffrait pas les deux
mains sur la poitrine dans la cabine en attendant le résultat d'une
mammographie. Comme si la vieille dame aux espadrilles usées n'allait
pas repasser rue des Ursulines avec ses pieds qui traînent sur le sol,
telle une skieuse de fond, parce que pour elle la neige est partout, la
glisse est partout, les sillons grisés et maculés de brun qu'elle doit
suivre sont partout, même sur les pavés irisés de la rue piétonne, et
pour elle c'est toujours l'heure de demander Vous avez l'heure ? Quel
jour on est ? avec la peau de ses joues toute pâle et striée des lignes
superficielles laissées par les draps froissés d'un sommeil si profond
que personne n'en verra jamais le bout, ni aucun être humain, ou animal,
ni quoi que ce soit dans cet univers-ci ou dans un autre.

Parfois on a des doutes (j'ai). C'est peut-être une voie de garage de
croire que tout est lié, c'est peut-être un malentendu. Comme ces
phrases tronquées parce qu'il y a eu une mauvaise traduction. Les
mauvaises traductions sont des choses qui arrivent. Elles ne sont ni
gommées ni reprises (ni échangées). Certaines personnes font de
mauvaises traductions, et s'en vont. Elles ne tentent pas de se
réadapter, de retraduire et retraduire encore, alors que ça devrait être
l'idée première d'une vie de tenter sans interruption, de se tourner
sans interruption vers les signes pour les traduire et les retraduire
sans interruption, avec la certitude de se tromper un peu, ou presque,
ou tout à fait. Si on ne sait pas qu'on est tous approximatifs, alors à
quoi bon. Les gens exacts ne se posent pas de questions, ne sont pas
tourmentés, ne pensent à rien lorsqu'ils croisent la vieille dame aux
espadrilles. Le monde est plaisant pour les gens exacts, les montres à
l'heure (la mienne est tombée derrière la banquette, sous le drap indien
qui la recouvre de gazelles et de trompes d'éléphants incomplètes).
J'incline à traduire et retraduire sans cesse ce qui passe à proximité.
Et les baleines fossilisées dans les déserts. Et les soupirs fossilisés
dans les couloirs. Ces lieux où il n'y a pas de signe égal. Le signe
égal, je suppose que c'est lui le responsable. Lui qu'il faudrait
traquer. Prendre par le col pour lui hurler dessus, le verbe juste
serait conspuer. Signe égal. Si net. Si parallèle. Si rectiligne.
Bouché. Canalisé, comme une théorie de tunnel bien désinfecté. Que la
plaie soit du signe égal. Que crève le signe égal. Que lui et tous ses
frères se fassent tous piétiner par les incertitudes et les insectes
ravageurs, qu'ils soient tous grignotés. Je voudrais que l'abeille
morte sur l'appui de fenêtre le soit, morte au combat, d'avoir mangé sa
juste dose de signes égal, jusqu'à ce que son appareil digestif éclate,
gloire à sa gloire, respect à sa mémoire.

Donc il y aurait deux sortes de gens (voilà où aboutissent mes
réflexions) : ceux qui ont une main de métal et les autres. Ceux qui
serrent leurs épaules en frissonnant sans le montrer et les autres. Ceux
qui regardent des photos de la chambre de mort de Marilyn et les autres.
Ceux qui ne comprennent pas les distances. Ceux qui ne calculent pas de
tête. Ceux qui ont un trou dans le mâché de leur œil, une dent dans le
mâché de leur langue, une patte directement reliée de leur thorax à
celui d'une abeille immobile depuis des semaines contre un volet dehors.
Une acclimatation défectueuse. Des difficultés à se connecter sur des
paraboles en mouvement. Des QI et des barbes trop longues. Des listes de
romans jamais écrits. Des listes d'écrits pas encore transférées par
écrit. Et l'amour des petits objets dans les brocantes, comme ça, pour
rien, sans explications.

# Où l'on se demandera quel est le bon côté des portes, tout en faisant une petite cosmogonie (deux)

Quand je tente de visualiser l'image d'une petite cosmogonie, c'est
une méduse qui apparaît : une tête ronde, mais ses tentacules sont
follement lancés n'importe où, vers d'autres méduses flottantes,
d'autres cosmogonies, dans un espace dont je ne n'imagine pas
réellement les bords, une sorte d'agencement copié sur ce que je pense
être l'organisation des neurones. Et --- là, la théorie des fractales
rapplique --- c'est possible. Pourquoi ne pas envisager que les petits
mondes agglomérés d'une société soient structurés sur le même modèle
que ce qui existe, à plus petite échelle, dans les cerveaux humains ?
Ces petits mondes seraient à la fois délimités et en connexion avec
d'autres. Uniques et unis par les mêmes contingences, les mêmes
urgences, les mêmes destins, même s'ils n'en ont pas toujours
conscience. Quoi qu'il en soit, pour fabriquer une petite cosmogonie,
la forme ronde semble inévitable. Planète, œil, forme de goutte d'eau,
de cadran d'horloge, de sein nourricier, tout, fini et infini, unité et
multiple, bouche, ouverture, margelle de puits, *Bocca della Verità*,
rondeur et perfection --- "*Perfetto come la 'O' di Giotto*" ---,
zodiaque, mandala, tonda, rosace, pierre du soleil, coupole, etc.

J'incline vers les choses brutes, non apprêtées. J'incline vers les
choses qu'on dit laides quand je les trouve sincères. Aussi après une
petite cosmogonie (un) en papier mâché, pour faire une petite cosmogonie
(deux), je m'emploie sincèrement à tordre un fil de fer pour lui donner
l'aspect d'un cercle. Puis je choisis de vieux fils de laine, de la
laine trouvée dans les vide-greniers, pas forcément la plus décorative.
Je fais en sorte, à force de tours et de détours, que ces fils
remplissent le cercle en s'enroulant autour de lui pour fabriquer un
disque. Le sens suit mon impulsion, mon mouvement de non-réflexion. Avec
l'aiguille je tourne, je ressors d'un côté, je traverse, je fais
quelques points ouvragés que je hachure de points frustes, je noue, je
m'emploie à combler les vides. Lorsque la forme est pleine (ou
pratiquement), je la pose bien à plat sur un support qui ne craint pas
d'être sali. Avec un pinceau assez grossier et de la peinture blanche
(bas de gamme), je recouvre. Certaines couleurs ne se laissent pas faire
: ça ne tend pas vers le blanc uniforme. Des tracés tendus résistent,
discrètement roses, bleus, jaunes. De petits cratères se sont formés là
où la peinture n'a pas pu s'infiltrer entre les fils serrés. Cela
donne une surface tissée qui semble homogène de loin, mais ne l'est pas
du tout lorsqu'on s'approche. De près, ça a l'air vieux. Avec des
mouvements traversants, figés, tordus, comme sont tordus les corps
fossiles des chiens de Pompéi. Comme quelque chose qui aurait survécu à
quelque chose. C'est ce que je voulais exactement. J'en fabrique
plusieurs, de plusieurs tailles. Et il est fort possible que je continue
sur cette lancée, parce que *let's face it*, il est fort possible que
je sois absorbée par ce genre d'activités depuis très longtemps. Je ne
m'en rends compte que maintenant.

Longtemps que ça dure, peut-être depuis le début, je veux dire depuis le
moment du commencement, quand j'apprends à écrire : je suis en classe,
non, ça ne commence pas là, ça commence la veille. La veille, je rêve
que je suis en classe et que c'est l'heure de la dictée ; je rêve que
l'institutrice dicte une phrase, elle articule une phrase et, à
l'intérieur de cette phrase, elle prononce le mot "voyage" ; je rêve
que je bute sur ce mot, peut-être à cause du *y* qu'on ne rencontre pas
souvent, je rêve que je m'arrête d'écrire parce que je bute sur ce mot,
ce *y*, je rêve que je lève la tête et que, tétanisée, je fixe
l'institutrice ; je rêve qu'en me voyant ainsi immobile, elle me
houspille, elle me dit que je dois me secouer un peu, je rêve qu'elle
me dit Allez allez allez, tête en l'air, bécasse, paresseuse. Le
lendemain, c'est l'heure de la dictée, réellement. Elle prononce
réellement le mot "voyage". Je me fige. Une voix dans ma tête dit
comme-dans-le-rêve, comme-dans-le-rêve, je lève la tête et je la fixe,
éperdue, comme dans mon rêve, elle me houspille de la même façon que la
nuit, exactement. Un phénomène bref, et je n'ai pas d'explications.
Peut-être une sorte de passage, une sorte de claudication du mauvais
côté de la porte. Ou bien du bon côté de la porte. Ou bien peut-être
qu'il n'y a pas de bon côté aux portes. Peut-être que c'est pour ça,
la nuit, le jour, bondissants vers l'avant, vers l'après.

Je ne sais pas faire le point sur ces choses inconcevables qui cachent
d'autres choses inconcevables. Je le sais, je ne sais pas faire le
point sur les intrications, sur la porosité. Je ne sais pas identifier
les *y* rares qui viennent habiter le sommeil puis se pavanent aux yeux
de tous, extravagants. Je ne sais pas réagir autrement que figée
(stupéfaction, interdiction) devant les bordures et les marges, franges
de tapis et féeries, contours zébrés, tours de passe-passe. Et cette
quantité de détails (inondation, stupéfaction), quantité de détails
prenants, palpitants, captivants, je ne sais pas faire le point. C'est
un problème. Un de mes problèmes. C'est ce qui me force à faire des
cercles de laine et des ronds de papier mâché, et ça depuis longtemps,
car le temps a passé comme un rêve, depuis ce rêve, avec ce rêve
(brutalement je comprends que j'ai écrit plusieurs cosmogonies, je veux
dire plusieurs livres --- dont certains pourvus de y ---, sur des sujets
que je pensais variés. Il me semble, en y regardant de loin --- ou de
près, car les distances peuvent se dissoudre --- que tous racontent la
même chose, malgré leur aspect extérieur divisé en volumes séparés,
découpé en rondelles c'est le même pain tranché).

# Nous voilà près d'une urne, c'est étonnant

Rien ne peut faire office de fin, tant les petites cosmogonies se
déplacent, changent de trottoirs ou de files à la caisse, attendent
assises sur des marches qu'on vienne les chercher, ou s'embrouillent en
essayant de lire un plan. Elles sont frêles.

C'est qu'autour, les choses sont vraiment versatiles. Les petits mondes
parfois s'effacent, se tassent, se compriment et décident de ne plus
apparaître, ou ils sont si étriqués que brutalement ils cèdent, ils
explosent, comme pour se délivrer des contractions qui sont aussi leur
forme de respiration.

Alors, plus rien n'est déchiffrable, ni les épaules serrées ni les
fuites éperdues. Les mondes les plus petits se disloquent. D'autres
mondes plus pesants les écrasent. Et la folie --- on ne peut pas ne pas
mentionner la folie, non pas ce genre de folie qui nettoie l'horizon
avec l'absurde, mais la folie consciente, la folie résultant de
contractions insupportables, non pas la folie vagabonde de la balle qui
rebondit, mais la folie qui fait tomber les étagères, et nous dessus ---,
oui la folie arrache des bras, se poste en haut d'une tour pour tirer à
la carabine, ou elle égorge une passante. Toutes les photos montrent en
gros plan des portières de voitures de police, parce qu'il est
impossible de prendre en photo la folie. Les commentaires décrivent des
conséquences, des causes, parfois en inventant, parfois donnant des
précisions qu'on ne leur demande pas, car il est impossible de commenter
la folie qui massacre, folie mortelle, impossible de commenter la mort.

Les petites cosmogonies possèdent elles aussi des trous noirs. La
locataire du 1^er^ connaît quelqu'un qui connaissait quelqu'un qui s'est
jeté d'une fenêtre. Elle a téléphoné à M à Vancouver, sa nièce a été
touchée à la jambe, cette chance qu'elle a, alors qu'une autre est morte
à côté d'elle, à un concert, debout, les poumons perforés.

Les fils s'emmêlent, ils sont méconnaissables. Les ourlets de rideau
s'effondrent lorsqu'on veut les recoudre. La rouille attrape les angles
des radiateurs, se propage, ne s'en va plus jamais. Après, on peut
toujours tenter d'écrire un trajet en voiture ou remplir un carnet de
notes, s'arrêter pour filmer les vols des mouettes à la traîne des
tracteurs. On peut toujours écouter à une terrasse de café un vieil
homme parler de Guernesey, du temps où l'île s'appelait S, et il
raconte qu'un livre sur ce thème existe, il précise que c'est écrit en
anglo-normand, c'est-à-dire avec un vocabulaire anglais et une syntaxe
normande. Un enfant passe en riant. Des voix d'enfants s'élancent (du
Benjamin Britten), ils roulent les r, tapent du pied, montent la gamme,
ralentissent, les notes dégringolent. L'un d'eux imite le chant du
coucou, l'autre reprend, comme une rivière coule dans une forêt
détruite, la voilà ranimée. Tout a vieilli, sauf ta présence, dit un
poète. Ainsi est fait le tour de la question. Est-ce que c'est vrai ?
Est-il vrai qu'une galaxie (en forme de galaxie), lorsqu'elle se place
au milieu d'autres galaxies prend soin d'être à l'endroit exact qui fait
que toutes les galaxies ensemble forment une autre forme en forme de
galaxie ? --- où cela s'arrête-t-il ? Est-ce que c'est à cause de cela,
de ce paramètre implacable, que l'oreille humaine aime beaucoup écouter
les canons, la mélodie reprise en écho décalé, à l'infini ? Et est-ce
que le mot "canon", dans d'autres langues, veut aussi dire la musique
et la guerre également ? Les morceaux d'insectes de Britten (*Two Insect
Pieces*) ne donnent pas d'indices, ou c'est à d'autres questions qu'ils
répondent. Peut-être qu'entre eux les mondes s'interrogent et que les
questions sont voilées, les réponses décalées dans un QCM délirant.
Lorsqu'on éloigne le regard, qu'on se recule un peu, les petits mondes
que chacun porte, chacun frôlé par le monde voisin (où l'obscur a sa
place et la folie aussi) forment-ils tous ensemble une image cohérente ?
Les fictions l'espèrent sans doute, ou veulent le croire. Installent des
mondes chronologiquement fiables, aux contours fiables, dotés de noyaux
et d'enveloppes précises, confinés dans la boîte compacte d'un
assemblage de pages numérotées convenablement. Peu importe le chaos
retranscrit à l'intérieur, l'enveloppe rigide de la couverture saura le
circonscrire, éviter qu'il déborde. Ou c'est une illusion. Les mondes
très structurés, pensés et assemblés débordent de toute façon, les pages
se décollent et les mots se répandent, percent l'enveloppe,
s'infiltrent, viennent grossir d'autres chaos, plus palpables ceux-là,
chauds et vivants, qui descendent d'un bus ou portent un sac de plâtre
sur l'épaule. On ne sait pas. On ne sait rien finalement, on ne connaît
que les détails.

Dans des états du nord des USA sont fabriquées des urnes votives. Il
s'agit d'urnes de grès, des poteries artisanales sur lesquelles sont
collés les symboles de ce qu'aimait un homme, une femme. L'urne sert à
se souvenir. Si l'homme ou la femme adorait les trains, on y colle une
locomotive miniature. Si il ou elle aimait les chiens, une figurine de
lévrier ou d'épagneul. Si il ou elle aimait boire, la capsule de sa
bière préférée. On suppose qu'à force d'être couverte des choses qu'une
personne aimait, l'urne contiendra son esprit. Ce serait une
bienheureuse idée de décorer sa propre urne soi-même, ou plutôt ses
urnes, car pourquoi se contenter d'une seule, la vie est courte, autant
qu'elle soit en expansion et fasse qu'on multiplie les poteries (au
moins ça). Sur l'une des miennes, je mets de la ficelle et de vieux
caractères d'imprimerie, par exemple un *y* acheté dans une brocante,
voilà.

# Pour faire de petites cosmogonies (trois, etc.)

Plus c'est grand plus on aime. Les chercheurs en neurologie le disent.
Et plus on se reflète dedans, plus on est attiré. On n'y peut rien.
Notre cerveau sur ces décisions-là est parfaitement émancipé. On peut
faire des batteries de tests, notre impulsion première nous entraîne
inévitablement vers le plus grand et le plus miroitant. C'est dans
notre nature. Alors je me pose des questions quand je vois un artiste
créer un chien géant d'aluminium *very shiny*. Est-ce que ce ne serait
pas un peu facile, comme installer des blocs de sucre devant une ruche ?

Je pars du principe que, pour faire de petites cosmogonies (trois), je
dois utiliser un support rond. Je pars du principe que ce support doit
être ordinaire. Par exemple une assiette en carton. Par exemple le dos
d'une assiette en carton. Je mouille une feuille de papier que je
chiffonne, que j'imbibe d'encres de couleurs, que je trempe dans de la
colle, que j'étale sur le dos de l'assiette, que je lisse, que
j'oriente, que je fripe, que je travaille. L'encre longe les pliures
du papier, et forcément le reste suit. D'autres papiers emboîtent le
pas, déchirés, découpés, mots ou visages, messages pris au hasard,
formes prises au hasard. Des lignes d'écriture asémique. Des traits qui
font points de suture. Des soleils. Des branches. Des morceaux de photos
et de diapositives. Du fil cousu. Ce qui s'ouvre avec les cosmogonies
(trois) est à la fois anecdotique et décisif. Faire, faire, disent les
mains, pendant que les chiffres tombent --- 80 %, 30 %, 1 000 000, insectes
éteints, oiseaux disparus, espèces menacées ---, tant de chiffres, à en
devenir arithmophobe juste avant que débaroule la fin du monde --- et une
fois le monde fini, où trouver un bon thérapeute pour soigner sa
déprime ? Qu'est-ce que ça fait d'écrire, de coller, de peindre sous
cette menace, au milieu de ces grandes secousses, qu'est-ce qu'on doit
faire, pleurer, s'indigner, dénoncer, fatiguer, traficoter, se
résoudre, mettre en œuvre, et quoi ? à quel rythme, fréquence, longueur
d'onde, sur quelles pancartes, quels murs, quelles peaux ? à qui se
vendre, qui éviter, où exposer, conférencer, colloquer, séminarer ? le
matin au réveil, quand on est encore propre de la nuit qui lave, encore
naïf de ne pas avoir vu les vomissures toxiques déversées (fleuves et
champs, têtes et cerveaux reptiliens), les yeux quand on les ouvre
c'est comment ? qu'est-ce qu'on voit et on en fait quoi ? --- depuis
Vénus, la Terre ressemble à un point bleu, des points bleus, voilà ce
que mes mains font sur mes assiettes, car je ne suis pas experte en
retombées nocives, mais en ombres que les rideaux font chavirer, oui, et
je fais des études pour que mes petites cosmogonies quatre, cinq, six,
etc. voient un jour le jour.

# Quelques données scientifiques

Le temps de Planck est le temps qu'il faudrait à un photon dans le vide
pour parcourir une distance égale à la longueur de Planck. L'univers
est en expansion. Ses points les plus lointains sont ceux qui
s'éloignent le plus vite. C'est pourquoi les fers à repasser de métal
(N^o^ 5 W) nous arrivent avec fulgurance et filent comme des comètes. Le
temps des vide-greniers est le temps qu'il faudrait à un fer à repasser
(N^o^ 5 W) pour parcourir une distance égale à la circonférence de son
propre thorax, lieu où siège l'émotion, aussi appelée longueur des
vide-greniers. Le fond diffus cosmologique se mesure en spectres. Les
spectres sont des apparitions fantastiques, effrayantes. Cette peur est
à détricoter retricoter.

C'est ce que font les petites cosmogonies, les miennes, les vôtres, de
toutes leurs forces. Le problème de l'horizon ne les freine pas, ni le
problème de l'horizon des particules, ni le problème de l'horizon des
événements, et encore moins le problème des quatre coins de l'horizon
(qui sont bien plus nombreux que quatre, les chiffres étant des données
imparfaites, imprécises. Hors, bien sûr, le N^o^ 5).

# Livraison gratuite satisfait ou remboursé deux ans de garantie paiement sécurisé elle n'est pas belle la vie ? n'attendez plus pour vous faire plaisir

La blonde comme il faut a sorti la cuisse de dinde de sa barquette de
cellophane, avec une seconde d'arrêt, on pourrait presque dire de
stupéfaction, devant le mot écrit à la main sur l'étiquette, sous la
date de péremption, un mot isolé qu'elle a eu du mal à déchiffrer (déjà
en se servant dans le bac réfrigérant au moment de l'acheter, elle avait
senti que quelque chose clochait), c'était écrit au stylo bille *Cuisse*
à la main, avec un C majuscule à l'ancienne, comme ceux de ses cahiers
de classe, et il lui rappelait, comme petite ça lui rappelait, le T
majuscule en écriture cursive, ce qui installait une ambivalence, une
confusion entre un C et un T enfantins, et puis il y avait un espace non
utilisé avant la suite, le uisse, et les deux s côte à côte semblaient à
la fois identiques et las, abattus, comme pris d'une sorte de terreur
interne à être ainsi exposés, presque pareils mais pas vraiment, comme
s'ils savaient qu'ils en perdaient leur sens. Elle lisait une sorte de T
ui s s e qui n'avait rien à voir avec rien, un mot inadapté, incapable
d'être projeté en plusieurs langues sur des murs du XIV^e^ siècle,
incapable de s'ajuster à cette barquette, à ce morceau de viande
traversé par un os qui perçait presque l'emballage, son embout arrondi
dédoublé, et sa peau comme un drap mal remonté d'un tissu gaufré de
petites piquetures rigides bien réparties.

C'est un détail. C'est le genre de détails qui n'arrêtent pas
(l'homme à la) chemise satinée. Les barquettes de cuisses de dinde
n'entrent pas dans son champ de vision (cf. myopie du rhinocéros). Ce
même phénomène se reproduit quand chemise satinée est confronté à des
toiles d'araignées invraisemblables, des bateaux qui accostent en 1933
et des pendules au mécanisme rouillé. Il serait d'ailleurs bien plus
simple de faire la liste de ce qui entre dans son champ de vision plutôt
qu'une liste de ce qui n'y entre pas. Précisons : la vue de chemise
satinée n'excède pas l'éclairage circonscrit et bien délimité d'une
lampe de bureau. Ce qui n'est par cerné par ce halo est privé de ses
droits, c'est-à-dire soit non existant, soit non pertinent. C'est très
pragmatique. Lorsqu'on lui expose quelque chose, par exemple une idée
qui jusque-là était dans l'ombre, il l'examine avec circonspection,
sérieux, avant de la déclarer neuve
(je-ne-connais-pas/je-ne-savais-pas), puis il la pose dans un coin non
éclairé, aux rebuts. La même idée, quelques jours plus tard, sera
traitée de la même façon (je-ne-connais-pas/je-ne-savais-pas), car sa
mémoire est aussi myope que lui. Le nombre de je-ne-savais-pas qu'il
prononce n'a rien à voir avec un sentiment de curiosité à assouvir, ce
n'est pas un appel pour en apprendre plus. C'est plutôt un constat de
ce qui doit être trié, de ce qu'il faut conserver ou repousser, pour
que l'organisation interne qui est la sienne soit harmonieuse, je veux
dire valide. Comme des volets qu'on ferme, mais sans nostalgie de ce
qui est abandonné au-dehors. Ce genre de rhinocéros lampe de bureau
n'est jamais traversé par ce qu'on pourrait appeler la mélancolie, ou
la sensation douloureuse de la perte. Perdre est, pour lui, un concept
aussi net qu'une lame de guillotine. Quelque chose (un dossier par
exemple) est perdu ? C'est un échec, il y a un responsable. Chemise
satinée le cherche, le pointe du doigt, et dispense les bons points et
les punitions avec la même froideur qu'un distributeur les tickets à
l'entrée des parkings sous-terrains. Lorsqu'il tombe sur la chaîne du
télé-achat, le visage de chemise satinée s'éclaire. Non pas qu'il
voudrait tout acheter ou qu'il serait charmé par un éplucheur de
légumes qui transforme les carottes en fleurs, ou par un nettoyeur
vapeur qui rénove des éviers qui ont vu de trop près la guerre de 14-18,
non. Il est séduit par la prestation des présentateurs, cette qualité
qu'ils et elles ont de vous faire sentir proche d'eux, cette sorte de
finition dans les détails qui laisse apparaître un peu de spontanéité
dans le dispositif exact, ce presque rien qui fait que nous laissons
tomber nos défenses. Ces gens-là peuvent déboulonner n'importe quelles
protections. Manier le verbe. Ces gens-là savent ce qui fonctionne. Ces
gens-là sont l'essence même du pragmatisme, c'est-à-dire qu'ils
évoluent au centre même de l'éclairage lampe de bureau, juste en face
de l'ampoule. C'est très agréable, pense chemise satinée. D'ailleurs,
on ne s'intéresse pas assez au télé-achat, à cette quintessence d'un
vivre totalement libéré de toute entrave. Il n'est question que de
confort, consommer, luxe, amélioration des performances, mise en
compétition, bonnes affaires, tout un programme émancipateur.
L'aspirateur autonome va réellement dans tous les coins. L'intérieur
d'une tête devrait prendre en exemple cette efficacité, cette binarité
si agréable (sale/propre, pire/mieux) et se débarrasser de toutes ces
nuances inutiles, un peu crasseuses (le doute, le flottement,
l'indécision, le questionnement, l'incertitude), cette flopée
d'à-peu-près qui ne fait que freiner l'expansion du système. Pour
chemise satinée, le monde est un télé-achat, ou si ce n'est pas le
cas, il faut qu'il le devienne. On s'en porterait mieux. Entre un
astronome qui n'est même pas capable de dire de quoi est constituée la
matière noire et un représentant de commerce, son choix (rapidité,
solidité, performance) est vite fait.

À moins que. À moins que cette position bétonnée et sûre d'elle de
chemise satinée ne cache quelque chose, de la même façon que le crépi
recouvre les fissures du mur. Quelque chose de l'ordre d'une mince
ouverture sur du vide, un vide qui n'est pas vide, tout comme la
matière noire n'est pas constituée de rien. Des paroles affectueuses
non dites, des chagrins non consolés --- car ce qui n'a pas existé a,
paradoxalement, des effets. Des solitudes d'enfant. Des raideurs. Des
dressages. Des mesquineries en exemples à reproduire. Des tuteurs et des
sécateurs, des buis taillés en mèches de perceuse dans les jardins à la
française. Des terrains quadrillés, domestiqués. Des garrots,
muselières, œillères, tout un assemblage d'instruments rigides destinés
à tenir l'ensemble ensemble pour assurer la stabilité de l'édifice. On
ne sait pas, chemise satinée ne le dit pas, quels sont les premiers mots
qu'il a entendus, les premiers gestes qu'il a vus et comment il les a
traduits dans son cerveau petit, son cerveau encore malléable, sensible
--- il y a ces plantes que l'on appelle des sensitives, elles se
rétractent à peine on les touche. On ne sait pas, personne ne sait. Mais
la finalité du procédé fait que les choses sont ce qu'elles sont
censées être. Les chiens ne font pas des chats, tant va la cruche, les
proverbes disent peut-être la vérité. Il y a des connecteurs logiques
très logiques (la trentaine a peut-être raison ?). Ce n'est pas évident
de faire trembler l'échafaudage. À moins de regarder le télé-achat
autrement. Par exemple avec compassion. Par exemple en mettant de la
compassion partout. Trop. Comme on submerge des chiquettes de papier
avec de l'eau dans un saladier. À moins de s'activer autrement, de
façon à submerger les saladiers impropres à la consommation, je veux
dire à la compassion, les saladiers pourvus d'une date limite.

Quelqu'un-quelqu'une dit : "Il y a beaucoup de tyrans, et peu de
rois." et il couvre les murs de son palais d'assiettes brisées qui
scintillent au soleil. Lorsqu'on vient le visiter, on doit passer par
une grande pièce vide, avec au centre une balançoire.
Quelqu'une-quelqu'un a construit un magasin de haute couture au milieu
du désert, pour qu'on puisse regarder la vitrine chic et les stores
impeccables levés sur une immensité désolée en friche.
Quelqu'un-quelqu'une fabrique des sortes de bâtons de marche en
comprimant sous des bandes serrées (tissu, scotch, bandes plâtrées) ce
qui est jeté aux environs, des sacs d'eau en sachets, des canettes
vides, des emballages de restauration rapide. Ces bâtons sont vernis.
Alignés, ils font penser aux tuyaux d'un orgue. Et finalement ça
chante. Une sorte de liturgie du simple (liturgie, du grec λειτουργία /
*leitourgía*, qui veut dire "au service du peuple"). Je ne sais pas si
on doit appeler tout cela gestes saufs, gestes vaillants, gestes
piquants ou gestes compassionnels.

# Où la narration évoque l'éventualité du yukon et frôle des gueules de truites avant de se perdre dans les visages et l'écholocation chez les chauves-souris

Le jeune racontait qu'il habitait dans un sous-sol, à Vancouver, que la
lumière du jour n'entrait jamais très fort ni sans avoir été pliée
auparavant par le sol affleurant la moitié de la vitre, que le loyer
était très cher, que ses études étaient très compliquées et qu'il avait
du mal à rester concentré (sans doute à cause de ses perpétuelles
connexions à renouveler avec le monde). Je pourrais écrire la biographie
du jeune. Pour qu'elle soit plus complète, il faudrait y ajouter son
pendant brut, son frère, un double d'une substance plus épaisse. Le
frère du jeune est bûcheron au Canada, donc un cliché qui a pris forme
humaine (ou l'inverse). Je ne sais pas s'il pousse le vice jusqu'à
porter des chemises écossaises, manches remontées sur un maillot de peau
délavé à cause de la dernière lessive. Je ne sais pas non plus s'il
habite Port-Cartier, ou Forrestville, ou le Yukon, je sais seulement que
c'est un lieu isolé et froid comme ceux qu'on imagine. Il y résiste
sans doute grâce à sa substance épaisse et à sa moelle déconnectée, ce
qui lui donne de l'endurance, car il ne perd pas d'énergie à faire la
sempiternelle mise au point en direction du reste. Dans cette biographie
bi face, j'insisterais bien sur les destins divergents des deux frères.
Comme si la vie, en sa grande sagacité, avait décidé de bien les
séparer, en distinguant leurs maltraitances : celui-là sera en
transition, en recherche à perpétuité, s'est-elle dit, et l'autre sera
une particule isolée, autonome et rugueuse, a-t-elle pensé. La vie est
une sacrée peste. Je pourrais tout aussi bien retricoter les éléments,
rendre au jeune sa partie stable, sa moitié indestructible, rendre à son
frère un peu du flou qui lui manque autour des cheveux, je veux dire
romancer. Les deux porteraient une barbe, leurs portraits se
superposeraient. Mais ça ne va pas être possible, car il semble qu'en
dehors de toute attente d'imprévus, le frère du jeune habite bien un
chalet de bois au milieu d'une forêt et qu'une fois par semaine il va
en toute logique se ravitailler dans la seule boutique ouverte à la
ronde. À chaque fois qu'il y entre, un jeu de grelots accroché à la
porte tinte en do sol fa (C G F en version originale). Les clients
peuvent accrocher leurs bonnets dans l'entrée, sur le portemanteau fait
sur mesure par le grand-père de la propriétaire --- un homme taiseux,
pêcheur et taxidermiste. C'est un portemanteau remarquable. Il a été
taillé dans une hampe de bois du Nouveau-Brunswick et possède cinq
crochets avec, clouée sous chacun d'eux, une gueule de truite
arc-en-ciel grande ouverte. Cinq gueules de truites arc-en-ciel en tout.
Il manque quelques dents, par-ci par-là et les mâchoires sont devenues
avec l'usure, la poussière, la fumée, les vapeurs de cuisine, aussi
sombres que le bois lui-même. On pourrait même penser qu'ils n'aient
jamais vécu séparément, le bois ayant avalé les mâchoires pendant sa
pousse, à moins que le grand-père n'ait pêché un poisson rarissime
formé de cinq têtes alignées aux bouches écartelées, hurlantes et
silencieuses comme savent faire les poissons. Cette boutique --- en haut
de la vitrine il y a un nom, mais je n'arrive pas à le lire --- est une
entité autonome, avec ses occupants de passage, ses histoires, ses
fantômes, ses histoires de fantômes et les histoires de tous ceux qui
viennent ici écouter de nouvelles histoires en même temps qu'ils
achètent des boîtes de haricots et du sucre. Ceci n'est pas une
reconstitution, un décor en trompe-l'œil comme on pourrait le croire ---
je ne contrôle rien, je ne sais pas qui on est lorsqu'on veut contrôler
les histoires mais je ne veux pas en être, je préfère laisser les
histoires bouger et laisser les visages incontrôlés.

La blonde comme il faut a laissé cuire au four la cuisse de dinde
pendant presque deux heures et elle l'aurait laissée davantage si elle
avait pu, si ça n'avait pas noirci, pour que la cuisson s'infiltre dans
chaque pouce de chair et que, grâce à ça, par sa texture et sa couleur,
la cuisse de dinde ne ressemble plus à quoi que ce soit d'animal, ni
chair, ni ossement, ni muscle, ni rien qui rappelle qu'une vie
quelconque ait pu la traverser. Elle aime tout ce qui est très cuit,
même les légumes, ceux qu'elle préfère doivent compoter, et l'oignon
doit se déliter en couches si fondantes qu'elles sont au bord de ne plus
se différencier, comme lissées par l'effet du chaud, comme imbibées
d'une crème uniforme antirides velours suprême qui serait capable de
reformer et de refondre en profondeur la chair détruite, prédécoupée, en
faisant renaître une matière compacte, aussi compacte que serait un
galet, mais molle.

La blonde comme il faut aime grignoter. Lorsqu'elle est seule, sa façon
de manger change. Ça devient aussi important que respirer, aussi vital.
Mais, si quelqu'un est là et risque de la voir, elle se concentre et
fait beaucoup d'efforts pour sembler désinvolte. Et ces mouvements
contradictoires qui se combattent (l'appel de la faim primitive et la
bouchée distraite, la vigilance intense et le désintérêt) la fatiguent
tant que parfois elle doit poser sa tête dans le fauteuil et fermer un
instant les yeux.

La trentaine ne ferme jamais les yeux. Même en dormant. C'est purement
mécanique, instinctif, une sorte de réflexe. Le même qui fait que les
yeux des requins s'équipent d'une pellicule de peau opaque et
protectrice au moment où ils mordent. La trentaine n'aime pas fermer les
yeux. Lorsqu'on ferme les yeux, on est fragile. On ne sait même pas à
quoi ressemble son visage. On est comme ces photographies ratées où
l'ombre d'un doigt masque l'objectif, et on doit les jeter. On devrait
pouvoir jeter son visage-des-yeux-fermés. On ne garderait que son
visage-avant-de-partir-au-travail, ou son
visage-vérifié-dans-les-vitrines. Tout ce qui s'éloigne de cette sûreté,
de cette certitude, devrait être annihilé, pense la trentaine. Tout
devrait être plus cuit, pense la blonde comme il faut. On devrait
interdire aux enfants de faire des miettes, pense chemise satinée. Le
cadre que j'ai accroché n'est pas droit, pense la locataire du 1^er^,
avant d'écrire *Love* au dos des cartes postales qu'elle envoie à son
fils bûcheron. Le visage de son autre fils, le jeune, ne montre pas de
différence jour nuit, parce que ce n'est pas le problème. Le visage du
jeune est traversé. Traversé de quoi n'est pas une question valable.
Traversé. Ou alors, pour tenter de le dire le plus exactement possible,
traversé de tâtonnements. Si je devais faire son portrait-robot,
m'adresser à un dessinateur professionnel pour dire "là les oreilles un
peu plus hautes, là le nez moins épais, et les sourcils plus décalés",
ce serait voué à l'échec. Le visage du jeune est traversé, je ne vois
pas comment l'exprimer autrement. Rien n'est stable, on ne peut rien
imprimer sur un avis de recherche. Ce n'est pas que tout fuit ou que
tout est flouté, c'est que tout est vibrant. Comme une image sur un
téléviseur refuse de se stabiliser et apparaît par flashs, ou par jets
de rayures transversales, ou en flux de pixels déroutés (vraiment je ne
sais pas quoi faire du visage du jeune). Celui de son frère est plus
reposant, un visage carré de bande dessinée. Le visage de la blonde
comme il faut est pâle. Le visage de la trentaine est sec. Le visage de
la main artificielle est comme un creux de couleur chaude, orangée, ou
beige lumineux. Le visage de la femme fatiguée plus que de raison mais
droite a les sourcils froncés, même lorsqu'elle rit. Elle rit beaucoup,
mais les sourcils froncés, parce qu'il n'y a rien de plus grave que la
gravité en son centre, une boule de gravité toute rutilante et si lourde
que cela tend son front et fait se froncer ses sourcils. Sa politique du
malgré-tout est assez efficace, et malgré-tout elle marche plutôt
tranquillement, même froncée. Elle peut même se froncer à un point tel
qu'elle pourrait devenir un pli, un long pli --- pas un pli aussi long
que la mer mais quand même, un pli d'une taille conséquente sous la
pression de ce qui ronge les ongles (soldats en treillis dans les gares,
colis suspects, tirs de LBD, femme que les forces de l'ordre tirent par
les cheveux sur plusieurs mètres, icebergs qui, après s'être détachés,
longent les côtes et les cuirassés, puis fondent peu à peu et une fois
devenus liquides s'enroulent autour des Îles Marshall, léchant le béton
désarmé d'un dôme fêlé qui déverse un acide extraordinaire, capable
d'atteindre l'os avant la peau, et ensuite tout part, tout se répand et
s'en va se dissoudre dans une langue immense, entièrement bleue, remplie
de corps).

S'il est difficile d'appréhender les visages externes, à cause du flou,
de la vitesse, et d'autres paramètres problématiques, il est difficile
d'appréhender les visages internes tout autant. Comme il est difficile
de savoir si on a affaire à un rêve au moment où on rêve ce rêve. Il
faut se réveiller pour le comprendre. Pour les visages, c'est un peu la
même chose : on sait que l'on voit le visage interne de quelqu'un à
l'instant où il s'évanouit. On sait que ce visage interne existe encore,
quelque part, mais on est bien sûr incapable de dire exactement où,
parce qu'il nous faudrait posséder d'autres outils, des outils qu'on ne
saurait même pas nommer. C'est comme l'intelligence des animaux qu'on ne
peut mesurer qu'avec notre propre intelligence, en fonction d'elle et de
nos capacités restreintes. Imagine que je puisse voir les couleurs comme
l'ara ou l'aigle des steppes, entendre les battements de cœur de la
belette ou la pulsation de la sève dans le tronc du platane, imagine que
je me déplace sous l'eau infiniment parce que je respire l'eau, imagine
que la membrane qui unit les os de mes membres supérieurs me fasse des
ailes diaphanes, imagine que mon chant chevauche le Gulf Stream, imagine
que mes yeux, mes oreilles et mes nerfs résonnent dans un système
désorientant pour les humains, en écholocation par exemple, imagine que
je perçoive les vents solaires, que mes sens soient à même de comprendre
l'infradétectable et de se déplacer dedans, naturellement, parce que j'y
vis : est-ce que ma géométrie serait moins fiable ? est-ce que mes
connaissances, mes théories seraient moins remarquables ? et mes points
de repère, ils seraient différents, mais qui pour décider qu'ils ne
seraient pas valables ? solides ? et est-ce que je ne pourrais pas
saisir les visages internes des humains, les débusquer quand on les
croit perdus, évaporés ? C'est possible. Oui, peut-être. Je dis
peut-être, rien de plus, rien de moins que peut-être.

# Digérer difficilement est un emploi à plein temps

Je travaille à faire de petites cosmogonies de toutes les tailles, mais
soyons réalistes, aucune n'excède la largeur de ma main. C'est que je
sens bien que plus elles seront grandes, plus elles frôleront des vortex
qui les changeront en trous noirs. Elles sont énormes, celles des
attaques de drones, celles des porte-avions dans le Golfe d'Aden. Et
celles du verbe "décimer", encore plus grandes que celles des vaches à
hublot ou des ours polaires faméliques, sont mathématiquement
insupportables. Il y a la gigantesque cosmogonie de l'autorégulation.
La race humaine se sépare de la race humaine. Les nuages de fumées
abrasives gagnent en intensité, et ils déciment. Les inondations
meurtrières déciment plus furieusement que la *Modeste proposition pour
empêcher les enfants des pauvres en Irlande d'être à la charge de leurs
parents ou de leur pays et pour les rendre utiles au public* de Jonathan
Swift. Ceux qui construisent des murs de quatre mètres de haut
s'achètent des îles et se font installer des bunkers pour survivre aux
grandes décimations à répétition. Les autres se débrouillent. Les
hôpitaux sont débordés. La pollution gomme un dixième de la population,
les feux de forêt un autre dixième, la montée des océans un autre
nouveau dixième, de dix en dix ça continue jusqu'à ce qu'il ne reste
plus qu'un seul humain et, comme le dit je ne sais plus qui (après
recherches, il s'agit d'André Leroi-Gourhan) cet unique survivant
videra la dernière poche de pétrole "pour cuire la dernière poignée
d'herbe qu'il mangera avec le dernier rat". On peut penser que de
nouvelles espèces de palmiers s'arrangeront, avec de nouvelles races de
passiflores, pour subsister, mais peut-être pas de notre vivant. En
attendant, je me limite à une taille valable, celle de ma main.

J'ai trouvé un très vieil exemplaire de *L'Antiquaire* de Walter Scott. À peu près à la moitié du livre, les pages cessent d'être rognées,
c'est-à-dire qu'on n'a pas utilisé de coupe-papier pour les ouvrir,
elles n'ont pas été lues, je ne sais pas pourquoi, et cela vient
rejoindre l'espace propice, fertile, savoureux, mystérieux, entêtant,
ravissant de la matière noire de mon ignorance.

# Focus sur la vie des nasses réticulées

Le jeune et la blonde comme il faut ne se rencontrent pas, mais ils sont
à une poignée de main de distance --- ce n'est peut-être pas la bonne
formule, je ne connais pas assez La théorie des six poignées de main (ou
des Six degrés de séparation) pour savoir quantifier ce qui sépare
l'amie de la locataire du 1^er^ et le fils de la locataire du 1^er^ ---
de toute façon il semblerait que la théorie des six poignées de main ait
été modifiée récemment ; des calculs plus précis, actualisés, l'ont
visiblement transformée en théorie des 4,71 poignées de main (les
chercheurs y travaillant ont des noms hongrois, américains, ils sont
mathématiciens, psychologues, ils voyagent, du Massachusetts en
Autriche, de Monte-Carlo à Cambridge et lorsqu'ils se croisent, par
exemple pour une conférence sur le thème *Contacts and Influences*, ils
se congratulent). Est-ce que ça modifie la trajectoire de quelqu'un ce
4,71 ?

Dans les films, les gens qui ne se connaissent pas entre eux mais qu'on
connaît séparément à travers l'écran se rencontrent. Dans les livres
aussi. C'est bon pour la narration. Il s'agit de mettre en place et de
faire durer un certain suspens, une sorte d'incertitude exaspérante, et
lorsque la rencontre a lieu, après avoir été manquée, contrariée par des
détails anodins spectaculaires, reportée par la fermeture trop rapide
d'une porte d'ascenseur, quel soulagement. Un soulagement archaïque,
proche de celui qui inonde les bébés lorsqu'ils comprennent que ce qui
sort d'une pièce n'est pas désintégré, mais cela n'aide pas.
Installer des portes d'ascenseur ouvertes en continu partout n'aide
pas. Simplifier les choses n'aide pas. C'est une question de vases
communicants, plus les choses sont simplifiées et plus la conscience que
l'on a du monde se fragilise. Elle se cartonne, se plisse, mangée
d'eau de pluie, elle fond sur le trottoir, décollée, ramassée par les
services de la voirie, ensuite personne ne sait où elle s'en va dans la
vie réelle, car dans la vie réelle les portes des ascenseurs se ferment
trop tôt continuellement. Les clichés s'empilent comme s'empilent les
appartements dans les immeubles. Les architectes calculent les
trajectoires pour les rendre les plus efficaces possible. Quand on
gratte sous la couche efficace, on trouve souvent une idée reçue en
forme de menottes, des rails inévitables, des magazines de mode et des
publicités pour les voitures diesel. Le jeune n'est pas très efficace.
La blonde comme il faut n'est pas très efficace. Ils ne sont pas
vraiment menottés, ou alors pas toujours, en tout cas, ils tentent de
défaire leurs liens --- la preuve, ils ne se rencontrent pas. En
contrepartie, ils subissent de grands aléas accidentels.

Le jeune et la blonde comme il faut sont des nasses réticulées : à marée
basse, entre les cailloux, les nasses réticulées se déplacent sur une
patte folle, et parfois tournent en rond dans l'inconscience de
l'existence des points cardinaux, puis les vagues les déplacent, les
brassent, et recommencent. La forme de leur coquille est toujours la
même, quelles que soient leur taille, il n'y a que les proportions qui
changent selon que ce sont des nouveau-nés qui viennent d'éclore ou de
vieilles branches chenues. C'est leur obstination qui m'attendrit. Dans
attendrir, il y a attendre, et je suis presque sûre que ce n'est pas
une coïncidence.

# "Tu n'arriveras à rien", dit-elle

Le marché suffoque. Le marché est rassuré. Le marché est séduit, le
marché bascule, le marché se diversifie. Le marché est mal interprété,
mal accueilli, dans les mains d'actionnaires étrangers, affaibli de
l'autre côté des Alpes, le marché se repositionne, stratégique, il fait
virage et accord de partenariat. Il prend des risques en voulant grandir
trop vite, perd pied, culbute dans la bulle Internet, met du temps à
récupérer. Maintenant en meilleure forme, il se donne des chances de
stabilité. Succès, à la fois exemple et pari, il attend l'échéance.
Chaque matin dans le mâché de ma langue et le trou dans mon œil, le
marché s'interroge, puis s'embarque dans des aventures exquises de
feuilleton, de telenovela.

Le jeune travaille maintenant dans un garage. Il pose des pneus neige
chaque automne et les remplace par des pneus ordinaires chaque
printemps. Il vit avec une fille au sourire éclatant en cuisse de
pamplemousse, aux joues larges et au ventre plat (elle ne s'épile pas
les jambes, rit la locataire du 1^er^, sans préciser si elle trouve cela
admirable ou comique). Trump promet de l'eau pure au robinet et des
machines à laver performantes, "les meilleures au monde", ajoute-t-il,
avant de s'engager à construire un mur anti-migrants dans un état non
frontalier (il est plus fort que la géographie). La blonde comme il faut
prépare de l'andouillette à sa petite-fille. Chaque chose est plus ou
moins à sa place, en tout cas à la place qu'elle occupe.

Cette nuit j'ai entendu du bruit, comme s'il y avait quelqu'un dans la
maison, et c'était moi. Ensuite j'ai pensé à un sac en plastique, je
l'ai visualisé très nettement : un petit sac à l'embouchure fermée en
nœud, comme deux oreilles de lapin. Je me suis dit en me retournant dans
mon lit que ce sac pourrait contenir une petite cosmogonie, que ce
serait plus simple de les ranger ainsi, dans des sacs, de petits sacs.
Curieusement, je n'envisageais pas une quantité de sacs, mais un seul,
alors que j'avais des milliers de sacs en tête. Comme si leur nombre
était impossible à mettre en images. Comme si un seul sac, symbolique,
savait théoriser la pensée de multitude de sacs, une multitude contenant
de multiples mondes qui, une fois rangés, s'endormaient. Comme si un
seul geste, symbolique, pouvait contenir une multitude de joies ou de
désastres. Comme si un seul personnage pouvait les contenir tous.

Quelqu'une-quelqu'un expose une tonne de confettis noirs. Sans doute
qu'avec un seul, un unique confetti noir, il aurait raconté la même
histoire, mais est-ce qu'on l'aurait vu ? C'est le souci. La visibilité.
Aux dépens de la sincérité ? Ce sac n'est pas étanche, je ferais mieux
de le ranger dans un placard fermé à clé, ai-je pensé avant de me
rendormir. Et la présence nocturne qui montait dans les escaliers,
allumait son ordinateur, se faisait couler du café et se tournait sous
les couvertures, c'était moi. Cette nuit j'étais partout en même temps,
dans mon passé aussi, avec de vieilles histoires à ressasser, et ma
colère montait, elle était neuve bien que les vieilles histoires soient
anciennes. Tout a vieilli, hors la présence, dit quelqu'un. Je suis
présente partout au milieu d'un espace très représentatif, donc flou et
précis à la fois, rempli de tonnes de détails, certains abrasifs et
d'autres moins. Beaucoup sont dérisoires.

Je n'aime pas qu'on se moque des "ouvrages de dame". Les choses
dérisoires sont très graves. Les broderies, les canevas et les soins
apportés aux plantes sont de minuscules intransigeances avec le temps,
un combat chétif mais impitoyable. Je n'aime pas qu'on les méprise en
les appelant des riens. Ces petits riens élaborent. Ces riens, c'est
nous. Et ces petits riens savent dire "nous". Qui sait de quoi serait
capable ce "nous" s'il se faisait confiance. Nous pourrions refuser
les chats et les pandas filmés sur des hamacs. Refuser le meilleur
buteur, le record d'escalade, le mangeur du plus grand nombre d'huîtres
au monde. Nous dirions --- avec force, avec vigueur, à voix haute --- nous
sommes nous, de petits riens, rejoignez-nous. Décidez comme Glenn Gould
de partir dans le grand froid blanc. Dites non à cette honte qu'est
Guiyu. Insistez pour nettoyer Guiyu, pour sauver les habitants de Guiyu.
Ou menacez. Dites non. Dites si vous ne faites rien, face à nous petits
riens, attention à notre colère ! Nous nous lèverons tous en répétant
Guiyu, Guiyu, comme les zombies dans le clip de *Bad*, inquiétants,
menaçants, jusqu'à ce que des bus soient affrétés qui récupèrent les
occupants pour les sauver de la gangrène que notre mode de vie provoque.
Et s'il fallait devenir zombie pour que cela arrive, et retrouver la
sauvagerie des recoins de méninges inutilisés, pourquoi pas ?

Ça ne fonctionnera pas, disent mes présences qui s'activent pendant que
d'autres dorment et que l'une d'elles rêve qu'elle trouve un secret
entre les feuilles (entre les lamelles, entre deux planches, sous un
empilement coloré, coincé dans le papier, un secret pastel, diablement
neuf quand on le comparait au reste --- il fallait mettre tous les
secrets dans des coffres, et celui-là encore plus que les autres, parce
que c'était le secret de tout le monde --- l'homme m'a abordée dans la
rue, inquiet, il m'a dit "il faut que je vous demande, c'est vous qui
l'avez", j'ai cherché dans mon sac, il parlait du secret, je le savais,
j'ai répondu "je l'ai trouvé mais je ne l'ai plus", "on va m'en
donner des milliers de dollars" il a dit, j'ai pensé c'est beaucoup,
j'ai pensé cet homme c'est le télé-achat, j'ai pensé mes propres
secrets, est-ce qu'ils valent autant, et où sont-ils au fait ? et dans
mon dos une déesse levait ses bras, elle faisait remonter à la surface
les ondes fossiles tout en disant à je-ne-sais-qui par-dessus mon épaule
"tu n'arriveras à rien", dans la glace j'ai vu que j'étais derrière
moi, je-ne-sais-qui c'était moi, je portais des vêtements tatoués de
fleurs et d'arabesques sans espacements, débordantes, une mosaïque de
tatouages qui ne tenait pas compte des surfaces de mon corps, tissu et
peau, qui les avalait entièrement --- je me suis dit "c'est une bonne
protection").

# Quittons-nous sans une once de rancœur, gardons-nous d'évoquer les fosses que nous creusons pour nous mettre à l'abri, ainsi que le manque de visibilité qui y règne, ayons la pugnacité joyeuse, allons allons, à toute berzingue

Il y a beaucoup de monde à cause d'une exposition en plein air. Une
promenade du dimanche autour de ce grand monument, et, quelle chance, on
entre sous un porche, voilà des photographies géantes, les familles, les
touristes sont là, ils lisent les légendes explicatives qui parlent de
Boko Haram, de jambes coupées et de terres oubliées au fond de
marécages, de puissantes sorcelleries qui protégeront les soldats, les
rendant invincibles aux projectiles, qui parlent de fêter des
anniversaires d'enfants dans des camps de réfugiés et l'un d'eux
traîne une chèvre par son licol, il n'y a pas de transitions, sans
transition annonce souvent le présentateur du journal, et nous y sommes,
nous sommes là, touristes et promeneurs, sans transition. Un enfant,
blouson rouge, cheveux bouclés, part en avant, sa grand-mère le retient,
lui montre la chèvre sur la photo, lui dit que, petite, elle en avait
une toute pareille, qu'elle lui donnait le biberon. "Bababam répond
l'enfant. C'est beau, bababam !" (il voit la mitrailleuse du soldat)
"C'est beau la guerre ! Tu viens ?" Il appelle la grand-mère pour
qu'elle l'aide à monter l'escalier. Pendant qu'il passe devant moi, je
reste bien immobile en tentant d'élargir ma masse le plus possible, à
cause de la photo géante avec les corps brûlés, j'espère que je les
cache grâce à mon corps, je fais fonction de paravent. C'est beau la
guerre, il répète, mais personne près de lui ne tente d'élargir son
corps le plus possible pour faire écran aux paroles mortelles.

Il n'y a pas de conclusion. Il n'y a que des paramètres ponctuels que
je refuse d'assembler pour qu'ils forment un angle de vue limité ou
une morale savante. La bonne hauteur est celle du moindre, celle des
lettres Café au 1^er^ qui s'effacent progressivement parce qu'il
pleut et qu'il y a du vent.

La locataire du 1^er^ étage s'est acheté une maison de l'autre côté de
l'océan, une maison immense, avec une terrasse face aux montagnes. Un
quartier tranquille. Des résidences de standing réservées à des
architectes, des avocats et de grands chiens de race. Elle a décidé de
revenir aux sources, *at home*, c'est ce qu'elle a dit. Peut-être le
besoin de quitter le circuit imprimé de son manège forcé, de respirer un
peu.

Respirer, la locataire du 1^er^ (lorsqu'elle était encore locataire du
1^er^) m'en a parlé une fois. La nuit tombait sur la terrasse. Levant
les yeux vers ses fenêtres, elle a dit C'est très humide mon
appartement (elle prononçait "youmide") et je ne peux pas laisser les
fenêtres ouvertes. C'est très mauvais les fenêtres ouvertes (en fait,
elle ne disait pas "mauvais", mais *bad*, *very bad*, je n'ai pas su
mémoriser exactement ses mots). Je ne peux pas ouvrir les fenêtres à
cause des esprits. Les esprits entrent la nuit si les fenêtres sont
ouvertes. C'est comme des souffles (elle mimait plus qu'elle
formulait). Ça me presse le cou quand je m'endors (elle mettait ses
deux mains autour de sa gorge et serrait). Ou bien ça appuie là (elle
montrait, les paumes à plat sur sa poitrine, yeux implorants), tu
comprends ? Les esprits rentrent. Je ne peux pas aérer. Très youmide (et
je ne sais pas ce qui était le plus humide, les murs qui avait faim de
salpêtre ou ses yeux bordés d'eau, ou cette sorte de poisse
lorsqu'elle marchait en pilotant des clients, Ici la Cathédrale, ici la
maison de la dentelle, là l'Hôtel du Doyen, le plus humide étant sans
doute ce mot, Liberté, affiché sur les murs en plusieurs langues).

L'appartement du 1^er^ est vide depuis plusieurs semaines. Un couple
dont je ne sais rien mais pour qui tout est possible va s'installer.
Les silhouettes de la blonde comme il faut, du jeune et des autres
s'écartent, se dispersent, comme les résidents de l'Hôtel de la plage,
dans *Les Vacances de Monsieur Hulot*, à la toute fin. Je les imagine
sur le départ, réunis pour une sorte de rencontre ultime, de celles qui
n'ont jamais lieu parce qu'on ne sait pas au moment de les vivre que
c'est la dernière fois. On se promet de s'écrire. On a un pansement
sur le nez comme Jacques Tati, on est maladroit et souriant comme
Jacques Tati, on a allumé un minuscule feu d'artifice --- on s'est
blessé la main en le faisant ---, on a couru en rond avec un arrosoir
pour éteindre le feu comme Jacques Tati. On aurait bien voulu le remplir
d'eau et de menues bricoles, comme des "comme" à répétitions, la vie,
les rochers, les vagues, la plage et la musique du générique, il y a
tant de choses à récupérer. On l'a tenté. On n'a peut-être pas réussi
mais au moins on n'a rien abîmé.

Ce matin, j'ai aperçu de loin, sur le même trottoir que moi, un homme
qui marchait dans ma direction. Il portait une maquette de trois-mâts
fabuleux, ou bien c'était une caravelle portugaise du XV^e^ siècle,
toutes voiles dehors. Et comme il la tenait par la quille, elle se
balançait à l'envers, au rythme de son bras, une sorte de sac à main
inouï. Je l'ai croisé, vite il s'est éloigné, et moi aussi, en
symétrie. C'est bien une preuve. C'est bien la preuve qu'au-delà des
trous dans les yeux et des langues mâchées existent des tours de
passe-passe permanents --- allégories, énigmes, traductions et
stupéfactions^[Oh, il faut que je revienne en arrière parce qu'à la
liste de ce que je n'ai pas écrit, j'ai oublié d'ajouter un éloge du
doute. Faire, faire, faire, dit la voix de la femme fatiguée plus que de
raison mais droite. Je suis d'accord.].

# Liste des chapitres précédents (ou suivants si vous venez d'arriver, le facteur temps est ici essentiel)

1. Où l'on en saura plus sur une mauvaise habitude, bali, detroit, la
liberté et les baleines
2. Où sera posée sur la table la question du sujet (du latin
*subjectus*, "soumis, assujetti", ce qui d'entrée me contrarie, je
propose une sédition)
3. Où je me demande ce que j'oublie
4. Où le voyage écarte soudainement les bras sur une distance qui va de
calhoun street au jardin du luxembourg
5. Où se lave et se rince quelque chose qui dit oui
6. Ne perdons pas de temps : penchons.nous sur l'attente
7. Du youpala à frank capra
8. Où sera mentionné un livre au futur antérieur, ce qui n'est pas une
mince affaire, et pourquoi pas une épopée au plus.que.parfait pendant
qu'on y est, tout cela finira en toute logique à la poste avec un
gobelet tendu
9. Parlons un peu des filatures et des pianos qui tombent
10. Dessiner des lunettes
11. Examinons ensemble la question des dieux, de la bavière et des
cheveux gris
12. Que dire des pierres, des rhinocéros et des ongles manucurés
13. Quand tout à coup surgit une énorme boule orange ... heureusement
miró intervient
14. Entre parenthèses
15. Aimez.vous le vélo, les montagnes violettes et les effilochures ?
moi oui
16. En aparté
17. Et qu'est.ce qu'on sait du poids des couvertures de survie ?
18. Où il sera question d'œufs cuisinés, de draps, de taxis et de
floraisons blanches
19. Dans la partie qui va suivre, on ne dira pas ce que wechsler pense
de l'intelligence (qui à ses yeux n'est pas une capacité unique, mais
bien un agrégat de plusieurs traits humains), c'est bien sûr une marque
d'étourderie de ma part
20. Très court chapitre qui pointe ex abrupto la possible inadéquation
d'un titre avec ce qu'il est censé annoncer
21. Et pourquoi pas un roman sur la flûte et rien d'autre
22. S'arrêter pour un bilan provisoire me semble ici approprié
23. Où l'on se demandera quel est le bon côté des portes, tout en
faisant une petite cosmogonie (deux)
24. Nous voilà près d'une urne, c'est étonnant
25. Pour faire de petites cosmogonies (trois, etc.)
26. Quelques données scientifiques
27. Livraison gratuite satisfait ou remboursé deux ans de garantie
paiement sécurisé elle n'est pas belle la vie ? n'attendez plus pour
vous faire plaisir
28. Où la narration évoque l'éventualité du yukon et frôle des gueules
de truites avant de se perdre dans les visages et l'écholocation des
chauves.souris
29. Digérer difficilement est un emploi à plein temps
30. Focus sur la vie des nasses réticulées
31. "Tu n'arriveras à rien", dit.elle
32. Quittons.nous sans une once de rancœur, gardons.nous d'évoquer les
fosses que nous creusons pour nous mettre à l'abri, ainsi que le manque
de visibilité qui y règne, ayons la pugnacité joyeuse, allons allons, à
toute berzingue
