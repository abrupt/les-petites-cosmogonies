# ~/ABRÜPT/C. JEANNEY/LES PETITES COSMOGONIES/*

La [page de ce livre](https://abrupt.cc/c-jeanney/les-petites-cosmogonies/) sur le réseau.

## Sur le livre

Cosmogonie est un mot rond, mais ce n’est pas un mot rétréci. Le monde y est bien plus grand que les récits qui le façonnent, mais, à bien y regarder, les cosmogonies parcourent nos heures de gestes simples, se multiplient dans l'instant de nos silences. Elles vont petites et foisonnantes à la rencontre des vies qui les contemplent.

Éloge du doute et de la nuance, l'écriture s'y jette comme le risque qui va à la brisure. De petites cosmogonies pour dire l'attention qui s'en retourne invariablement à ce qui «doit» être moindre. Cahots et soupirs y cherchent une manière d'être au monde. Le déploiement d'une cosmogonie, aussi fragile soit-elle, demeure une tentative : l'appel à faire brèche.

## Sur l'autrice

C. Jeanney est autrice, parfois traductrice, souvent artiste visuelle. Elle fait ce qu'elle a à faire, et ce qu'elle a fait parle pour elle. Pas besoin de chercher beaucoup plus.

Et ce qu'elle a fait ou fait ou fera se trouve notamment sur son site [Tentatives](http://christinejeanney.net/).

## Sur la licence

Cet [antilivre](https://antilivre.org/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0). Nous avons néanmoins une [lecture libre](https://abrupt.cc/partage) de cette licence.

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.cc) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.cc/partage/).
