.PHONY: cleantex cleanall tout

#
# Variables
#

## Extension des fichiers textes (ex.: txt, md, markdown)
MEXT = txt

## Tous les fichiers textes dans le dossier contenant le texte brut de l'ouvrage
SRC = $(sort $(wildcard texte/*.$(MEXT))) 

## Le fichier contenant la bibliographie
BIB = bibliographie/sources.bib

## CSL stylesheet (un style possible pour la bibliographie)
CSL = bibliographie/style.csl

# Nom du fichier PDF produit (il porte le nom du dossier)
NOM = $(shell basename $(CURDIR))

# Template
GABARIT = gabarit/livre.tex
GABARIT_COUVERTURE = gabarit/couverture.tex

#
# Spécificités du livre
#
hauteur = 181mm
largeur = 121mm
fondperdu = 5mm
dos = 7mm
titre_table = Table des matières
titre_bibliographie = Bibliographie
# Police (ebgaramond, librecaslon, librebaskerville, un autre paquet TeX de police,
# sinon modifier le gabarit avec le paquet fontspec pour une police particulière)
# En fonction du choix de la police, il faudra optimiser certains espacements
# Comme le tocnumwidth pour les chapitres (à agrandir par ex. pour le Baskerville)
police = ebgaramond
# des options pour la police
# (par ex. lining pour un alignement des chiffres avec le Garamond, mais rien avec le Baskerville)
policeoptions = lining


html:
	pandoc -s -f markdown -t html --self-contained --top-level-division=chapter --lua-filter gabarit/pandoc-quotes.lua --filter pandoc-citeproc --csl=$(CSL) --bibliography=$(BIB) --metadata reference-section-title="$(titre_bibliographie)" --toc --toc-depth=3 --template=gabarit/antilivre.html -o $(NOM).html $(SRC) && cp $(NOM).html public/index.html

tex:
	pandoc -s -f markdown -t latex --pdf-engine=lualatex --top-level-division=chapter --filter pandoc-citeproc --csl=$(CSL) --bibliography=$(BIB) --metadata reference-section-title="$(titre_bibliographie)" --toc --toc-depth=3 -V toc-title="$(titre_table)" -V police="$(police)" -V policeoptions="$(policeoptions)" -V hauteur="$(hauteur)" -V largeur="$(largeur)" --template=$(GABARIT) -o $(NOM).tex $(SRC)

odt:
	pandoc -s -f markdown -t odt --top-level-division=chapter --filter pandoc-citeproc --csl=$(CSL) --bibliography=$(BIB) --metadata reference-section-title="$(titre_bibliographie)" --toc --toc-depth=3 -o $(NOM).odt $(SRC)

pdf:
	pandoc -s -f markdown -t latex --pdf-engine=lualatex --top-level-division=chapter --filter pandoc-citeproc --csl=$(CSL) --bibliography=$(BIB) --metadata reference-section-title="$(titre_bibliographie)" --toc --toc-depth=3 -V toc-title="$(titre_table)" -V police="$(police)" -V policeoptions="$(policeoptions)" -V hauteur="$(hauteur)" -V largeur="$(largeur)" --template=$(GABARIT) -o $(NOM).pdf $(SRC)

couv:
	pandoc -s -f markdown -t latex --pdf-engine=lualatex -V police="$(police)" -V policeoptions="$(policeoptions)" -V hauteur="$(hauteur)" -V largeur="$(largeur)" -V fondperdu="$(fondperdu)" -V dos="$(dos)" --template=$(GABARIT_COUVERTURE) -o couverture.pdf gabarit/meta_couverture.txt

epub:
	pandoc -s -f markdown -t epub2 --epub-cover-image=gabarit/ebook/couverture_ebook.jpg --css=gabarit/ebook/ebook.css --epub-embed-font=gabarit/ebook/fonts/EBGaramond12-*.otf --template=gabarit/ebook/gabarit.epub2 --filter pandoc-citeproc --csl=$(CSL) --bibliography=$(BIB) --metadata reference-section-title="$(titre_bibliographie)"  --epub-chapter-level=1 --base-header-level=1 -o ebook.epub $(SRC)

epub_notes:
	./gabarit/ebook/replace.php

txt:
	pandoc -s -f markdown -t markdown --top-level-division=chapter --atx-headers --wrap=auto --filter pandoc-citeproc --csl=$(CSL) --bibliography=$(BIB) -o $(NOM).txt $(SRC)

livre:
	./livre.sh

cleantex:
	latexmk -c && rm -f *.xml

cleanall:
	latexmk -c && rm -f *.xml && rm -f *.html *.odt *.txt *.epub *.idx *.tex *.pdf

tout: livre couv epub epub_notes txt odt html
