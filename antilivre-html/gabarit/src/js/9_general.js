// Scripts

// Sortable
const el = document.querySelector('.melange');
const sortable = Sortable.create(el,{
  animation: 500,
  draggable: ".cosmogonie",
  fallbackTolerance: 3,
});

// Interface
const menu = document.querySelector('.btn--menu');
const title = document.querySelector('.title');
const compositionBtn = document.querySelector('.btn--composition');
const composition = document.querySelector('.meta--composition');

menu.addEventListener('click', (e) => {
  e.preventDefault();
  if (composition.classList.contains('show')) {
    composition.classList.remove('show');
    compositionBtn.classList.remove('btn--anim');
  }
  title.classList.toggle('show');
  menu.classList.toggle('btn--anim');
});

compositionBtn.addEventListener('click', (e) => {
  e.preventDefault();
  if (title.classList.contains('show')) {
    title.classList.remove('show');
    menu.classList.remove('btn--anim');
  }
  composition.classList.toggle('show');
  compositionBtn.classList.toggle('btn--anim');
});

// Shuffle texts
function shuffleArray(array) {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
}

function shuffleTexts() {
  const melange = document.querySelector('.melange');
  const melangeArray = Array.prototype.slice.call(melange.querySelectorAll('.cosmogonie'));

  melangeArray.forEach((el) => {
    melange.removeChild(el);
  });

  shuffleArray(melangeArray);
  melangeArray.forEach((el) => {
    melange.appendChild(el);
  });
}

const shuffleBtn = document.querySelector('.btn--shuffle');
const textes = document.querySelectorAll('.cosmogonie');
shuffleBtn.addEventListener('click', (e) => {
  e.preventDefault();
  shuffleTexts();
});

const cosmoTitle = document.querySelectorAll('.cosmogonie__titre');
cosmoTitle.forEach((e) => {
  e.addEventListener('click', () => {
    e.nextElementSibling.classList.toggle('cosmogonie__contenu--show');
  });
});


const cursor = document.querySelector('.cursor--main');
const cursorBg = document.querySelector('.cursor--bg');
const links = document.querySelectorAll('h1, .title__link, .btn');

let cursorX = -100;
let cursorY = -100;
let cursorBgX = -100;
let cursorBgY = -100;
let pageLoad = true;

const cursorMove = () => {
  document.addEventListener('mousemove', (e) => {
    cursorX = e.clientX;
    cursorY = e.clientY;
    if (pageLoad) {
      cursorBgX = e.clientX;
      cursorBgY = e.clientY;
      pageLoad = !pageLoad;
    }
  });

  // Lerp (linear interpolation)
  const lerp = (start, finish, speed) => {
    return (1 - speed) * start + speed * finish;
  };

  const rendering = () => {
    cursorBgX = lerp(cursorBgX, cursorX, 0.1);
    cursorBgY = lerp(cursorBgY, cursorY, 0.1);
    cursor.style.left = `${cursorX}px`;
    cursor.style.top = `${cursorY}px`;
    cursorBg.style.left = `${cursorBgX}px`;
    cursorBg.style.top = `${cursorBgY}px`;
    requestAnimationFrame(rendering);
  };
  requestAnimationFrame(rendering);
}

cursorMove();

// Interaction with the cursor
 links.forEach((e) => {
   e.addEventListener('mouseenter', () => {
     cursor.classList.add('cursor--interact');
     cursorBg.classList.add('cursor--interact-bg');
   });
   e.addEventListener('mouseleave', () => {
     cursor.classList.remove('cursor--interact');
     cursorBg.classList.remove('cursor--interact-bg');
   });
 });

 document.addEventListener('click', () => {
   cursor.classList.add('cursor--click');

   setTimeout(() => {
     cursor.classList.remove('cursor--click');
   }, 250);
 });


// Dark mode
const btnDark = document.querySelector('.btn--dark');

let darkModeToggle = false;
const darkModeTheme = localStorage.getItem('theme');

if (darkModeTheme) {
  document.documentElement.setAttribute('data-theme', darkModeTheme);

  if (darkModeTheme === 'dark') {
    darkModeToggle = true;
  }
}

function darkModeSwitch() {
  if (!darkModeToggle) {
    document.documentElement.setAttribute('data-theme', 'dark');
    localStorage.setItem('theme', 'dark');
    darkModeToggle = true;
  } else {
    document.documentElement.setAttribute('data-theme', 'light');
    localStorage.setItem('theme', 'light');
    darkModeToggle = false;
  }
}

btnDark.addEventListener('click', darkModeSwitch);

// Correct 100 Vh - Source : https://codepen.io/team/css-tricks/pen/WKdJaB
// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty('--vh', `${vh}px`);

// We listen to the resize event
window.addEventListener('resize', () => {
  // We execute the same script as before
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
});
