module.exports = {
  syntax: 'postcss-scss',
  plugins: {
    'postcss-import': { path: './gabarit/src/css'},
    'postcss-import': {},
    'precss': {},
    'css-mqpacker': { sort: true },
    'autoprefixer': {cascade: false},
    'postcss-flexbugs-fixes': {},
    'cssnano': { reduceIdents: false, normalizeUrl: { stripWWW: false }},
  }
}
